package com.easipos.tabletordering.activities.login.navigation

import android.app.Activity
import com.easipos.tabletordering.activities.table_selection.TableSelectionActivity

class LoginNavigationImpl : LoginBaseNavigationImpl() {

    override fun navigateToTableSelection(activity: Activity) {
        activity.startActivity(TableSelectionActivity.newIntent(activity))
        activity.finishAffinity()
    }
}