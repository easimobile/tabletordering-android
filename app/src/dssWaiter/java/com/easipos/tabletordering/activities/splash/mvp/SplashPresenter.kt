package com.easipos.tabletordering.activities.splash.mvp

import android.app.Application

class SplashPresenter(application: Application)
    : SplashBasePresenter(application)
