package com.easipos.tabletordering.activities.main.mvp

import android.app.Application
import com.easipos.tabletordering.R
import com.easipos.tabletordering.api.requests.ordering.DeleteUsingTableRequestModel
import com.easipos.tabletordering.use_cases.base.DefaultObserver
import com.easipos.tabletordering.use_cases.ordering.DeleteUsingTableUseCase
import com.easipos.tabletordering.util.ErrorUtil

class MainPresenter(application: Application) : MainBasePresenter(application) {

    private val deleteUsingTableUseCase by lazy { DeleteUsingTableUseCase(kodein) }

    override fun onDetachView() {
        super.onDetachView()
        deleteUsingTableUseCase.dispose()
    }

    override fun doAfterHoldBill() {
        view?.showErrorAlertDialog(application.getString(R.string.prompt_hold_bill_success)) {
            view?.navigateToTableSelection()
        }
    }

    fun doDeleteUsingTable(model: DeleteUsingTableRequestModel) {
        view?.setLoadingIndicator(true)
        deleteUsingTableUseCase.execute(object : DefaultObserver<Void>() {
            override fun onComplete() {
                view?.setLoadingIndicator(false)
                view?.clearCart()
                view?.navigateToTableSelection()
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
                view?.showErrorAlertDialog(ErrorUtil.parseException(error))
            }
        }, DeleteUsingTableUseCase.Params.createQuery(model))
    }
}
