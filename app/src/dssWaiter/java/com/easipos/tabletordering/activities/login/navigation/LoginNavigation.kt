package com.easipos.tabletordering.activities.login.navigation

import android.app.Activity

interface LoginNavigation : LoginBaseNavigation {

    fun navigateToTableSelection(activity: Activity)
}