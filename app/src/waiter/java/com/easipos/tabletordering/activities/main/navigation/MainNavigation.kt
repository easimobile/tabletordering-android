package com.easipos.tabletordering.activities.main.navigation

import android.app.Activity

interface MainNavigation : MainBaseNavigation {

    fun navigateToTableSelection(activity: Activity)
}