package com.easipos.tabletordering.activities.main.mvp

interface MainView : MainBaseView {

    fun navigateToTableSelection()
}
