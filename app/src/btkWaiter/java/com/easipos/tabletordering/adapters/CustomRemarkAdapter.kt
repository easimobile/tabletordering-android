package com.easipos.tabletordering.adapters

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.easipos.tabletordering.R
import com.easipos.tabletordering.databinding.ViewRemarkBinding
import com.easipos.tabletordering.models.Remark
import io.github.anderscheow.library.kotlinExt.findColor
import io.github.anderscheow.library.kotlinExt.then
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_remark.*
import org.jetbrains.anko.textColor

class CustomRemarkAdapter(context: Context,
                          private val listener: OnGestureDetectedListener? = null)
    : BaseRecyclerViewAdapter<Remark>(context) {

    interface OnGestureDetectedListener {
        fun onAddRemark(remark: Remark)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewRemarkBinding>(
            layoutInflater, R.layout.view_remark, parent, false)
        return RemarkViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is RemarkViewHolder) {
            holder.bind(items[holder.adapterPosition])
        }
    }

    override fun getItemId(position: Int): Long = position.toLong()

    inner class RemarkViewHolder(private val binding: ViewRemarkBinding)
        : BaseViewHolder<Remark>(binding), LayoutContainer {

        override val containerView: View?
            get() = binding.root

        override fun extraBinding(item: Remark) {
        }

        override fun onClick(view: View, item: Remark?) {
            item?.let { item ->
                listener?.onAddRemark(item)
            }
        }
    }
}