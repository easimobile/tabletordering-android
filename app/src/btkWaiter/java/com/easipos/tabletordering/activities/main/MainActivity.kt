package com.easipos.tabletordering.activities.main

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.easipos.tabletordering.R
import com.easipos.tabletordering.api.requests.ordering.DeleteUsingTableRequestModel
import com.easipos.tabletordering.fragments.alert_dialog.SettingsAlertDialog
import com.easipos.tabletordering.fragments.alert_dialog.YesNoAlertDialog
import com.easipos.tabletordering.fragments.remarks.CustomRemarksDialogFragment
import com.easipos.tabletordering.models.Menu
import com.easipos.tabletordering.models.Remark
import com.easipos.tabletordering.room.RoomService
import com.easipos.tabletordering.tools.GridSpacingItemDecoration
import com.easipos.tabletordering.tools.Preference
import io.github.anderscheow.library.kotlinExt.click
import org.kodein.di.generic.instance

class MainActivity : MainBaseActivity() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    private val tableImageView by lazy { findViewById<AppCompatImageView>(R.id.image_view_table) }
    private val settingImageView by lazy { findViewById<AppCompatImageView>(R.id.image_view_setting) }
    private val productRecyclerView by lazy { findViewById<RecyclerView>(R.id.recycler_view_product) }

    private val roomService by instance<RoomService>()

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        tableImageView.click {
            checkAvailableCartItems()
        }

        settingImageView.click {
            SettingsAlertDialog.show(this)
        }
    }

    override fun navigateToTableSelection() {
        AsyncTask.execute {
            roomService.runInTransaction {
                roomService.clearAllTables()
            }
        }

        navigation.navigateToTableSelection(this)
    }

    override fun getMenuName(menu: Menu): String {
        return menu.deptCh
    }

    override fun openRemarksDialogFragment(cartItemIndex: Int, remarks: List<Remark>) {
        CustomRemarksDialogFragment.show(this, cartItemIndex, remarks)
    }

    fun updateProductAdapter() {
        productAdapter?.notifyDataSetChanged()
    }

    fun updateCartItemAdapter() {
        cartItemAdapter?.notifyDataSetChanged()
    }

    fun updateProductRow() {
        productRecyclerView.apply {
            (this.layoutManager as? GridLayoutManager)?.let {
                it.spanCount = Preference.prefSettingNumProductPerRow
            }
            this.removeItemDecorationAt(0)
            this.addItemDecoration(GridSpacingItemDecoration(Preference.prefSettingNumProductPerRow, 24, true))
        }
    }

    private fun checkAvailableCartItems() {
        cartItemAdapter?.items?.let { items ->
            val removableItems = items.filter { it.product != null }
            if (removableItems.isNotEmpty()) {
                showDeleteUsingTableAlertDialog()
            } else {
                deleteUsingTable()
            }
        }
    }

    private fun showDeleteUsingTableAlertDialog() {
        YesNoAlertDialog.show(this,
            getString(R.string.label_delete_using_table_alert),
            onNo = {
            }, onYes = {
                deleteUsingTable()
            }
        )
    }

    private fun deleteUsingTable() {
        val model = DeleteUsingTableRequestModel()

        presenter.doDeleteUsingTable(model)
    }
}
