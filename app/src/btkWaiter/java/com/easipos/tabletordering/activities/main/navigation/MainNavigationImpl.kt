package com.easipos.tabletordering.activities.main.navigation

import android.app.Activity
import com.easipos.tabletordering.activities.table_selection.TableSelectionActivity

class MainNavigationImpl : MainBaseNavigationImpl() {

    override fun navigateToTableSelection(activity: Activity) {
        activity.startActivity(TableSelectionActivity.newIntent(activity))
        activity.finish()
    }
}