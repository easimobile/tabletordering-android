package com.easipos.tabletordering.activities.table_selection

import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.appcompat.widget.AppCompatButton
import com.easipos.tabletordering.R
import com.easipos.tabletordering.models.Table
import io.github.anderscheow.library.kotlinExt.gone

class TableSelectionActivity : TableSelectionBaseActivity() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, TableSelectionActivity::class.java)
        }
    }

    override fun getTableSpanCount(): Int = 7

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        findViewById<AppCompatButton>(R.id.button_continue).apply {
            this.gone()
        }
    }

    override fun onSelectItem(item: Table) {
        selectTable()
    }
}
