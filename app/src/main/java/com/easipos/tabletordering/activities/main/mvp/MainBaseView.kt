package com.easipos.tabletordering.activities.main.mvp

import com.easipos.tabletordering.base.View
import com.easipos.tabletordering.models.*

interface MainBaseView : View {

    fun showItemAddedToCart()

    fun populateMenu(menu: List<Menu>)

    fun populateNewProducts(products: List<Product>, scrollToTop: Boolean)

    fun populateProductSet(product: Product, productSet: List<ProductSet>)

    fun populateOrderItems(orderItems: List<OrderItem>)

    fun populateRemarks(index: Int, remarks: List<Remark>)

    fun populateReceipt(receipt: Receipt)

    fun changeRetryMenuVisibility(isHidden: Boolean)

    fun changeRetryProductVisibility(isHidden: Boolean)

    fun changeNoProductVisibility(isHidden: Boolean)

    fun changeReceiptImageVisibility(isHidden: Boolean)

    fun refreshCurrentProducts()

    fun removeItemFromCart(index: Int)

    fun clearCart()
}
