package com.easipos.tabletordering.activities.login.navigation

import android.app.Activity

interface LoginBaseNavigation {

    fun navigateToMain(activity: Activity)
}