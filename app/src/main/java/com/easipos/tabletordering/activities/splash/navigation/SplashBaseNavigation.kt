package com.easipos.tabletordering.activities.splash.navigation

import android.app.Activity

interface SplashBaseNavigation {

    fun navigateToLogin(activity: Activity)
}