package com.easipos.tabletordering.activities.main

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.easipos.tabletordering.R
import com.easipos.tabletordering.activities.main.mvp.MainPresenter
import com.easipos.tabletordering.activities.main.mvp.MainView
import com.easipos.tabletordering.activities.main.navigation.MainNavigation
import com.easipos.tabletordering.adapters.CartItemAdapter
import com.easipos.tabletordering.adapters.MenuAdapter
import com.easipos.tabletordering.adapters.ProductAdapter
import com.easipos.tabletordering.api.requests.ordering.*
import com.easipos.tabletordering.base.CustomBaseAppCompatActivity
import com.easipos.tabletordering.fragments.alert_dialog.CommonAlertDialog
import com.easipos.tabletordering.fragments.alert_dialog.YesNoAlertDialog
import com.easipos.tabletordering.fragments.guest_check.GuestCheckDialogFragment
import com.easipos.tabletordering.fragments.open_price.OpenPriceDialogFragment
import com.easipos.tabletordering.fragments.product_set.ProductSetDialogFragment
import com.easipos.tabletordering.fragments.remarks.RemarksDialogFragment
import com.easipos.tabletordering.managers.FcmManager
import com.easipos.tabletordering.managers.PushNotificationManager
import com.easipos.tabletordering.models.*
import com.easipos.tabletordering.tools.EqualSpacingItemDecoration
import com.easipos.tabletordering.tools.GridSpacingItemDecoration
import com.easipos.tabletordering.tools.Preference
import com.easipos.tabletordering.util.formatCurrencyAmount
import com.orhanobut.logger.Logger
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.formatAmount
import io.github.anderscheow.library.kotlinExt.then
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.nav_view_left.*
import kotlinx.android.synthetic.main.nav_view_right.*
import org.jetbrains.anko.toast
import org.kodein.di.generic.instance

abstract class MainBaseActivity : CustomBaseAppCompatActivity(), MainView,
    MenuAdapter.OnGestureDetectedListener, ProductAdapter.OnGestureDetectedListener,
    CartItemAdapter.OnGestureDetectedListener {

    //region Variables
    protected val navigation by instance<MainNavigation>()
    private val fcmManager by instance<FcmManager>()
    private val pushNotificationManager by instance<PushNotificationManager>()

    protected val presenter by lazy { MainPresenter(application) }

    private var selectedMenu: Menu? = null

    private var menuAdapter: MenuAdapter? = null
    protected var productAdapter: ProductAdapter? = null
    protected var cartItemAdapter: CartItemAdapter? = null
    //endregion

    //region Lifecycle
    override fun onBackPressed() {
        when {
            drawerLayout.isDrawerOpen(GravityCompat.START) -> drawerLayout.closeDrawer(GravityCompat.START)
            drawerLayout.isDrawerOpen(GravityCompat.END) -> drawerLayout.closeDrawer(GravityCompat.END)
            else -> super.onBackPressed()
        }
    }

    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_main

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)
//        registerPushTokenIfPossible()
//        processPayload()
        setupViews()
        setupListeners()

        Cart.cartLiveData.observe(this, Observer { cart ->
            processCart(cart)
        })

        getOrderItems()
        getMenu()

    }
    //endregion

    //region MainBaseView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        toast(message)
    }

    override fun toastMessage(message: Int) {
        toast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message, true)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog.show(this, message, action)
    }

    override fun showItemAddedToCart() {
        toastMessage(R.string.prompt_item_added_to_cart)
    }

    override fun populateMenu(menu: List<Menu>) {
        if (menu.isNotEmpty()) {
            menuAdapter?.items = menu.map { Selectable(it) }.toMutableList()
            menuAdapter?.selectFirstIndex()
        }
    }

    override fun populateNewProducts(products: List<Product>, scrollToTop: Boolean) {
        productAdapter?.items = products.toMutableList()

        if (scrollToTop) {
            recycler_view_product.scrollToPosition(0)
        }

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        }
    }

    override fun populateProductSet(product: Product, productSet: List<ProductSet>) {
        ProductSetDialogFragment.show(this, product, productSet)
    }

    override fun populateOrderItems(orderItems: List<OrderItem>) {
        Cart.addOrderItems(orderItems)
    }

    override fun populateRemarks(index: Int, remarks: List<Remark>) {
        // Normalise with existing remarks
        Cart.cartLiveData.value?.cartItems?.getOrNull(index)?.product?.remarks?.let { existingRemarks ->
            val newRemarks = remarks.map { remark ->
                Remark(
                    kitchenRequestCode = remark.kitchenRequestCode,
                    kitchenRequestDescription = remark.kitchenRequestDescription,
                    isSelected = existingRemarks.find { it.kitchenRequestCode == remark.kitchenRequestCode }?.isSelected ?: false
                )
            }.toMutableList()

            existingRemarks.find { it.kitchenRequestCode == "" }?.let { customRemark ->
                newRemarks.add(
                    Remark(
                        kitchenRequestCode = customRemark.kitchenRequestCode,
                        kitchenRequestDescription = customRemark.kitchenRequestDescription,
                        isSelected = true
                    )
                )
            }

            openRemarksDialogFragment(index, newRemarks)
        }
    }

    override fun populateReceipt(receipt: Receipt) {
        GuestCheckDialogFragment.show(this, receipt)
    }

    override fun changeRetryMenuVisibility(isHidden: Boolean) {
        layout_retry_menu.visibility = isHidden then View.GONE ?: View.VISIBLE
        recycler_view_menu.visibility = isHidden then View.VISIBLE ?: View.GONE
    }

    override fun changeRetryProductVisibility(isHidden: Boolean) {
        layout_retry_product.visibility = isHidden then View.GONE ?: View.VISIBLE
        recycler_view_product.visibility = isHidden then View.VISIBLE ?: View.GONE
    }

    override fun changeNoProductVisibility(isHidden: Boolean) {
        text_view_no_product.visibility = isHidden then View.GONE ?: View.VISIBLE
        recycler_view_menu.visibility = View.VISIBLE
    }

    override fun changeReceiptImageVisibility(isHidden: Boolean) {
        image_view_receipt.visibility = isHidden then View.GONE ?: View.VISIBLE
    }

    override fun refreshCurrentProducts() {
        getCurrentProducts()
    }

    override fun removeItemFromCart(index: Int) {
        removeCartItem(index)
    }

    override fun clearCart() {
        Cart.clearAll()
    }
    //endregion

    //region Interface methods
    override fun onSelectItem(position: Int, menu: Menu) {
        recycler_view_menu.smoothScrollToPosition(position)
        setSelectedMenu(menu)
    }

    override fun onAddProduct(view: View, product: Product) {
        val tempProduct = product.copy()
//        AddToCartAnimation(view, image_view_add_to_cart_placeholder, image_view_cart, object : AddToCartAnimation.AnimationListener {
//            override fun onComplete() {
//            }
//        }).start()

        when {
            tempProduct.openPrice -> {
                OpenPriceDialogFragment.show(this, tempProduct)
            }

            tempProduct.productType == "SI" -> {
                presenter.doGetProductSet(tempProduct)
            }

            else -> {
                insertProductIntoCart(tempProduct)
            }
        }
    }

    override fun onRequestRemarks(index: Int, cartItem: CartItem) {
        getRemarks(index, cartItem)
    }

    override fun onUpdateQuantity(index: Int, quantity: Int) {
        updateCartItemQuantity(index, quantity)
    }

    override fun onRemoveItem(index: Int) {
        removeCartItem(index)
    }

    override fun onVoidItem(index: Int, cartItem: CartItem) {
        voidItem(index, cartItem)
    }
    //endregion

    //region Action Methods
    fun processPayload() {
        Logger.d(pushNotificationManager.payload)
        pushNotificationManager.payload?.let { _ ->
//            navigateToNotification()
            pushNotificationManager.removePayload()
        }
    }

    fun getOrderItems() {
        val model = GetOrderItemsRequestModel()

        presenter.doGetOrderItems(model)
    }

    fun insertProductIntoCart(product: Product, openPrice: Double? = null) {
        val cartItem = CartItem(
            product = product,
            openPrice = openPrice
        )

        Cart.addCartItem(cartItem)
    }

    open fun openRemarksDialogFragment(cartItemIndex: Int, remarks: List<Remark>) {
        RemarksDialogFragment.show(this, cartItemIndex, remarks)
    }

    fun submitRemarks(cartItemIndex: Int, remarks: List<Remark>) {
        Cart.addRemarks(cartItemIndex, remarks)
    }

    private fun registerPushTokenIfPossible() {
        fcmManager.service.registerFcmToken()
    }

    private fun setupViews() {
        drawerLayout.setScrimColor(Color.parseColor("#66000000"))
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        text_view_current_table.text = Preference.prefTableName

        setupDefaultAmount()
        setupMenuRecyclerView()
        setupProductRecyclerView()
        setupCartItemRecyclerView()
    }

    private fun setupListeners() {
        image_view_cart.click {
            if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
                drawerLayout.closeDrawer(GravityCompat.END)
            } else {
                drawerLayout.openDrawer(GravityCompat.END)
                cartItemAdapter?.items?.size?.let { size ->
                    if (size > 0) {
                        recycler_view_cart_item.scrollToPosition(size - 1)
                    }
                }
            }
        }

        image_view_receipt.click {
            guestCheck()
        }

        text_view_refresh_menu.click {
            getMenu()
        }

        text_view_refresh_product.click {
            getNewProducts()
        }

        button_hold_bill.click {
            attemptHoldBill()
        }

        image_view_back.click {
            if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
                drawerLayout.closeDrawer(GravityCompat.END)
            }
        }
    }

    private fun setupDefaultAmount() {
        text_view_gst_amount.text = formatCurrencyAmount(0.0)
        text_view_sst_amount.text = formatCurrencyAmount(0.0)
        text_view_total_amount.text = formatCurrencyAmount(0.0)
    }

    private fun setupMenuRecyclerView() {
        menuAdapter = MenuAdapter(this, this)
        recycler_view_menu.apply {
            this.layoutManager = LinearLayoutManager(this@MainBaseActivity)
            this.adapter = menuAdapter
            this.addItemDecoration(EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.VERTICAL))
        }
    }

    private fun setupProductRecyclerView() {
        productAdapter = ProductAdapter(this, this)
        recycler_view_product.apply {
            this.layoutManager = GridLayoutManager(this@MainBaseActivity, Preference.prefSettingNumProductPerRow)
            this.adapter = productAdapter
            this.addItemDecoration(GridSpacingItemDecoration(Preference.prefSettingNumProductPerRow, 24, true))
        }
    }

    private fun setupCartItemRecyclerView() {
        cartItemAdapter = CartItemAdapter(this, this)
        recycler_view_cart_item.apply {
            this.layoutManager = LinearLayoutManager(this@MainBaseActivity)
            this.adapter = cartItemAdapter
            this.addItemDecoration(EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.VERTICAL))
        }
    }

    private fun processCart(cart: Cart) {
        val productCartItems = cart.cartItems.filter { it.product != null }
        val totalPrice = if (productCartItems.isNotEmpty()) {
            productCartItems.map {
                it.getTotalPrice()
            }.reduce { acc, d -> acc + d }
        } else {
            0.0
        }

        val totalQuantity = if (productCartItems.isNotEmpty()) {
            productCartItems.map {
                it.product?.quantity ?: 0
            }.reduce { acc, d -> acc + d }
        } else {
            0
        }

        cartItemAdapter?.items = cart.cartItems.toMutableList()

        text_view_gst_amount.text = formatCurrencyAmount(0.0)
        text_view_sst_amount.text = formatCurrencyAmount(0.0)
        text_view_total_amount.text = formatCurrencyAmount(totalPrice)

        image_view_cart.badgeValue = totalQuantity

        requestChangeOnQuantityInCart()
    }

    private fun getMenu() {
        presenter.doGetMenu()
    }

    private fun getNewProducts() {
        selectedMenu?.let { menu ->
            val model = GetNewProductsRequestModel(menu.deptCd)
            presenter.doGetNewProducts(model)
        }
    }

    // Get products from local DB
    private fun getCurrentProducts() {
        selectedMenu?.let { menu ->
            presenter.doGetCurrentProducts(menu.deptCd)
        }
    }

    private fun setSelectedMenu(menu: Menu) {
        this.selectedMenu = menu
        text_view_menu_label.text = getMenuName(menu)

        getNewProducts()
    }

    open fun getMenuName(menu: Menu): String {
        return menu.deptNm
    }

    private fun requestChangeOnQuantityInCart() {
        productAdapter?.notifyDataSetChanged()
    }

    private fun updateCartItemQuantity(cartItemIndex: Int, quantity: Int) {
        Cart.updateQuantity(cartItemIndex, quantity)
    }

    private fun removeCartItem(cartItemIndex: Int) {
        Cart.removeCartItem(cartItemIndex)
    }

    private fun voidItem(cartItemIndex: Int, cartItem: CartItem) {
        cartItem.orderItem?.let { orderItem ->
            YesNoAlertDialog.show(this,
                getString(R.string.label_void_item_alert),
                onYes = {
                    val model = VoidItemRequestModel(
                        prodCd = orderItem.prodCd,
                        linkCd = orderItem.linkCd
                    )

                    presenter.doVoidItem(cartItemIndex, model)
                }, onNo = {
                }
            )
        }
    }

    private fun getRemarks(cartItemIndex: Int, cartItem: CartItem) {
        cartItem.product?.let { product ->
            val model = GetRemarksRequestModel(
                prodCode = product.prodCd
            )

            presenter.doGetRemarks(cartItemIndex, model)
        }
    }

    private fun attemptHoldBill() {
        Cart.cartLiveData.value?.cartItems?.let { cartItems ->
            val removableCartItems = cartItems.filter {
                it.product != null
            }
            if (removableCartItems.isNotEmpty()) {
                holdBill(removableCartItems)
            } else {
                showErrorAlertDialog(getString(R.string.error_empty_cart_items))
            }
        }
    }

    private fun holdBill(cartItems: List<CartItem>) {
        val total = if (cartItems.isNotEmpty()) {
            cartItems.map { it.getTotalPrice() }.reduce { acc, d -> acc + d }
        } else {
            0.0
        }

        val model = HoldBillRequestModel(
            total = total.formatAmount(),
            items = cartItems.map { cartItem ->
                val price = cartItem.openPrice ?: (cartItem.product?.price ?: 0.0)
                val quantity = cartItem.product?.quantity ?: 0

                val selections = arrayListOf<HoldBillItemSelectionRequestModel>()
                cartItem.product?.productSet?.let { productSet ->
                    selections.addAll(
                        productSet.map {
                            HoldBillItemSelectionRequestModel(
                                productCode = it.productCode,
                                productName = it.productName,
                                productPrice = it.productPrice.formatAmount(),
                                quantity = it.mutableQuantity.toString(),
                                multotal = (it.productPrice * it.mutableQuantity).formatAmount()
                            )
                        }
                    )
                }

                HoldBillItemRequestModel(
                    productCode = cartItem.product?.prodCd ?: "",
                    productName = cartItem.product?.prodShNm ?: "",
                    productPrice = price.formatAmount(),
                    quantity = quantity.toString(),
                    remarks = cartItem.product?.remarks?.filter {
                        it.isSelected
                    }?.joinToString(", ") {
                        it.kitchenRequestDescription
                    } ?: "",
                    multotal = (price * quantity).formatAmount(),
                    selections = selections
                )
            }
        )

        presenter.doHoldBill(model)
    }

    private fun guestCheck() {
        val model = GetGuestCheckRequestModel()

        presenter.doGuestCheck(model)
    }
    //endregion
}
