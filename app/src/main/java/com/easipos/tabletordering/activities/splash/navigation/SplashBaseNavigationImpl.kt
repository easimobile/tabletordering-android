package com.easipos.tabletordering.activities.splash.navigation

import android.app.Activity
import com.easipos.tabletordering.activities.login.LoginActivity

abstract class SplashBaseNavigationImpl : SplashNavigation {

    override fun navigateToLogin(activity: Activity) {
        activity.startActivity(LoginActivity.newIntent(activity))
        activity.finishAffinity()
    }
}