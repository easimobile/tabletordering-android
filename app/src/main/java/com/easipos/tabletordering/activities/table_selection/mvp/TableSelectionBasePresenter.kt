package com.easipos.tabletordering.activities.table_selection.mvp

import android.app.Application
import com.easipos.tabletordering.Easi
import com.easipos.tabletordering.api.requests.ordering.*
import com.easipos.tabletordering.base.Presenter
import com.easipos.tabletordering.models.*
import com.easipos.tabletordering.tools.Preference
import com.easipos.tabletordering.use_cases.base.DefaultObserver
import com.easipos.tabletordering.use_cases.base.DefaultSingleObserver
import com.easipos.tabletordering.use_cases.ordering.*
import com.easipos.tabletordering.util.ErrorUtil

abstract class TableSelectionBasePresenter(application: Application) : Presenter<TableSelectionView>(application) {

    private val getFloorsUseCase by lazy { GetFloorsUseCase(kodein) }
    private val getTablesUseCase by lazy { GetTablesUseCase(kodein) }
    private val unlockTableUseCase by lazy { UnlockTableUseCase(kodein) }
    private val saveSelectedTableUseCase by lazy { SaveSelectedTableUseCase(kodein) }
    private val setOccupiedSessionUseCase by lazy { SetOccupiedSessionUseCase(kodein) }
    private val getOrderReviewUseCase by lazy { GetOrderReviewUseCase(kodein) }

    private var currentFloor: String? = null

    override fun onDetachView() {
        super.onDetachView()
        getFloorsUseCase.dispose()
        getTablesUseCase.dispose()
        unlockTableUseCase.dispose()
        saveSelectedTableUseCase.dispose()
        setOccupiedSessionUseCase.dispose()
        getOrderReviewUseCase.dispose()
    }

    fun doLogout() {
        (application as? Easi)?.logout()
        view?.navigateToSplash()
    }

   fun doGetFloors() {
       view?.setLoadingIndicator(true)
       getFloorsUseCase.execute(object : DefaultSingleObserver<List<Floor>>() {
           override fun onSuccess(value: List<Floor>) {
               view?.setLoadingIndicator(false)
               view?.populateFloors(value)
           }

           override fun onError(error: Throwable) {
               super.onError(error)
               view?.setLoadingIndicator(false)
               view?.showErrorAlertDialog(ErrorUtil.parseException(error)) {
                   doGetFloors()
               }
           }
       }, GetFloorsUseCase.Params.createQuery())
   }

    fun doGetTables(model: GetTablesRequestModel) {
        currentFloor = model.floor

        view?.setLoadingIndicator(true)
        getTablesUseCase.execute(object : DefaultSingleObserver<List<Table>>() {
            override fun onSuccess(value: List<Table>) {
                view?.setLoadingIndicator(false)
                view?.populateTables(value)
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
                view?.showErrorAlertDialog(ErrorUtil.parseException(error)) {
                    doGetTables(model)
                }
            }
        }, GetTablesUseCase.Params.createQuery(model))
    }

    fun doUnlockTable(model: UnlockTableRequestModel) {
        view?.setLoadingIndicator(true)
        unlockTableUseCase.execute(object : DefaultObserver<Void>() {
            override fun onComplete() {
                view?.setLoadingIndicator(false)

                currentFloor?.let { floor ->
                    doGetTables(GetTablesRequestModel(floor))
                }
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
                view?.showErrorAlertDialog(ErrorUtil.parseException(error))
            }
        }, UnlockTableUseCase.Params.createQuery(model))
    }

    fun doSaveSelectedTable(model: SaveSelectedTableRequestModel) {
        view?.setLoadingIndicator(true)
        saveSelectedTableUseCase.execute(object : DefaultSingleObserver<SaveSelectedTable>() {
            override fun onSuccess(value: SaveSelectedTable) {
                view?.setLoadingIndicator(false)
                Preference.prefTableName = value.tableName
                Preference.prefTableSysId = value.tblSysId
                view?.navigateToMain()
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
                view?.showErrorAlertDialog(ErrorUtil.parseException(error))
            }
        }, SaveSelectedTableUseCase.Params.createQuery(model))
    }

    fun doSetOccupiedSession(model: SetOccupiedSessionRequestModel) {
        view?.setLoadingIndicator(true)
        setOccupiedSessionUseCase.execute(object : DefaultSingleObserver<SetOccupiedSession>() {
            override fun onSuccess(value: SetOccupiedSession) {
                view?.setLoadingIndicator(false)
                Preference.prefTableName = model.tblname
                Preference.prefTableSysId = model.tblsysid
                Preference.prefCurrentPax = value.pax
                view?.navigateToMain()
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
                view?.showErrorAlertDialog(ErrorUtil.parseException(error))
            }
        }, SetOccupiedSessionUseCase.Params.createQuery(model))
    }

    fun doGetOrderReview(model: GetOrderReviewRequestModel) {
        view?.setLoadingIndicator(true)
        getOrderReviewUseCase.execute(object : DefaultSingleObserver<List<OrderReview>>() {
            override fun onSuccess(value: List<OrderReview>) {
                view?.setLoadingIndicator(false)
                view?.populateOrderReview(value)
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
                view?.showErrorAlertDialog(ErrorUtil.parseException(error))
            }
        }, GetOrderReviewUseCase.Params.createQuery(model))
    }
}
