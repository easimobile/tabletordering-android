package com.easipos.tabletordering.activities.login.mvp

import com.easipos.tabletordering.base.View

interface LoginBaseView : View {

    fun navigateToMain()
}
