package com.easipos.tabletordering.activities.login

import android.os.Bundle
import android.widget.TextView
import com.easipos.tabletordering.BuildConfig
import com.easipos.tabletordering.R
import com.easipos.tabletordering.activities.login.mvp.LoginPresenter
import com.easipos.tabletordering.activities.login.mvp.LoginView
import com.easipos.tabletordering.activities.login.navigation.LoginNavigation
import com.easipos.tabletordering.api.requests.auth.LoginRequestModel
import com.easipos.tabletordering.base.CustomBaseAppCompatActivity
import com.easipos.tabletordering.fragments.alert_dialog.CommonAlertDialog
import com.easipos.tabletordering.fragments.change_url.ChangeUrlDialogFragment
import com.easipos.tabletordering.tools.Preference
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.extensions.validate
import io.github.anderscheow.validator.rules.common.notBlank
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.longToast
import org.kodein.di.generic.instance
import kotlin.system.exitProcess

abstract class LoginBaseActivity : CustomBaseAppCompatActivity(), LoginView {

    //region Variables
    protected val navigation by instance<LoginNavigation>()
    private val validator by instance<Validator>()

    private val presenter by lazy { LoginPresenter(application) }

    private val onEditorActionListener = TextView.OnEditorActionListener { _, _, _ ->
        attemptLogin()
        true
    }
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_login

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
    }
    //endregion

    //region LoginBaseView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message, true)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog.show(this, message, action)
    }

    override fun navigateToMain() {
        navigation.navigateToMain(this)
    }
    //endregion

    //region Action methods
    fun setCustomBaseUrl(url: String) {
        Preference.prefCustomBaseUrl = url

        CommonAlertDialog.show(this, getString(R.string.prompt_url_changes)) {
            exitProcess(0)
        }
    }

    private fun setupViews() {
        text_view_version.text = getString(R.string.label_version, BuildConfig.VERSION_NAME)
    }

    private fun setupListeners() {
        text_input_edit_text_password.setOnEditorActionListener(onEditorActionListener)

        button_login.click {
            attemptLogin()
        }

        text_view_change_url.click {
            ChangeUrlDialogFragment.show(this)
        }
    }

    private fun attemptLogin() {
        val usernameValidation = text_input_layout_username.validate()
            .notBlank(R.string.error_field_required)
        val passwordValidation = text_input_layout_password.validate()
            .notBlank(R.string.error_field_required)

        validator.setListener(object : Validator.OnValidateListener {
            override fun onValidateFailed(errors: List<String>) {
            }

            override fun onValidateSuccess(values: List<String>) {
                val username = values[0].trim()
                val password = values[1].trim()

                val model = LoginRequestModel(
                    usercode = username,
                    password = password
                )
                presenter.doLogin(model)
            }
        }).validate(usernameValidation, passwordValidation)
    }
    //endregion
}
