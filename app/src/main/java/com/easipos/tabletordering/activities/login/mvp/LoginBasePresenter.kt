package com.easipos.tabletordering.activities.login.mvp

import android.app.Application
import com.easipos.tabletordering.api.requests.auth.LoginRequestModel
import com.easipos.tabletordering.base.Presenter
import com.easipos.tabletordering.models.LoginInfo
import com.easipos.tabletordering.tools.Preference
import com.easipos.tabletordering.use_cases.auth.LoginUseCase
import com.easipos.tabletordering.use_cases.base.DefaultSingleObserver
import com.easipos.tabletordering.util.ErrorUtil

abstract class LoginBasePresenter(application: Application) : Presenter<LoginView>(application) {

    private val loginUseCase by lazy { LoginUseCase(kodein) }

    override fun onDetachView() {
        super.onDetachView()
        loginUseCase.dispose()
    }

    open fun doAfterLogin() {
        view?.navigateToMain()
    }

    fun doLogin(model: LoginRequestModel) {
        view?.setLoadingIndicator(true)
        loginUseCase.execute(object : DefaultSingleObserver<LoginInfo>() {
            override fun onSuccess(value: LoginInfo) {
                view?.setLoadingIndicator(false)
                Preference.prefTableName = value.tableName
                Preference.prefTableSysId = value.tblSysId
                Preference.prefUserCode = model.usercode
                doAfterLogin()
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
                view?.showErrorAlertDialog(ErrorUtil.parseException(error))
            }
        }, LoginUseCase.Params.createQuery(model))
    }
}
