package com.easipos.tabletordering.activities.login.navigation

import android.app.Activity
import com.easipos.tabletordering.activities.main.MainActivity

abstract class LoginBaseNavigationImpl : LoginNavigation {

    override fun navigateToMain(activity: Activity) {
        activity.startActivity(MainActivity.newIntent(activity))
        activity.finishAffinity()
    }
}