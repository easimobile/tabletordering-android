package com.easipos.tabletordering.activities.table_selection.navigation

import android.app.Activity

interface TableSelectionBaseNavigation {

    fun navigateToSplash(activity: Activity)

    fun navigateToMain(activity: Activity)
}