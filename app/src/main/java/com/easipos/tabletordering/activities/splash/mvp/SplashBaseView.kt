package com.easipos.tabletordering.activities.splash.mvp

import com.easipos.tabletordering.base.View

interface SplashBaseView : View {

    fun showUpdateAppDialog()

    fun navigateToLogin()

    fun navigateToMain()
}
