package com.easipos.tabletordering.activities.splash

import android.os.AsyncTask
import android.os.Bundle
import com.easipos.tabletordering.Easi
import com.easipos.tabletordering.R
import com.easipos.tabletordering.activities.splash.mvp.SplashPresenter
import com.easipos.tabletordering.activities.splash.mvp.SplashView
import com.easipos.tabletordering.activities.splash.navigation.SplashNavigation
import com.easipos.tabletordering.base.CustomBaseAppCompatActivity
import com.easipos.tabletordering.bundle.ParcelData
import com.easipos.tabletordering.room.RoomService
import io.github.anderscheow.library.kotlinExt.argument
import io.github.anderscheow.library.kotlinExt.delay
import io.github.anderscheow.library.kotlinExt.rate
import org.kodein.di.generic.instance

abstract class SplashBaseActivity : CustomBaseAppCompatActivity(), SplashView {

    //region Variables
    private val navigation by instance<SplashNavigation>()
    private val roomService by instance<RoomService>()

    private val presenter by lazy { SplashPresenter(application) }

    private val clearDb by argument(ParcelData.CLEAR_DB, true)
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_splash

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        forceLogout()

        if (clearDb) {
            AsyncTask.execute {
                roomService.runInTransaction {
                    roomService.clearAllTables()
                }
            }
        }

        delay(1000) {
            navigation.navigateToLogin(this)
        }
    }
    //endregion

    //region SplashView Abstract Methods
    override fun toastMessage(message: CharSequence) {
    }

    override fun toastMessage(message: Int) {
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message, true)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
    }

    override fun showUpdateAppDialog() {
        showYesAlertDialog(getString(R.string.prompt_update_app), buttonText = R.string.action_upgrade_now) {
            this@SplashBaseActivity.rate()
            finishAffinity()
        }
    }

    override fun navigateToLogin() {
    }

    override fun navigateToMain() {
    }
    //endregion

    //region Action Methods
    private fun checkVersion() {
        presenter.checkVersion()
    }

    private fun forceLogout() {
        (application as? Easi)?.logout()
    }
    //endregion
}
