package com.easipos.tabletordering.activities.table_selection

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.PopupWindow
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.easipos.tabletordering.R
import com.easipos.tabletordering.activities.table_selection.mvp.TableSelectionPresenter
import com.easipos.tabletordering.activities.table_selection.mvp.TableSelectionView
import com.easipos.tabletordering.activities.table_selection.navigation.TableSelectionNavigation
import com.easipos.tabletordering.adapters.OrderReviewAdapter
import com.easipos.tabletordering.adapters.TableAdapter
import com.easipos.tabletordering.api.requests.ordering.*
import com.easipos.tabletordering.base.CustomBaseAppCompatActivity
import com.easipos.tabletordering.constant.TableStatus
import com.easipos.tabletordering.fragments.alert_dialog.CommonAlertDialog
import com.easipos.tabletordering.fragments.alert_dialog.LockedTableAlertDialog
import com.easipos.tabletordering.fragments.alert_dialog.LogoutAlertDialog
import com.easipos.tabletordering.fragments.alert_dialog.OccupiedTableAlertDialog
import com.easipos.tabletordering.fragments.pax.PaxDialogFragment
import com.easipos.tabletordering.models.Floor
import com.easipos.tabletordering.models.OrderReview
import com.easipos.tabletordering.models.Selectable
import com.easipos.tabletordering.models.Table
import com.easipos.tabletordering.tools.EqualSpacingItemDecoration
import com.easipos.tabletordering.tools.GridSpacingItemDecoration
import com.easipos.tabletordering.tools.Preference
import com.easipos.tabletordering.util.generatePopupWindow
import io.github.anderscheow.library.kotlinExt.*
import kotlinx.android.synthetic.main.activity_table_selection.*
import kotlinx.android.synthetic.main.nav_view_left_table_selection.*
import org.jetbrains.anko.longToast
import org.kodein.di.generic.instance
import java.util.*
import kotlin.collections.emptyList
import kotlin.concurrent.fixedRateTimer

abstract class TableSelectionBaseActivity : CustomBaseAppCompatActivity(), TableSelectionView,
    TableAdapter.OnGestureDetectedListener {

    //region Variables
    private val navigation by instance<TableSelectionNavigation>()

    private val presenter by lazy { TableSelectionPresenter(application) }

    private var popupWindow: PopupWindow? = null
    private var tableAdapter: TableAdapter? = null
    private var orderReviewAdapter: OrderReviewAdapter? = null
    private var floors = emptyList<Floor>()

    private var currentFloor: Floor? = null
    private var tableTimer: Timer? = null
    //endregion

    //region Lifecycle
    override fun onBackPressed() {
        when {
            drawerLayout.isDrawerOpen(GravityCompat.START) -> drawerLayout.closeDrawer(GravityCompat.START)
            else -> super.onBackPressed()
        }
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        setTimerForGetTables()
    }

    override fun onDestroy() {
        tableTimer?.cancel()
        tableTimer?.purge()
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_table_selection

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
        getFloors()
        setTimerForGetTables()
    }
    //endregion

    //region LoginBaseView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message, true)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog.show(this, message, action)
    }

    override fun populateFloors(floors: List<Floor>) {
        this.floors = floors

        if (floors.isNotEmpty()) {
            val floor = floors.find { it.floorName == Preference.prefDefaultFloor } ?: floors[0]

            selectFloor(floor)
            getTables()
        }
    }

    override fun populateTables(tables: List<Table>) {
        tableAdapter?.items = tables.map { Selectable(it) }.toMutableList()
    }

    override fun populateOrderReview(orderReviews: List<OrderReview>) {
        orderReviewAdapter?.items = orderReviews.toMutableList()
        drawerLayout.openDrawer(GravityCompat.START)
    }

    override fun navigateToSplash() {
        navigation.navigateToSplash(this)
    }

    override fun navigateToMain() {
        navigation.navigateToMain(this)
    }
    //endregion

    //region Interface Methods
    override fun onSelectItem(item: Table) {
    }
    //endregion

    //region Action methods
    fun saveSelectedTable(table: Table) {
        val model = SaveSelectedTableRequestModel(
            tblname = table.tableName,
            tblsysid = table.tblSysId
        )

        presenter.doSaveSelectedTable(model)
    }

    private fun setupViews() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        card_view_table.gone()

        setupTableRecyclerView()
        setupOrderReviewRecyclerView()
    }

    private fun setupListeners() {
        image_view_order.click {
            getOrderReview()
        }

        image_view_logout.click {
            logout()
        }

        text_view_floor.click {
            showFloorSelection(it)
        }

        text_view_floor_default.click {
            setFloorAsDefault()
        }

        button_continue.click {
            selectTable()
        }
    }

    private fun setupTableRecyclerView() {
        tableAdapter = TableAdapter(this, this)
        with(recycler_view_table) {
            val spanCount = getTableSpanCount()
            this.adapter = this@TableSelectionBaseActivity.tableAdapter
            this.layoutManager = GridLayoutManager(this@TableSelectionBaseActivity, spanCount).apply {
//                this.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
//                    override fun getSpanSize(position: Int): Int {
//                        val listSize = this@TableSelectionActivity.adapter?.items?.size ?: 0
//                        val numFullRow = listSize % spanCount
//                        return if (position in 0 until (spanCount * numFullRow)) {
//                           spanCount
//                        } else {
//                            listSize - (numFullRow * spanCount)
//                        }
//                    }
//                }
            }
            this.addItemDecoration(GridSpacingItemDecoration(spanCount, 24, false))
        }
    }

    open fun getTableSpanCount(): Int = 8

    private fun setupOrderReviewRecyclerView() {
        orderReviewAdapter = OrderReviewAdapter(this)
        with(recycler_view_order_review) {
            this.adapter = this@TableSelectionBaseActivity.orderReviewAdapter
            this.layoutManager = LinearLayoutManager(this@TableSelectionBaseActivity)
            this.addItemDecoration(EqualSpacingItemDecoration(24))
        }
    }

    private fun setTimerForGetTables() {
        tableTimer?.cancel()
        tableTimer?.purge()
        tableTimer = fixedRateTimer("getTables", initialDelay = 30000, period = 30000) {
            runOnUiThread {
                getTables()
            }
        }
    }

    private fun getFloors() {
        presenter.doGetFloors()
    }

    private fun getTables() {
        currentFloor?.let { floor ->
            card_view_table.visible()
            text_view_floor.text = floor.floorName

            val model = GetTablesRequestModel(
                floor = floor.floorName
            )

            presenter.doGetTables(model)
        }
    }

    private fun getOrderReview() {
        val model = GetOrderReviewRequestModel()

        presenter.doGetOrderReview(model)
    }

    private fun showFloorSelection(itemView: View) {
        popupWindow = generatePopupWindow(this, R.layout.popup_select_floor, itemView.width, ViewGroup.LayoutParams.WRAP_CONTENT) { view ->
            val listView = view.findViewById<ListView>(R.id.list_view_floor)
            val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, floors.map { it.floorName })

            listView.adapter = adapter
            listView.setOnItemClickListener { _, _, position, _ ->
                popupWindow?.dismiss()

                selectFloor(floors[position])
                getTables()
            }
        }

        popupWindow?.showAsDropDown(itemView, 0, 10)
        popupWindow?.dimBehind()
    }

    private fun selectFloor(floor: Floor) {
        currentFloor = floor

        if (Preference.prefDefaultFloor == floor.floorName) {
            text_view_floor_default.setText(R.string.label_default_floor)
            text_view_floor_default.setTextColor(Color.parseColor("#5D6174"))
        } else {
            text_view_floor_default.setText(R.string.label_set_floor_as_default)
            text_view_floor_default.setTextColor(findColor(android.R.color.holo_red_light))
        }
    }

    private fun setFloorAsDefault() {
        currentFloor?.let { floor ->
            if (Preference.prefDefaultFloor != floor.floorName) {
                Preference.prefDefaultFloor = floor.floorName

                selectFloor(floor)
            }
        }
    }

    protected fun selectTable() {
        tableAdapter?.items?.let { items ->
            items.find { it.isSelected }?.data?.let { item ->
                when (item.status) {
                    TableStatus.OCCUPIED -> selectOccupiedTable(item)
                    TableStatus.AVAILABLE -> selectAvailableTable(item)
                    TableStatus.LOCKED -> selectLockedTable(item)
                }
            }
        }
    }

    private fun selectOccupiedTable(table: Table) {
        OccupiedTableAlertDialog.show(
            this,
            table,
            onSelectCurrentOrder = {
                val model = SetOccupiedSessionRequestModel(
                    tblname = table.tableName,
                    tblsysid = table.tblSysId
                )

                presenter.doSetOccupiedSession(model)
            },
            onSelectNewOrder = {
                selectAvailableTable(table)
            },
            onSelectCancel = {
                tableAdapter?.resetSelection()
            }
        )
    }

    private fun selectAvailableTable(table: Table) {
        if (Preference.prefSettingRequirePax) {
            PaxDialogFragment.show(this, table)
        } else {
            saveSelectedTable(table)
        }
    }

    private fun selectLockedTable(table: Table) {
        LockedTableAlertDialog.show(
            this,
            onSelectYes = {
                val model = UnlockTableRequestModel(
                    tblname = table.tableName,
                    tblsysid = table.tblSysId
                )

                presenter.doUnlockTable(model)
            },
            onSelectNo = {
                tableAdapter?.resetSelection()
            }
        )
    }

    private fun logout() {
        LogoutAlertDialog.show(
            this,
            onLogout = {
                presenter.doLogout()
            }
        )
    }
    //endregion
}
