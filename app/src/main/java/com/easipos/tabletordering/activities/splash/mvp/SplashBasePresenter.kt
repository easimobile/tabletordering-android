package com.easipos.tabletordering.activities.splash.mvp

import android.app.Application
import com.easipos.tabletordering.api.requests.precheck.CheckVersionRequestModel
import com.easipos.tabletordering.base.Presenter
import com.easipos.tabletordering.managers.UserManager
import com.easipos.tabletordering.tools.Preference
import com.easipos.tabletordering.use_cases.base.DefaultSingleObserver
import com.easipos.tabletordering.use_cases.precheck.CheckVersionUseCase

abstract class SplashBasePresenter(application: Application)
    : Presenter<SplashView>(application) {

    private val checkVersionUseCase by lazy { CheckVersionUseCase(kodein) }

    fun checkVersion() {
        checkVersionUseCase.execute(object : DefaultSingleObserver<Boolean>() {
            override fun onSuccess(value: Boolean) {
                super.onSuccess(value)
                if (value) {
                    view?.showUpdateAppDialog()
                } else {
                    checkIsAuthenticated()
                }
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                checkIsAuthenticated()
            }
        }, CheckVersionUseCase.Params.createQuery(CheckVersionRequestModel()))
    }

    fun checkIsAuthenticated() {
        if (Preference.prefIsLoggedIn && UserManager.token != null) {
            view?.navigateToMain()
        } else {
            view?.navigateToLogin()
        }
    }
}
