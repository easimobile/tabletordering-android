package com.easipos.tabletordering.activities.table_selection.mvp

import com.easipos.tabletordering.base.View
import com.easipos.tabletordering.models.Floor
import com.easipos.tabletordering.models.OrderReview
import com.easipos.tabletordering.models.Table

interface TableSelectionBaseView : View {

    fun populateFloors(floors: List<Floor>)

    fun populateTables(tables: List<Table>)

    fun populateOrderReview(orderReviews: List<OrderReview>)

    fun navigateToSplash()

    fun navigateToMain()
}
