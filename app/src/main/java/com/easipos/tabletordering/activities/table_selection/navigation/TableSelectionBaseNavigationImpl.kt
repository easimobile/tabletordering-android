package com.easipos.tabletordering.activities.table_selection.navigation

import android.app.Activity
import com.easipos.tabletordering.activities.main.MainActivity
import com.easipos.tabletordering.activities.splash.SplashActivity

abstract class TableSelectionBaseNavigationImpl : TableSelectionNavigation {

    override fun navigateToSplash(activity: Activity) {
        activity.startActivity(SplashActivity.newIntent(activity))
        activity.finishAffinity()
    }

    override fun navigateToMain(activity: Activity) {
        activity.startActivity(MainActivity.newIntent(activity))
        activity.finishAffinity()
    }
}