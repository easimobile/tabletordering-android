package com.easipos.tabletordering.activities.main.mvp

import android.app.Application
import com.easipos.tabletordering.R
import com.easipos.tabletordering.api.requests.ordering.*
import com.easipos.tabletordering.base.Presenter
import com.easipos.tabletordering.models.*
import com.easipos.tabletordering.use_cases.base.DefaultObserver
import com.easipos.tabletordering.use_cases.base.DefaultSingleObserver
import com.easipos.tabletordering.use_cases.ordering.*
import com.easipos.tabletordering.util.ErrorUtil
import io.github.anderscheow.library.kotlinExt.delay

abstract class MainBasePresenter(application: Application) : Presenter<MainView>(application) {

    private val getMenuUseCase by lazy { GetMenuUseCase(kodein) }
    private val getNewProductsUseCase by lazy { GetNewProductsUseCase(kodein) }
    private val getProductSetUseCase by lazy { GetProductSetUseCase(kodein) }
    private val getCurrentProductsUseCase by lazy { GetCurrentProductsUseCase(kodein) }
    private val getOrderItemsUseCase by lazy { GetOrderItemsUseCase(kodein) }
    private val voidItemUseCase by lazy { VoidItemUseCase(kodein) }
    private val getRemarksUseCase by lazy { GetRemarksUseCase(kodein) }
    private val holdBillUseCase by lazy { HoldBillUseCase(kodein) }
    private val guestCheckUseCase by lazy { GetGuestCheckUseCase(kodein) }

    override fun onDetachView() {
        super.onDetachView()
        getMenuUseCase.dispose()
        getNewProductsUseCase.dispose()
        getProductSetUseCase.dispose()
        getCurrentProductsUseCase.dispose()
        getOrderItemsUseCase.dispose()
        voidItemUseCase.dispose()
        getRemarksUseCase.dispose()
        holdBillUseCase.dispose()
        guestCheckUseCase.dispose()
    }

    fun doGetMenu() {
        view?.setLoadingIndicator(true)
        getMenuUseCase.execute(object : DefaultSingleObserver<List<Menu>>() {
            override fun onSuccess(value: List<Menu>) {
                view?.setLoadingIndicator(false)
                view?.populateMenu(value)
                view?.changeRetryMenuVisibility(true)
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
                view?.changeRetryMenuVisibility(false)
                view?.toastMessage(ErrorUtil.parseException(error))
            }
        }, GetMenuUseCase.Params.createQuery())
    }

    fun doGetNewProducts(model: GetNewProductsRequestModel) {
        view?.setLoadingIndicator(true)
        getNewProductsUseCase.execute(object : DefaultSingleObserver<List<Product>>() {
            override fun onSuccess(value: List<Product>) {
                view?.setLoadingIndicator(false)
                view?.populateNewProducts(value, true)
                view?.changeRetryProductVisibility(true)
                view?.changeNoProductVisibility(value.isNotEmpty())
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
                view?.changeRetryProductVisibility(false)
                view?.toastMessage(ErrorUtil.parseException(error))
            }
        }, GetNewProductsUseCase.Params.createQuery(model))
    }

    fun doGetProductSet(product: Product) {
        val model = GetProductSetRequestModel(
            prodCode = product.prodCd
        )

        view?.setLoadingIndicator(true)
        getProductSetUseCase.execute(object : DefaultSingleObserver<List<ProductSet>>() {
            override fun onSuccess(value: List<ProductSet>) {
                view?.setLoadingIndicator(false)
                view?.populateProductSet(product, value)
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
                view?.showErrorAlertDialog(ErrorUtil.parseException(error))
            }
        }, GetProductSetUseCase.Params.createQuery(model))
    }

    fun doGetCurrentProducts(deptCode: String) {
        getCurrentProductsUseCase.execute(object : DefaultSingleObserver<List<Product>>() {
            override fun onSuccess(value: List<Product>) {
                view?.populateNewProducts(value, false)
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.toastMessage(ErrorUtil.parseException(error))
            }
        }, GetCurrentProductsUseCase.Params.createQuery(deptCode))
    }

    fun doGetOrderItems(model: GetOrderItemsRequestModel) {
        getOrderItemsUseCase.execute(object : DefaultSingleObserver<List<OrderItem>>() {
            override fun onSuccess(value: List<OrderItem>) {
                view?.populateOrderItems(value)
                view?.changeReceiptImageVisibility(value.isEmpty())
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                delay(2000) {
                    doGetOrderItems(model)
                }
            }
        }, GetOrderItemsUseCase.Params.createQuery(model))
    }

    fun doVoidItem(cartItemIndex: Int, model: VoidItemRequestModel) {
        view?.setLoadingIndicator(true)
        voidItemUseCase.execute(object : DefaultObserver<Void>() {
            override fun onComplete() {
                view?.setLoadingIndicator(false)
                view?.removeItemFromCart(cartItemIndex)

                doGetOrderItems(GetOrderItemsRequestModel())
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
                view?.toastMessage(ErrorUtil.parseException(error))
            }
        }, VoidItemUseCase.Params.createQuery(model))
    }

    fun doGetRemarks(cartItemIndex: Int, model: GetRemarksRequestModel) {
        view?.setLoadingIndicator(true)
        getRemarksUseCase.execute(object : DefaultSingleObserver<List<Remark>>() {
            override fun onSuccess(value: List<Remark>) {
                view?.setLoadingIndicator(false)
                view?.populateRemarks(cartItemIndex, value)
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
                view?.toastMessage(ErrorUtil.parseException(error))
            }
        }, GetRemarksUseCase.Params.createQuery(model))
    }

    fun doHoldBill(model: HoldBillRequestModel) {
        view?.setLoadingIndicator(true)
        holdBillUseCase.execute(object : DefaultObserver<Void>() {
            override fun onComplete() {
                view?.setLoadingIndicator(false)
                view?.refreshCurrentProducts()
                view?.clearCart()

                doAfterHoldBill()
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
                view?.showErrorAlertDialog(ErrorUtil.parseException(error))
            }
        }, HoldBillUseCase.Params.createQuery(model))
    }

    open fun doAfterHoldBill() {
        view?.showErrorAlertDialog(application.getString(R.string.prompt_hold_bill_success))
        doGetOrderItems(GetOrderItemsRequestModel())
    }

    fun doGuestCheck(model: GetGuestCheckRequestModel) {
        view?.setLoadingIndicator(true)
        guestCheckUseCase.execute(object : DefaultSingleObserver<Receipt>() {
            override fun onSuccess(value: Receipt) {
                view?.setLoadingIndicator(false)
                view?.populateReceipt(value)
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
                view?.toastMessage(ErrorUtil.parseException(error))
            }
        }, GetGuestCheckUseCase.Params.createQuery(model))
    }
}
