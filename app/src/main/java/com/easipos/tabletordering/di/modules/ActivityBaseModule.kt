package com.easipos.tabletordering.di.modules

import com.easipos.tabletordering.activities.login.navigation.LoginNavigation
import com.easipos.tabletordering.activities.login.navigation.LoginNavigationImpl
import com.easipos.tabletordering.activities.main.navigation.MainNavigation
import com.easipos.tabletordering.activities.main.navigation.MainNavigationImpl
import com.easipos.tabletordering.activities.splash.navigation.SplashNavigation
import com.easipos.tabletordering.activities.splash.navigation.SplashNavigationImpl
import com.easipos.tabletordering.activities.table_selection.navigation.TableSelectionNavigation
import com.easipos.tabletordering.activities.table_selection.navigation.TableSelectionNavigationImpl
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.provider

open class ActivityBaseModule {

    fun provideActivityModule() = Kodein.Module("activityModule") {
        bind<SplashNavigation>() with provider { SplashNavigationImpl() }
        bind<LoginNavigation>() with provider { LoginNavigationImpl() }
        bind<TableSelectionNavigation>() with provider { TableSelectionNavigationImpl() }
        bind<MainNavigation>() with provider { MainNavigationImpl() }

        provideAdditionalActivityModule(this)
    }

    open fun provideAdditionalActivityModule(builder: Kodein.Builder) {
    }
}


