package com.easipos.tabletordering.di.modules

import org.kodein.di.Kodein

open class FragmentBaseModule {

    fun provideFragmentModule() = Kodein.Module("fragmentModule") {
        provideAdditionalFragmentModule(this)
    }

    open fun provideAdditionalFragmentModule(builder: Kodein.Builder) {
    }
}
