package com.easipos.tabletordering.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.easipos.tabletordering.models.Notification
import com.easipos.tabletordering.models.Product

@Database(
    entities = [
        Product::class,
        Notification::class
    ],
    version = 7,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class RoomService : RoomDatabase() {

    abstract fun orderingDao(): OrderingDao

    abstract fun notificationDao(): NotificationDao
}

val MIGRATION_2_3: Migration = object : Migration(2, 3) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL(
            "ALTER TABLE Product ADD COLUMN openPrice INTEGER NOT NULL DEFAULT 0"
        )
    }
}