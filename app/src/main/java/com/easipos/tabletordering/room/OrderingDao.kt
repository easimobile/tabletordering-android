package com.easipos.tabletordering.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.easipos.tabletordering.models.CartItem
import com.easipos.tabletordering.models.Product
import com.easipos.tabletordering.models.Remark

@Dao
interface OrderingDao {

    //region Product
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProducts(products: List<Product>)

    @Query("SELECT * FROM PRODUCT WHERE deptCode = :deptCode")
    fun findProducts(deptCode: String): List<Product>
    //endregion
}