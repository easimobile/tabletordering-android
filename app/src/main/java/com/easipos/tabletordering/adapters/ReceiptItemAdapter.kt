package com.easipos.tabletordering.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.easipos.tabletordering.R
import com.easipos.tabletordering.databinding.ViewReceiptItemBinding
import com.easipos.tabletordering.models.ReceiptItem
import com.easipos.tabletordering.tools.EqualSpacingItemDecoration
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_receipt_item.*

class ReceiptItemAdapter(context: Context) : BaseRecyclerViewAdapter<ReceiptItem>(context) {

    private val viewPool = RecyclerView.RecycledViewPool()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewReceiptItemBinding>(
            layoutInflater, R.layout.view_receipt_item, parent, false)
        return ReceiptItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ReceiptItemViewHolder) {
            holder.bind(items[holder.adapterPosition])
        }
    }

    override fun getItemId(position: Int): Long = position.toLong()

    inner class ReceiptItemViewHolder(private val binding: ViewReceiptItemBinding)
        : BaseViewHolder<ReceiptItem>(binding), LayoutContainer {

        init {
            recycler_view_receipt_item_selection.apply {
                this.layoutManager = LinearLayoutManager(context).apply {
                    this.initialPrefetchItemCount = 2
                }
                this.adapter = ReceiptItemSelectionAdapter(context)
                this.addItemDecoration(EqualSpacingItemDecoration(0, EqualSpacingItemDecoration.VERTICAL))
                this.setRecycledViewPool(viewPool)
            }
        }

        override val containerView: View?
            get() = binding.root

        override fun extraBinding(item: ReceiptItem) {
            (recycler_view_receipt_item_selection.adapter as? ReceiptItemSelectionAdapter)?.items = item.selections.toMutableList()
        }

        override fun onClick(view: View, item: ReceiptItem?) {
        }
    }
}