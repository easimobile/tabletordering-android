package com.easipos.tabletordering.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.easipos.tabletordering.R
import com.easipos.tabletordering.databinding.ViewProductSetCartBinding
import com.easipos.tabletordering.models.ProductSetCart
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer

class ProductSetCartAdapter(context: Context) : BaseRecyclerViewAdapter<ProductSetCart>(context) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewProductSetCartBinding>(
            layoutInflater, R.layout.view_product_set_cart, parent, false)
        return ProductSetCartViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ProductSetCartViewHolder) {
            holder.bind(items[holder.adapterPosition])
        }
    }

    inner class ProductSetCartViewHolder(private val binding: ViewProductSetCartBinding)
        : BaseViewHolder<ProductSetCart>(binding), LayoutContainer {

        override val containerView: View?
            get() = binding.root

        override fun extraBinding(item: ProductSetCart) {

        }

        override fun onClick(view: View, item: ProductSetCart?) {
        }
    }
}