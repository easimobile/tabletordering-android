package com.easipos.tabletordering.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.easipos.tabletordering.R
import com.easipos.tabletordering.databinding.ViewCartItemBinding
import com.easipos.tabletordering.models.CartItem
import com.easipos.tabletordering.models.Product
import com.easipos.tabletordering.tools.EqualSpacingItemDecoration
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_cart_item.*

class CartItemAdapter(context: Context,
                      private val listener: OnGestureDetectedListener) : BaseRecyclerViewAdapter<CartItem>(context) {

    interface OnGestureDetectedListener {
        fun onRequestRemarks(index: Int, cartItem: CartItem)
        fun onUpdateQuantity(index: Int, quantity: Int)
        fun onRemoveItem(index: Int)
        fun onVoidItem(index: Int, cartItem: CartItem)
    }

    private val viewPool = RecyclerView.RecycledViewPool()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewCartItemBinding>(
            layoutInflater, R.layout.view_cart_item, parent, false)
        return CartItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CartItemViewHolder) {
            holder.bind(items[holder.adapterPosition])
        }
    }

    inner class CartItemViewHolder(private val binding: ViewCartItemBinding)
        : BaseViewHolder<CartItem>(binding), LayoutContainer {

        init {
            recycler_view_product_set_cart.apply {
                this.layoutManager = LinearLayoutManager(context).apply {
                    this.initialPrefetchItemCount = 2
                }
                this.adapter = ProductSetCartAdapter(context)
                this.addItemDecoration(EqualSpacingItemDecoration(0, EqualSpacingItemDecoration.VERTICAL))
                this.setRecycledViewPool(viewPool)
            }
        }

        override val containerView: View?
            get() = binding.root

        override fun extraBinding(item: CartItem) {
            (recycler_view_product_set_cart.adapter as? ProductSetCartAdapter)?.items = item.getProductSetCart().toMutableList()

            item.product?.let { product ->
                button_remark.click {
                    listener.onRequestRemarks(adapterPosition, item)
                }

                button_minus.click {
                    minusQuantity(product)
                }

                button_plus.click {
                    addQuantity(product)
                }

                image_view_remove.click {
                    listener.onRemoveItem(adapterPosition)
                }

                button_remark.setBackgroundResource(R.drawable.bg_button_primary)
                button_minus.setBackgroundResource(R.drawable.bg_button_plus_minus)
                button_plus.setBackgroundResource(R.drawable.bg_button_plus_minus)
            } ?: run {
                button_remark.setOnClickListener(null)
                button_minus.setOnClickListener(null)
                button_plus.setOnClickListener(null)
                image_view_remove.setOnClickListener(null)

                image_view_remove.click {
                    listener.onVoidItem(adapterPosition, item)
                }

                button_remark.setBackgroundResource(R.drawable.bg_button_disable)
                button_minus.setBackgroundResource(R.drawable.bg_button_disable)
                button_plus.setBackgroundResource(R.drawable.bg_button_disable)
            }
        }

        override fun onClick(view: View, item: CartItem?) {
        }

        private fun minusQuantity(product: Product) {
            if (product.quantity > 1) {
                product.quantity -= 1
                listener.onUpdateQuantity(adapterPosition, product.quantity)
                notifyDataSetChanged()
            }
        }

        private fun addQuantity(product: Product) {
            if (product.quantity < 99) {
                product.quantity += 1
                listener.onUpdateQuantity(adapterPosition, product.quantity)
                notifyDataSetChanged()
            }
        }
    }
}