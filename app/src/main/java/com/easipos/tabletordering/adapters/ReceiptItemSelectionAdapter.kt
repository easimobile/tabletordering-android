package com.easipos.tabletordering.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.easipos.tabletordering.R
import com.easipos.tabletordering.databinding.ViewReceiptItemSelectionBinding
import com.easipos.tabletordering.models.ReceiptItemSelection
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer

class ReceiptItemSelectionAdapter(context: Context) : BaseRecyclerViewAdapter<ReceiptItemSelection>(context) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewReceiptItemSelectionBinding>(
            layoutInflater, R.layout.view_receipt_item_selection, parent, false)
        return ReceiptItemSelectionViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ReceiptItemSelectionViewHolder) {
            holder.bind(items[holder.adapterPosition])
        }
    }

    override fun getItemId(position: Int): Long = position.toLong()

    inner class ReceiptItemSelectionViewHolder(private val binding: ViewReceiptItemSelectionBinding)
        : BaseViewHolder<ReceiptItemSelection>(binding), LayoutContainer {

        override val containerView: View?
            get() = binding.root

        override fun extraBinding(item: ReceiptItemSelection) {
        }

        override fun onClick(view: View, item: ReceiptItemSelection?) {
        }
    }
}