package com.easipos.tabletordering.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.easipos.tabletordering.R
import com.easipos.tabletordering.constant.SelectionType
import com.easipos.tabletordering.databinding.ViewProductSetItemBinding
import com.easipos.tabletordering.models.ProductSet
import com.easipos.tabletordering.models.Selectable
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.gone
import io.github.anderscheow.library.kotlinExt.visible
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_product_set_item.*

class ProductSetItemAdapter(context: Context,
                            private val listener: OnGestureDetectedListener? = null)
    : BaseRecyclerViewAdapter<Selectable<ProductSet>>(context) {

    interface OnGestureDetectedListener {
        fun onShowMoreSubProductSet(productSet: ProductSet)
    }

    private var selectionType = SelectionType.SINGLE
    private var maxSelection = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewProductSetItemBinding>(
            layoutInflater, R.layout.view_product_set_item, parent, false)
        return ProductSetItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ProductSetItemViewHolder) {
            holder.bind(items[holder.adapterPosition])
        }
    }

    fun setCustomItems(items: List<Selectable<ProductSet>>, selectionType: SelectionType, maxSelection: Int) {
        this.selectionType = selectionType
        this.maxSelection = maxSelection
        this.items = items.toMutableList()
    }

    private fun selectIndex(selectedIndex: Int) {
        when (selectionType) {
            SelectionType.SINGLE -> {
                items.forEachIndexed { index, selectable ->
                    if (index == selectedIndex) {
                        if (selectable.isSelected.not()) {
                            selectable.isSelected = true
                            listener?.onShowMoreSubProductSet(selectable.data)
                        }
                    } else {
                        selectable.isSelected = false
                        selectable.data.mutableQuantity = 1
                    }
                }
            }

            SelectionType.MULTIPLE -> {
                val selectedItem = items.getOrNull(selectedIndex)
                if (selectedItem != null) {
                    if (selectedItem.isSelected) {
                        selectedItem.isSelected = false
                        selectedItem.data.mutableQuantity = 1
                    } else {
                        if (maxSelection == 0 || isReachMaxSelection().not()) {
                            selectedItem.isSelected = true
                        }
                    }
                }
            }
        }

        notifyDataSetChanged()
    }

    private fun isReachMaxSelection(): Boolean {
        return (items
            .filter { it.isSelected }
            .takeIf { it.isNotEmpty() }
            ?.map { it.data.mutableQuantity }
            ?.reduce { acc, i -> acc + i } ?: 0) >= maxSelection
    }

    inner class ProductSetItemViewHolder(private val binding: ViewProductSetItemBinding)
        : BaseViewHolder<Selectable<ProductSet>>(binding), LayoutContainer {

        override val containerView: View?
            get() = binding.root

        override fun extraBinding(item: Selectable<ProductSet>) {
            if (item.isSelected) {
                if (item.data.selections.isEmpty() && selectionType == SelectionType.MULTIPLE) {
                    // Can select quantity
                    image_view_tick.gone()
                    text_view_quantity.visible()
                    button_minus.visible()
                    button_plus.visible()
                } else {
                    image_view_tick.visible()
                    text_view_quantity.gone()
                    button_minus.gone()
                    button_plus.gone()
                }
            } else {
                image_view_tick.gone()
                text_view_quantity.gone()
                button_minus.gone()
                button_plus.gone()
            }

            button_minus.click {
                minusQuantity(item.data)
            }

            button_plus.click {
                addQuantity(item.data)
            }
        }

        override fun onClick(view: View, item: Selectable<ProductSet>?) {
            item?.let { _ ->
                selectIndex(adapterPosition)
            }
        }

        private fun minusQuantity(productSet: ProductSet) {
            if (productSet.mutableQuantity > 1) {
                productSet.mutableQuantity -= 1
                notifyItemChanged(adapterPosition)
            } else if (productSet.mutableQuantity == 1) {
                items.getOrNull(adapterPosition)?.isSelected = false
                notifyItemChanged(adapterPosition)
            }
        }

        private fun addQuantity(productSet: ProductSet) {
            if (isReachMaxSelection().not()) {
                productSet.mutableQuantity += 1
                notifyItemChanged(adapterPosition)
            }
        }
    }
}