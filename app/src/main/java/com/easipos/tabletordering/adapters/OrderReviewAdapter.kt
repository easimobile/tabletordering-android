package com.easipos.tabletordering.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.easipos.tabletordering.R
import com.easipos.tabletordering.databinding.ViewOrderReviewBinding
import com.easipos.tabletordering.models.OrderReview
import com.easipos.tabletordering.util.loadImage
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer

class OrderReviewAdapter(context: Context) : BaseRecyclerViewAdapter<OrderReview>(context) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewOrderReviewBinding>(
            layoutInflater, R.layout.view_order_review, parent, false)
        return OrderReviewViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is OrderReviewViewHolder) {
            holder.bind(items[holder.adapterPosition])
        }
    }

    inner class OrderReviewViewHolder(private val binding: ViewOrderReviewBinding)
        : BaseViewHolder<OrderReview>(binding), LayoutContainer {

        private val productImageView by lazy { containerView?.findViewById<AppCompatImageView>(R.id.image_view_product) }

        override val containerView: View?
            get() = binding.root

        override fun extraBinding(item: OrderReview) {
            productImageView?.loadImage(item.imageUrl)
        }

        override fun onClick(view: View, item: OrderReview?) {
        }
    }
}