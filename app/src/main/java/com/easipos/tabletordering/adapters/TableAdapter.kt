package com.easipos.tabletordering.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.easipos.tabletordering.R
import com.easipos.tabletordering.constant.TableStatus
import com.easipos.tabletordering.databinding.ViewTableBinding
import com.easipos.tabletordering.models.Selectable
import com.easipos.tabletordering.models.Table
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer

class TableAdapter(context: Context,
                   private val listener: OnGestureDetectedListener) : BaseRecyclerViewAdapter<Selectable<Table>>(context) {

    interface OnGestureDetectedListener {
        fun onSelectItem(item: Table)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewTableBinding>(
            layoutInflater, R.layout.view_table, parent, false)
        return TableViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is TableViewHolder) {
            holder.bind(items[holder.adapterPosition])
        }
    }

    fun resetSelection() {
        items.forEach { selectable ->
            selectable.isSelected = false
        }

        notifyDataSetChanged()
    }

    private fun selectIndex(position: Int) {
        items.forEachIndexed { index, selectable ->
            if (index == position) {
                selectable.isSelected = selectable.isSelected.not()

                if (selectable.isSelected) {
                    listener.onSelectItem(selectable.data)
                }
            } else {
                selectable.isSelected = false
            }
        }

        notifyDataSetChanged()
    }

    inner class TableViewHolder(private val binding: ViewTableBinding)
        : BaseViewHolder<Selectable<Table>>(binding), LayoutContainer {

        private val tableTextView by lazy { containerView?.findViewById<AppCompatTextView>(R.id.text_view_table) }

        override val containerView: View?
            get() = binding.root

        override fun extraBinding(item: Selectable<Table>) {
            if (item.isSelected) {
                tableTextView?.setBackgroundResource(R.drawable.bg_table_selected)
            } else {
                when (item.data.status) {
                    TableStatus.OCCUPIED -> tableTextView?.setBackgroundResource(R.drawable.bg_table_occupied)
                    TableStatus.AVAILABLE -> tableTextView?.setBackgroundResource(R.drawable.bg_table_available)
                    TableStatus.LOCKED -> tableTextView?.setBackgroundResource(R.drawable.bg_table_locked)
                }
            }
        }

        override fun onClick(view: View, item: Selectable<Table>?) {
            item?.let {
                selectIndex(adapterPosition)
            }
        }
    }
}