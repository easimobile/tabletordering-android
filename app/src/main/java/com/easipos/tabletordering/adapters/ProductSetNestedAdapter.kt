package com.easipos.tabletordering.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.easipos.tabletordering.R
import com.easipos.tabletordering.constant.SelectionType
import com.easipos.tabletordering.databinding.ViewProductSetNestedBinding
import com.easipos.tabletordering.models.ProductSet
import com.easipos.tabletordering.models.Selectable
import com.easipos.tabletordering.tools.GridSpacingItemDecoration
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_product_set_multiple_layer.recycler_view_product_set_nested
import kotlinx.android.synthetic.main.view_product_set_nested.*

class ProductSetNestedAdapter(context: Context) : BaseRecyclerViewAdapter<ProductSet>(context) {

    private val adapterMaps = hashMapOf<String, ProductSetItemAdapter>()
    private val adapterMaps2 = hashMapOf<String, ProductSetNestedAdapter>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewProductSetNestedBinding>(
            layoutInflater, R.layout.view_product_set_nested, parent, false)
        return ProductSetViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ProductSetViewHolder) {
            holder.bind(items[holder.adapterPosition])
        }
    }

    fun getSelectedProductSet(): List<ProductSet> {
        val productSet = arrayListOf<ProductSet>()

        adapterMaps.values.forEach { adapter ->
            adapter.items.forEach { item ->
                if (item.isSelected && item.data.insert && productSet.none { it.productCode == item.data.productCode }) {
                    productSet.add(item.data)
                }
            }
        }

        adapterMaps2.values.forEach { adapter ->
            adapter.getSelectedProductSet().forEach { item ->
                if (productSet.none { it.productCode == item.productCode }) {
                    productSet.add(item)
                }
            }
        }

        return productSet.sortedBy { it.parentProductCode }
    }

    inner class ProductSetViewHolder(private val binding: ViewProductSetNestedBinding)
        : BaseViewHolder<ProductSet>(binding), LayoutContainer, ProductSetItemAdapter.OnGestureDetectedListener {

        private var adapter = ProductSetItemAdapter(context, this)
        private var adapter2 = ProductSetNestedAdapter(context)

        init {
            recycler_view_product_set_item.apply {
                this.layoutManager = GridLayoutManager(context, 3).apply {
                    this.initialPrefetchItemCount = 2
                }
                this.adapter = this@ProductSetViewHolder.adapter
                this.addItemDecoration(GridSpacingItemDecoration(3, context.resources.getDimensionPixelSize(R.dimen.sixteen_dp), false))
            }

            recycler_view_product_set_nested.apply {
                this.layoutManager = LinearLayoutManager(context).apply {
                    this.initialPrefetchItemCount = 2
                }
                this.adapter = adapter2
                this.addItemDecoration(GridSpacingItemDecoration(3, context.resources.getDimensionPixelSize(R.dimen.sixteen_dp), false))
            }
        }

        override val containerView: View?
            get() = binding.root

        override fun extraBinding(item: ProductSet) {
            adapterMaps[item.productCode] = adapter

            adapter.setCustomItems(
                item.selections.map { Selectable(it) },
                if (item.selections.isEmpty() || item.quantity == 1) {
                    SelectionType.SINGLE
                } else {
                    SelectionType.MULTIPLE
                },
                maxSelection = item.quantity
            )
        }

        override fun onClick(view: View, item: ProductSet?) {
        }

        override fun onShowMoreSubProductSet(productSet: ProductSet) {
            adapter2.items = productSet.selections.toMutableList()

            adapterMaps2[productSet.productCode] = adapter2
        }
    }
}