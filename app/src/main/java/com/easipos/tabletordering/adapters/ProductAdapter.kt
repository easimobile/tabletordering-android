package com.easipos.tabletordering.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.easipos.tabletordering.R
import com.easipos.tabletordering.databinding.ViewProductBinding
import com.easipos.tabletordering.models.Product
import com.easipos.tabletordering.tools.Preference
import com.easipos.tabletordering.util.loadImageBase64
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.gone
import io.github.anderscheow.library.kotlinExt.visible
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_product.*

class ProductAdapter(context: Context,
                     private val listener: OnGestureDetectedListener) : BaseRecyclerViewAdapter<Product>(context) {

    interface OnGestureDetectedListener {
        fun onAddProduct(view: View, product: Product)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewProductBinding>(
            layoutInflater, R.layout.view_product, parent, false)
        return ProductViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ProductViewHolder) {
            holder.bind(items[holder.adapterPosition])
        }
    }

    inner class ProductViewHolder(private val binding: ViewProductBinding)
        : BaseViewHolder<Product>(binding), LayoutContainer {

        override val containerView: View?
            get() = binding.root

        override fun extraBinding(item: Product) {
            button_minus.click {
                minusQuantity(item)
            }

            button_plus.click {
                addQuantity(item)
            }

            button_add.click {
                addProduct(item)
            }

            if (Preference.prefSettingShowPrice) {
                text_view_product_price.visible()
            } else {
                text_view_product_price.gone()
            }

            if (Preference.prefSettingShowQuantityAddButton) {
                button_minus.visible()
                text_view_quantity.visible()
                button_plus.visible()
                button_add.visible()

                layout_content.setOnClickListener(null)
            } else {
                button_minus.gone()
                text_view_quantity.gone()
                button_plus.gone()
                button_add.gone()

                layout_content.click {
                    addProduct(item)
                }
            }
        }

        override fun onClick(view: View, item: Product?) {
        }

        private fun minusQuantity(item: Product) {
            if (item.quantity > 1) {
                item.quantity -= 1
                notifyDataSetChanged()
            }
        }

        private fun addQuantity(item: Product) {
            if (item.quantity < 99) {
                item.quantity += 1
                notifyDataSetChanged()
            }
        }

        private fun addProduct(item: Product) {
            listener.onAddProduct(layout_content, item)
            item.quantity = 1
            notifyDataSetChanged()
        }
    }
}