package com.easipos.tabletordering.adapters

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.easipos.tabletordering.R
import com.easipos.tabletordering.databinding.ViewMenuBinding
import com.easipos.tabletordering.models.Menu
import com.easipos.tabletordering.models.Selectable
import io.github.anderscheow.library.kotlinExt.findColor
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_menu.*

class MenuAdapter(context: Context,
                  private val listener: OnGestureDetectedListener)
    : BaseRecyclerViewAdapter<Selectable<Menu>>(context) {

    interface OnGestureDetectedListener {
        fun onSelectItem(position: Int, menu: Menu)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewMenuBinding>(
            layoutInflater, R.layout.view_menu, parent, false)
        return MenuViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MenuViewHolder) {
            holder.bind(items[holder.adapterPosition])
        }
    }

    fun selectFirstIndex() {
        selectIndex(0)
    }

    private fun selectIndex(selectedIndex: Int) {
        items.forEachIndexed { index, selectable ->
            if (selectedIndex == index && !selectable.isSelected) {
                listener.onSelectItem(selectedIndex, selectable.data)
            }
            selectable.isSelected = selectedIndex == index
        }
        notifyDataSetChanged()
    }

    inner class MenuViewHolder(private val binding: ViewMenuBinding)
        : BaseViewHolder<Selectable<Menu>>(binding), LayoutContainer {

        override val containerView: View?
            get() = binding.root

        override fun extraBinding(item: Selectable<Menu>) {
            if (item.isSelected) {
                layout_root.setBackgroundResource(R.drawable.bg_selected_menu)
                layout_root.elevation = 8f
                text_view_menu.setTextColor(Color.WHITE)
            } else {
                layout_root.background = null
                layout_root.elevation = 0f
                text_view_menu.setTextColor(context.findColor(R.color.colorTVPrimary))
            }
        }

        override fun onClick(view: View, item: Selectable<Menu>?) {
            item?.let { _ ->
                selectIndex(adapterPosition)
            }
        }
    }
}