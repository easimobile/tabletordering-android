package com.easipos.tabletordering.use_cases.ordering

import com.easipos.tabletordering.api.requests.ordering.DeleteUsingTableRequestModel
import com.easipos.tabletordering.repositories.ordering.OrderingRepository
import com.easipos.tabletordering.use_cases.base.AbsRxObservableUseCase
import io.reactivex.Observable
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class DeleteUsingTableUseCase(kodein: Kodein)
    : AbsRxObservableUseCase<Void, DeleteUsingTableUseCase.Params>(kodein) {

    private val repository by kodein.instance<OrderingRepository>()

    override fun createObservable(params: Params): Observable<Void> =
            repository.deleteUsingTable(params.model)

    class Params private constructor(val model: DeleteUsingTableRequestModel) {
        companion object {
            fun createQuery(model: DeleteUsingTableRequestModel): Params {
                return Params(model)
            }
        }
    }
}
