package com.easipos.tabletordering.use_cases.ordering

import com.easipos.tabletordering.api.requests.ordering.GetRemarksRequestModel
import com.easipos.tabletordering.models.Remark
import com.easipos.tabletordering.repositories.ordering.OrderingRepository
import com.easipos.tabletordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class GetRemarksUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<List<Remark>, GetRemarksUseCase.Params>(kodein) {

    private val repository by kodein.instance<OrderingRepository>()

    override fun createSingle(params: Params): Single<List<Remark>> =
            repository.getRemarks(params.model)

    class Params private constructor(val model: GetRemarksRequestModel) {
        companion object {
            fun createQuery(model: GetRemarksRequestModel): Params {
                return Params(model)
            }
        }
    }
}
