package com.easipos.tabletordering.use_cases.ordering

import com.easipos.tabletordering.api.requests.ordering.GetGuestCheckRequestModel
import com.easipos.tabletordering.models.Receipt
import com.easipos.tabletordering.repositories.ordering.OrderingRepository
import com.easipos.tabletordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class GetGuestCheckUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<Receipt, GetGuestCheckUseCase.Params>(kodein) {

    private val repository by kodein.instance<OrderingRepository>()

    override fun createSingle(params: Params): Single<Receipt> =
            repository.getGuestCheck(params.model)

    class Params private constructor(val model: GetGuestCheckRequestModel) {
        companion object {
            fun createQuery(model: GetGuestCheckRequestModel): Params {
                return Params(model)
            }
        }
    }
}
