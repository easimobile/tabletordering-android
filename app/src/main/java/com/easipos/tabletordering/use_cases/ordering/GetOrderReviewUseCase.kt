package com.easipos.tabletordering.use_cases.ordering

import com.easipos.tabletordering.api.requests.ordering.GetOrderReviewRequestModel
import com.easipos.tabletordering.models.OrderReview
import com.easipos.tabletordering.repositories.ordering.OrderingRepository
import com.easipos.tabletordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class GetOrderReviewUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<List<OrderReview>, GetOrderReviewUseCase.Params>(kodein) {

    private val repository by kodein.instance<OrderingRepository>()

    override fun createSingle(params: Params): Single<List<OrderReview>> =
            repository.getOrderReview(params.model)

    class Params private constructor(val model: GetOrderReviewRequestModel) {
        companion object {
            fun createQuery(model: GetOrderReviewRequestModel): Params {
                return Params(model)
            }
        }
    }
}
