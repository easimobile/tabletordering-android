package com.easipos.tabletordering.use_cases.base

import com.easipos.tabletordering.executor.PostExecutionThread
import com.easipos.tabletordering.executor.ThreadExecutor
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

abstract class BaseUseCase protected constructor(kodein: Kodein) {

    protected val threadExecutor by kodein.instance<ThreadExecutor>()
    protected val postExecutionThread by kodein.instance<PostExecutionThread>()

    private val disposables: CompositeDisposable = CompositeDisposable()

    fun dispose() {
        if (!disposables.isDisposed) {
            disposables.dispose()
        }
    }

    protected fun addDisposable(disposable: Disposable) {
        disposables.add(disposable)
    }
}
