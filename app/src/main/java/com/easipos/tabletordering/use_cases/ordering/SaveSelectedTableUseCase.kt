package com.easipos.tabletordering.use_cases.ordering

import com.easipos.tabletordering.api.requests.ordering.SaveSelectedTableRequestModel
import com.easipos.tabletordering.models.SaveSelectedTable
import com.easipos.tabletordering.repositories.ordering.OrderingRepository
import com.easipos.tabletordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class SaveSelectedTableUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<SaveSelectedTable, SaveSelectedTableUseCase.Params>(kodein) {

    private val repository by kodein.instance<OrderingRepository>()

    override fun createSingle(params: Params): Single<SaveSelectedTable> =
            repository.saveSelectedTable(params.model)

    class Params private constructor(val model: SaveSelectedTableRequestModel) {
        companion object {
            fun createQuery(model: SaveSelectedTableRequestModel): Params {
                return Params(model)
            }
        }
    }
}
