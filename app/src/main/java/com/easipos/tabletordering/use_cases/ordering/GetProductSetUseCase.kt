package com.easipos.tabletordering.use_cases.ordering

import com.easipos.tabletordering.api.requests.ordering.GetProductSetRequestModel
import com.easipos.tabletordering.models.ProductSet
import com.easipos.tabletordering.repositories.ordering.OrderingRepository
import com.easipos.tabletordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class GetProductSetUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<List<ProductSet>, GetProductSetUseCase.Params>(kodein) {

    private val repository by kodein.instance<OrderingRepository>()

    override fun createSingle(params: Params): Single<List<ProductSet>> =
            repository.getProductSet(params.model)

    class Params private constructor(val model: GetProductSetRequestModel) {
        companion object {
            fun createQuery(model: GetProductSetRequestModel): Params {
                return Params(model)
            }
        }
    }
}
