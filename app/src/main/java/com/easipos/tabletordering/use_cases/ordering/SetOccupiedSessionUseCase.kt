package com.easipos.tabletordering.use_cases.ordering

import com.easipos.tabletordering.api.requests.ordering.SetOccupiedSessionRequestModel
import com.easipos.tabletordering.models.SetOccupiedSession
import com.easipos.tabletordering.repositories.ordering.OrderingRepository
import com.easipos.tabletordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class SetOccupiedSessionUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<SetOccupiedSession, SetOccupiedSessionUseCase.Params>(kodein) {

    private val repository by kodein.instance<OrderingRepository>()

    override fun createSingle(params: Params): Single<SetOccupiedSession> =
            repository.setOccupiedSession(params.model)

    class Params private constructor(val model: SetOccupiedSessionRequestModel) {
        companion object {
            fun createQuery(model: SetOccupiedSessionRequestModel): Params {
                return Params(model)
            }
        }
    }
}
