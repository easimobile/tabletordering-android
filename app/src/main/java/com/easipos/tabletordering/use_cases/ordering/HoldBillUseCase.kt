package com.easipos.tabletordering.use_cases.ordering

import com.easipos.tabletordering.api.requests.ordering.HoldBillRequestModel
import com.easipos.tabletordering.repositories.ordering.OrderingRepository
import com.easipos.tabletordering.use_cases.base.AbsRxObservableUseCase
import io.reactivex.Observable
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class HoldBillUseCase(kodein: Kodein)
    : AbsRxObservableUseCase<Void, HoldBillUseCase.Params>(kodein) {

    private val repository by kodein.instance<OrderingRepository>()

    override fun createObservable(params: Params): Observable<Void> =
            repository.holdBill(params.model)

    class Params private constructor(val model: HoldBillRequestModel) {
        companion object {
            fun createQuery(model: HoldBillRequestModel): Params {
                return Params(model)
            }
        }
    }
}
