package com.easipos.tabletordering.use_cases.auth

import com.easipos.tabletordering.api.requests.auth.LoginRequestModel
import com.easipos.tabletordering.models.LoginInfo
import com.easipos.tabletordering.repositories.auth.AuthRepository
import com.easipos.tabletordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class LoginUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<LoginInfo, LoginUseCase.Params>(kodein) {

    private val repository by kodein.instance<AuthRepository>()

    override fun createSingle(params: Params): Single<LoginInfo> =
            repository.login(params.model)

    class Params private constructor(val model: LoginRequestModel) {
        companion object {
            fun createQuery(model: LoginRequestModel): Params {
                return Params(model)
            }
        }
    }
}
