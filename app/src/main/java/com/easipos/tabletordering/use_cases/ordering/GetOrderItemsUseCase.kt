package com.easipos.tabletordering.use_cases.ordering

import com.easipos.tabletordering.api.requests.ordering.GetOrderItemsRequestModel
import com.easipos.tabletordering.models.CartItem
import com.easipos.tabletordering.models.OrderItem
import com.easipos.tabletordering.repositories.ordering.OrderingRepository
import com.easipos.tabletordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class GetOrderItemsUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<List<OrderItem>, GetOrderItemsUseCase.Params>(kodein) {

    private val repository by kodein.instance<OrderingRepository>()

    override fun createSingle(params: Params): Single<List<OrderItem>> =
            repository.getOrderItems(params.model)

    class Params private constructor(val model: GetOrderItemsRequestModel) {
        companion object {
            fun createQuery(model: GetOrderItemsRequestModel): Params {
                return Params(model)
            }
        }
    }
}
