package com.easipos.tabletordering.use_cases.ordering

import com.easipos.tabletordering.api.requests.ordering.GetNewProductsRequestModel
import com.easipos.tabletordering.models.Product
import com.easipos.tabletordering.repositories.ordering.OrderingRepository
import com.easipos.tabletordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class GetNewProductsUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<List<Product>, GetNewProductsUseCase.Params>(kodein) {

    private val repository by kodein.instance<OrderingRepository>()

    override fun createSingle(params: Params): Single<List<Product>> =
            repository.getNewProducts(params.model)

    class Params private constructor(val model: GetNewProductsRequestModel) {
        companion object {
            fun createQuery(model: GetNewProductsRequestModel): Params {
                return Params(model)
            }
        }
    }
}
