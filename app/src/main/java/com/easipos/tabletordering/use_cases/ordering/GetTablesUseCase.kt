package com.easipos.tabletordering.use_cases.ordering

import com.easipos.tabletordering.api.requests.ordering.GetTablesRequestModel
import com.easipos.tabletordering.models.Table
import com.easipos.tabletordering.repositories.ordering.OrderingRepository
import com.easipos.tabletordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class GetTablesUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<List<Table>, GetTablesUseCase.Params>(kodein) {

    private val repository by kodein.instance<OrderingRepository>()

    override fun createSingle(params: Params): Single<List<Table>> =
            repository.getTables(params.model)

    class Params private constructor(val model: GetTablesRequestModel) {
        companion object {
            fun createQuery(model: GetTablesRequestModel): Params {
                return Params(model)
            }
        }
    }
}
