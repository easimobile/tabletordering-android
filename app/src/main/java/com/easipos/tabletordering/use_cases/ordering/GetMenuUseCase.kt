package com.easipos.tabletordering.use_cases.ordering

import com.easipos.tabletordering.models.Menu
import com.easipos.tabletordering.repositories.ordering.OrderingRepository
import com.easipos.tabletordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class GetMenuUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<List<Menu>, GetMenuUseCase.Params>(kodein) {

    private val repository by kodein.instance<OrderingRepository>()

    override fun createSingle(params: Params): Single<List<Menu>> =
            repository.getMenu()

    class Params private constructor() {
        companion object {
            fun createQuery(): Params {
                return Params()
            }
        }
    }
}
