package com.easipos.tabletordering.use_cases.ordering

import com.easipos.tabletordering.api.requests.ordering.UnlockTableRequestModel
import com.easipos.tabletordering.repositories.ordering.OrderingRepository
import com.easipos.tabletordering.use_cases.base.AbsRxObservableUseCase
import io.reactivex.Observable
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class UnlockTableUseCase(kodein: Kodein)
    : AbsRxObservableUseCase<Void, UnlockTableUseCase.Params>(kodein) {

    private val repository by kodein.instance<OrderingRepository>()

    override fun createObservable(params: Params): Observable<Void> =
            repository.unlockTable(params.model)

    class Params private constructor(val model: UnlockTableRequestModel) {
        companion object {
            fun createQuery(model: UnlockTableRequestModel): Params {
                return Params(model)
            }
        }
    }
}
