package com.easipos.tabletordering.use_cases.ordering

import com.easipos.tabletordering.api.requests.ordering.VoidItemRequestModel
import com.easipos.tabletordering.models.CartItem
import com.easipos.tabletordering.repositories.ordering.OrderingRepository
import com.easipos.tabletordering.use_cases.base.AbsRxObservableUseCase
import com.easipos.tabletordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Observable
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class VoidItemUseCase(kodein: Kodein)
    : AbsRxObservableUseCase<Void, VoidItemUseCase.Params>(kodein) {

    private val repository by kodein.instance<OrderingRepository>()

    override fun createObservable(params: Params): Observable<Void> =
            repository.voidItem(params.model)

    class Params private constructor(val model: VoidItemRequestModel) {
        companion object {
            fun createQuery(model: VoidItemRequestModel): Params {
                return Params(model)
            }
        }
    }
}
