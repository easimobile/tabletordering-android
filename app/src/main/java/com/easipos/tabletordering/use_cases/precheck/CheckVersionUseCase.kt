package com.easipos.tabletordering.use_cases.precheck

import com.easipos.tabletordering.api.requests.precheck.CheckVersionRequestModel
import com.easipos.tabletordering.repositories.precheck.PrecheckRepository
import com.easipos.tabletordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class CheckVersionUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<Boolean, CheckVersionUseCase.Params>(kodein) {

    private val repository by kodein.instance<PrecheckRepository>()

    override fun createSingle(params: Params): Single<Boolean> =
            repository.checkVersion(params.model)

    class Params private constructor(val model: CheckVersionRequestModel) {
        companion object {
            fun createQuery(model: CheckVersionRequestModel): Params {
                return Params(model)
            }
        }
    }
}
