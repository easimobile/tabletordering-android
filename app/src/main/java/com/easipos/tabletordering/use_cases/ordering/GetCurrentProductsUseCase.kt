package com.easipos.tabletordering.use_cases.ordering

import com.easipos.tabletordering.models.Product
import com.easipos.tabletordering.repositories.ordering.OrderingRepository
import com.easipos.tabletordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class GetCurrentProductsUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<List<Product>, GetCurrentProductsUseCase.Params>(kodein) {

    private val repository by kodein.instance<OrderingRepository>()

    override fun createSingle(params: Params): Single<List<Product>> =
            repository.getCurrentProducts(params.deptCode)

    class Params private constructor(val deptCode: String) {
        companion object {
            fun createQuery(deptCode: String): Params {
                return Params(deptCode)
            }
        }
    }
}
