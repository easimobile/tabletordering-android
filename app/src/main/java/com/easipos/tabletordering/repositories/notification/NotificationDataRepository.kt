package com.easipos.tabletordering.repositories.notification

import com.easipos.tabletordering.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.tabletordering.api.requests.notification.RemoveFcmTokenRequestModel
import com.easipos.tabletordering.datasource.DataFactory
import io.reactivex.Completable

class NotificationDataRepository(private val dataFactory: DataFactory) : NotificationRepository {

    override fun registerFcmToken(model: RegisterFcmTokenRequestModel): Completable =
        dataFactory.createNotificationDataSource()
            .registerFcmToken(model)

    override fun removeFcmToken(model: RemoveFcmTokenRequestModel): Completable =
        dataFactory.createNotificationDataSource()
            .removeFcmToken(model)
}