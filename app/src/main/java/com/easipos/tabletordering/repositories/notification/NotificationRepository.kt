package com.easipos.tabletordering.repositories.notification

import com.easipos.tabletordering.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.tabletordering.api.requests.notification.RemoveFcmTokenRequestModel
import io.reactivex.Completable

interface NotificationRepository {

    fun registerFcmToken(model: RegisterFcmTokenRequestModel): Completable

    fun removeFcmToken(model: RemoveFcmTokenRequestModel): Completable
}
