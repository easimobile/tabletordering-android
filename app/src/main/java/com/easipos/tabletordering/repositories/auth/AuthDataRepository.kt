package com.easipos.tabletordering.repositories.auth

import com.easipos.tabletordering.api.requests.auth.LoginRequestModel
import com.easipos.tabletordering.datasource.DataFactory
import com.easipos.tabletordering.mapper.auth.LoginInfoMapper
import com.easipos.tabletordering.models.LoginInfo
import io.reactivex.Single

class AuthDataRepository(private val dataFactory: DataFactory) : AuthRepository {

    private val loginInfoMapper by lazy { LoginInfoMapper() }

    override fun login(model: LoginRequestModel): Single<LoginInfo> =
        dataFactory.createAuthDataSource()
            .login(model, loginInfoMapper)
}
