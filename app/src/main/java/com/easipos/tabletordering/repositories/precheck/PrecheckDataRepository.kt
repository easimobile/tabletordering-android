package com.easipos.tabletordering.repositories.precheck

import com.easipos.tabletordering.api.requests.precheck.CheckVersionRequestModel
import com.easipos.tabletordering.datasource.DataFactory
import io.reactivex.Single

class PrecheckDataRepository(private val dataFactory: DataFactory) : PrecheckRepository {

    override fun checkVersion(model: CheckVersionRequestModel): Single<Boolean> =
        dataFactory.createPrecheckDataSource()
            .checkVersion(model)
}
