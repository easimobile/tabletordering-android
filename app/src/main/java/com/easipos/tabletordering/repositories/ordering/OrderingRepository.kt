package com.easipos.tabletordering.repositories.ordering

import com.easipos.tabletordering.api.requests.ordering.*
import com.easipos.tabletordering.models.*
import io.reactivex.Observable
import io.reactivex.Single

interface OrderingRepository {

    fun getFloors(): Single<List<Floor>>

    fun getTables(model: GetTablesRequestModel): Single<List<Table>>

    fun unlockTable(model: UnlockTableRequestModel): Observable<Void>

    fun saveSelectedTable(model: SaveSelectedTableRequestModel): Single<SaveSelectedTable>

    fun setOccupiedSession(model: SetOccupiedSessionRequestModel): Single<SetOccupiedSession>

    fun getOrderReview(model: GetOrderReviewRequestModel): Single<List<OrderReview>>

    fun getMenu(): Single<List<Menu>>

    fun getNewProducts(model: GetNewProductsRequestModel): Single<List<Product>>

    fun getProductSet(model: GetProductSetRequestModel): Single<List<ProductSet>>

    fun getCurrentProducts(deptCode: String): Single<List<Product>>

    fun getOrderItems(model: GetOrderItemsRequestModel): Single<List<OrderItem>>

    fun voidItem(model: VoidItemRequestModel): Observable<Void>

    fun getRemarks(model: GetRemarksRequestModel): Single<List<Remark>>

    fun holdBill(model: HoldBillRequestModel): Observable<Void>

    fun getGuestCheck(model: GetGuestCheckRequestModel): Single<Receipt>

    fun deleteUsingTable(model: DeleteUsingTableRequestModel): Observable<Void>
}
