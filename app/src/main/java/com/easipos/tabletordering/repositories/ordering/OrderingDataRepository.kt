package com.easipos.tabletordering.repositories.ordering

import com.easipos.tabletordering.api.requests.ordering.*
import com.easipos.tabletordering.datasource.DataFactory
import com.easipos.tabletordering.mapper.ordering.*
import com.easipos.tabletordering.models.*
import io.reactivex.Observable
import io.reactivex.Single

class OrderingDataRepository(private val dataFactory: DataFactory) : OrderingRepository {

    private val floorMapper by lazy { FloorMapper() }
    private val tableMapper by lazy { TableMapper() }
    private val saveSelectedTableMapper by lazy { SaveSelectedTableMapper() }
    private val setOccupiedSessionMapper by lazy { SetOccupiedSessionMapper() }
    private val orderReviewMapper by lazy { OrderReviewMapper() }
    private val menuMapper by lazy { MenuMapper() }
    private val productMapper by lazy { ProductMapper() }
    private val productSetMapper by lazy { ProductSetMapper("") }
    private val receiptMapper by lazy { ReceiptMapper() }
    private val remarkMapper by lazy { RemarkMapper() }
    private val orderItemMapper by lazy { OrderItemMapper() }

    override fun getFloors(): Single<List<Floor>> =
        dataFactory.createOrderingDataSource()
            .getFloors(floorMapper)

    override fun getTables(model: GetTablesRequestModel): Single<List<Table>> =
        dataFactory.createOrderingDataSource()
            .getTables(model, tableMapper)

    override fun unlockTable(model: UnlockTableRequestModel): Observable<Void> =
        dataFactory.createOrderingDataSource()
            .unlockTable(model)

    override fun saveSelectedTable(model: SaveSelectedTableRequestModel): Single<SaveSelectedTable> =
        dataFactory.createOrderingDataSource()
            .saveSelectedTable(model, saveSelectedTableMapper)

    override fun setOccupiedSession(model: SetOccupiedSessionRequestModel): Single<SetOccupiedSession> =
        dataFactory.createOrderingDataSource()
            .setOccupiedSession(model, setOccupiedSessionMapper)

    override fun getOrderReview(model: GetOrderReviewRequestModel): Single<List<OrderReview>> =
        dataFactory.createOrderingDataSource()
            .getOrderReview(model, orderReviewMapper)

    override fun getMenu(): Single<List<Menu>> =
        dataFactory.createOrderingDataSource()
            .getMenu(menuMapper)

    override fun getNewProducts(model: GetNewProductsRequestModel): Single<List<Product>> =
        dataFactory.createOrderingDataSource()
            .getNewProducts(model, productMapper)

    override fun getProductSet(model: GetProductSetRequestModel): Single<List<ProductSet>> =
        dataFactory.createOrderingDataSource()
            .getProductSet(model, productSetMapper)

    override fun getCurrentProducts(deptCode: String): Single<List<Product>> =
        dataFactory.createOrderingDataSource()
            .getCurrentProducts(deptCode)

    override fun getOrderItems(model: GetOrderItemsRequestModel): Single<List<OrderItem>> =
        dataFactory.createOrderingDataSource()
            .getOrderItems(model, orderItemMapper)

    override fun voidItem(model: VoidItemRequestModel): Observable<Void> =
        dataFactory.createOrderingDataSource()
            .voidItem(model)

    override fun getRemarks(model: GetRemarksRequestModel): Single<List<Remark>> =
        dataFactory.createOrderingDataSource()
            .getRemarks(model, remarkMapper)

    override fun holdBill(model: HoldBillRequestModel): Observable<Void> =
        dataFactory.createOrderingDataSource()
            .holdBill(model)

    override fun getGuestCheck(model: GetGuestCheckRequestModel): Single<Receipt> =
        dataFactory.createOrderingDataSource()
            .getGuestCheck(model, receiptMapper)

    override fun deleteUsingTable(model: DeleteUsingTableRequestModel): Observable<Void> =
        dataFactory.createOrderingDataSource()
            .deleteUsingTable(model)
}
