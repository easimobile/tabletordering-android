package com.easipos.tabletordering.repositories.auth

import com.easipos.tabletordering.api.requests.auth.LoginRequestModel
import com.easipos.tabletordering.models.LoginInfo
import io.reactivex.Single

interface AuthRepository {

    fun login(model: LoginRequestModel): Single<LoginInfo>
}
