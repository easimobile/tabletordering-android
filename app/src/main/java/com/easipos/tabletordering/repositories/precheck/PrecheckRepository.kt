package com.easipos.tabletordering.repositories.precheck

import com.easipos.tabletordering.api.requests.precheck.CheckVersionRequestModel
import io.reactivex.Single

interface PrecheckRepository {

    fun checkVersion(model: CheckVersionRequestModel): Single<Boolean>
}
