package com.easipos.tabletordering.bundle

object ParcelData {

    const val CLEAR_DB = "clear_db"
    const val URL = "url"
    const val CART_ITEM_INDEX = "CART_ITEM_INDEX"
    const val PROD_CODE = "PROD_CODE"
    const val REMARKS = "REMARKS"
    const val RECEIPT = "RECEIPT"
    const val PRODUCT = "PRODUCT"
    const val PRODUCT_SET = "PRODUCT_SET"
    const val TABLE = "TABLE"
}