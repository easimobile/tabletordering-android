package com.easipos.tabletordering.mapper.ordering

import com.easipos.tabletordering.api.responses.ordering.ProductResponseModel
import com.easipos.tabletordering.mapper.Mapper
import com.easipos.tabletordering.models.Product

class ProductMapper : Mapper<Product, ProductResponseModel>() {

    override fun transform(item: ProductResponseModel): Product {
        return Product(
            prodCd = item.prodCd ?: "",
            prodShNm = item.prodShNm ?: "",
            price = item.price?.toDoubleOrNull() ?: 0.0,
            prodNmCh = item.prodNmCh ?: "",
            tax1 = item.tax1 ?: "",
            tax2 = item.tax2 ?: "",
            imgData = item.imgData ?: "",
            openPrice = item.openPrice == "1",
            productType = item.productType ?: ""
        )
    }
}