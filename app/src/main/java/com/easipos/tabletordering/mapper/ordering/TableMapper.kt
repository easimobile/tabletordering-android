package com.easipos.tabletordering.mapper.ordering

import com.easipos.tabletordering.api.responses.ordering.TableResponseModel
import com.easipos.tabletordering.mapper.Mapper
import com.easipos.tabletordering.models.Table

class TableMapper : Mapper<Table, TableResponseModel>() {

    override fun transform(item: TableResponseModel): Table {
        return Table(
            tableName = item.tableName ?: "",
            tblSysId = item.tblSysId ?: "",
            status = item.status ?: ""
        )
    }
}