package com.easipos.tabletordering.mapper.ordering

import com.easipos.tabletordering.api.responses.ordering.ProductSetResponseModel
import com.easipos.tabletordering.mapper.Mapper
import com.easipos.tabletordering.models.ProductSet

class ProductSetMapper(private val parentProductCode: String) : Mapper<ProductSet, ProductSetResponseModel>() {

    override fun transform(item: ProductSetResponseModel): ProductSet {
        return ProductSet(
            parentProductCode = parentProductCode,
            productCode = item.productCode ?: "",
            quantity = item.quantity ?: 1,
            productName = item.productName ?: "",
            productNameCh = item.productNameCh ?: "",
            productPrice = item.productPrice?.toDoubleOrNull() ?: 0.0,
            prodType = item.prodType ?: "",
            insert = item.insert == 1,
            selections = ProductSetMapper(item.productCode ?: "").transform(item.selections ?: emptyList())
        )
    }
}