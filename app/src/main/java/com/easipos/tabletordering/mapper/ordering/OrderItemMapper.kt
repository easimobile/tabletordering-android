package com.easipos.tabletordering.mapper.ordering

import com.easipos.tabletordering.api.responses.ordering.OrderItemResponseModel
import com.easipos.tabletordering.api.responses.ordering.OrderItemSelectionResponseModel
import com.easipos.tabletordering.mapper.Mapper
import com.easipos.tabletordering.models.OrderItem
import com.easipos.tabletordering.models.OrderItemSelection

class OrderItemMapper : Mapper<OrderItem, OrderItemResponseModel>() {

    private val orderItemSelectionMapper by lazy { OrderItemSelectionMapper() }

    override fun transform(item: OrderItemResponseModel): OrderItem {
        return OrderItem(
            prodCd = item.prodCd ?: "",
            prodNm = item.prodNm ?: "",
            quantity = item.quantity ?: 0,
            remarks = item.remarks ?: "",
            prodPrice = item.prodPrice?.toDoubleOrNull() ?: 0.0,
            multotal = item.multotal ?: "",
            linkCd = item.linkCd ?: "",
            prodNmCh = item.prodNmCh ?: "",
            selections = if (item.selections == null) emptyList() else orderItemSelectionMapper.transform(item.selections)
        )
    }
}

private class OrderItemSelectionMapper : Mapper<OrderItemSelection, OrderItemSelectionResponseModel>() {

    override fun transform(item: OrderItemSelectionResponseModel): OrderItemSelection {
        return OrderItemSelection(
            prodCd = item.prodCd ?: "",
            prodNm = item.prodNm ?: "",
            quantity = item.quantity ?: 0,
            remarks = item.remarks ?: "",
            prodPrice = item.prodPrice?.toDoubleOrNull() ?: 0.0,
            multotal = item.multotal ?: "",
            linkCd = item.linkCd ?: "",
            prodNmCh = item.prodNmCh ?: ""
        )
    }
}