package com.easipos.tabletordering.mapper.ordering

import com.easipos.tabletordering.api.responses.ordering.FloorResponseModel
import com.easipos.tabletordering.mapper.Mapper
import com.easipos.tabletordering.models.Floor

class FloorMapper : Mapper<Floor, FloorResponseModel>() {

    override fun transform(item: FloorResponseModel): Floor {
        return Floor(
            floorName = item.floorName ?: ""
        )
    }
}