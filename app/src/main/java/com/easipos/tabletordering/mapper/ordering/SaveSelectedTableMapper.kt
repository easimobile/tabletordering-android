package com.easipos.tabletordering.mapper.ordering

import com.easipos.tabletordering.api.responses.ordering.SaveSelectedTableResponseModel
import com.easipos.tabletordering.mapper.Mapper
import com.easipos.tabletordering.models.SaveSelectedTable

class SaveSelectedTableMapper : Mapper<SaveSelectedTable, SaveSelectedTableResponseModel>() {

    override fun transform(item: SaveSelectedTableResponseModel): SaveSelectedTable {
        return SaveSelectedTable(
            tableName = item.tableName ?: "",
            tblSysId = item.tblSysId ?: ""
        )
    }
}