package com.easipos.tabletordering.mapper.ordering

import com.easipos.tabletordering.api.responses.ordering.ReceiptItemResponseModel
import com.easipos.tabletordering.api.responses.ordering.ReceiptItemSelectionResponseModel
import com.easipos.tabletordering.api.responses.ordering.ReceiptResponseModel
import com.easipos.tabletordering.mapper.Mapper
import com.easipos.tabletordering.models.Receipt
import com.easipos.tabletordering.models.ReceiptItem
import com.easipos.tabletordering.models.ReceiptItemSelection

class ReceiptMapper : Mapper<Receipt, ReceiptResponseModel>() {

    private val receiptItemMapper by lazy { ReceiptItemMapper() }

    override fun transform(item: ReceiptResponseModel): Receipt {
        return Receipt(
            tableNo = item.tableNo ?: "",
            total = item.total ?: "",
            subTotal = item.subTotal ?: "",
            tax1Name = item.tax1Name ?: "",
            tax2Name = item.tax2Name ?: "",
            tax1 = item.tax1 ?: "",
            tax2 = item.tax2 ?: "",
            totalQty = item.totalQty ?: "",
            discount = item.discount ?: "",
            receiptNo = item.receiptNo ?: "",
            datetime = item.datetime ?: "",
            address = item.address ?: emptyList(),
            items = if (item.items == null) emptyList() else receiptItemMapper.transform(item.items)
        )
    }
}

private class ReceiptItemMapper : Mapper<ReceiptItem, ReceiptItemResponseModel>() {

    private val receiptItemSelectionMapper by lazy { ReceiptItemSelectionMapper() }

    override fun transform(item: ReceiptItemResponseModel): ReceiptItem {
        return ReceiptItem(
            productName = item.productName ?: "",
            quantity = item.quantity ?: "",
            price = item.price ?: "",
            amount = item.amount ?: "",
            remarks = item.remarks ?: "",
            selections = if (item.selections == null) emptyList() else receiptItemSelectionMapper.transform(item.selections)
        )
    }
}

private class ReceiptItemSelectionMapper : Mapper<ReceiptItemSelection, ReceiptItemSelectionResponseModel>() {

    override fun transform(item: ReceiptItemSelectionResponseModel): ReceiptItemSelection {
        return ReceiptItemSelection(
            productName = item.productName ?: "",
            quantity = item.quantity ?: "",
            price = item.price ?: "",
            amount = item.amount ?: "",
            remarks = item.remarks ?: ""
        )
    }
}