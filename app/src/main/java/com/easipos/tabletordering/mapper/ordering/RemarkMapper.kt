package com.easipos.tabletordering.mapper.ordering

import com.easipos.tabletordering.api.responses.ordering.OrderItemResponseModel
import com.easipos.tabletordering.api.responses.ordering.ProductResponseModel
import com.easipos.tabletordering.api.responses.ordering.RemarkResponseModel
import com.easipos.tabletordering.mapper.Mapper
import com.easipos.tabletordering.models.OrderItem
import com.easipos.tabletordering.models.Product
import com.easipos.tabletordering.models.Remark

class RemarkMapper : Mapper<Remark, RemarkResponseModel>() {

    override fun transform(item: RemarkResponseModel): Remark {
        return Remark(
            kitchenRequestCode = item.kitchenRequestCode ?: "",
            kitchenRequestDescription = item.kitchenRequestDescription ?: "",
            isSelected = false
        )
    }
}