package com.easipos.tabletordering.mapper.auth

import com.easipos.tabletordering.api.responses.auth.LoginInfoResponseModel
import com.easipos.tabletordering.mapper.Mapper
import com.easipos.tabletordering.models.LoginInfo

class LoginInfoMapper : Mapper<LoginInfo, LoginInfoResponseModel>() {

    override fun transform(item: LoginInfoResponseModel): LoginInfo {
        return LoginInfo(
            tableName = item.tableName ?: "",
            tblSysId = item.tblSysId ?: "",
            currency = item.currency ?: ""
        )
    }
}