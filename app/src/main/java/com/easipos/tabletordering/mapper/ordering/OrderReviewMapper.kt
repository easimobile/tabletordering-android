package com.easipos.tabletordering.mapper.ordering

import com.easipos.tabletordering.api.responses.ordering.OrderReviewResponseModel
import com.easipos.tabletordering.constant.OrderReviewStatus
import com.easipos.tabletordering.mapper.Mapper
import com.easipos.tabletordering.models.OrderReview

class OrderReviewMapper : Mapper<List<OrderReview>, OrderReviewResponseModel>() {

    override fun transform(item: OrderReviewResponseModel): List<OrderReview> {
        return arrayListOf<OrderReview>().apply {
            this.addAll(item.prepare.map {
                OrderReview(
                    orderNumber = it.orderNumber ?: "",
                    prodNm = it.prodNm ?: "",
                    prodNmCh = it.prodNmCh ?: "",
                    remarks = it.remarks ?: "",
                    status = OrderReviewStatus.PREPARE,
                    imageUrl = it.imageUrl ?: ""
                )
            })

            this.addAll(item.ready.map {
                OrderReview(
                    orderNumber = it.orderNumber ?: "",
                    prodNm = it.prodNm ?: "",
                    prodNmCh = it.prodNmCh ?: "",
                    remarks = it.remarks ?: "",
                    status = OrderReviewStatus.READY,
                    imageUrl = it.imageUrl ?: ""
                )
            })
        }
    }
}