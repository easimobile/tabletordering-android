package com.easipos.tabletordering.mapper.ordering

import com.easipos.tabletordering.api.responses.ordering.MenuResponseModel
import com.easipos.tabletordering.mapper.Mapper
import com.easipos.tabletordering.models.Menu

class MenuMapper : Mapper<Menu, MenuResponseModel>() {

    override fun transform(item: MenuResponseModel): Menu {
        return Menu(
            deptCd = item.deptCd ?: "",
            deptNm = item.deptNm ?: "",
            deptCh = item.deptCh ?: "",
            dispOrder = item.dispOrder ?: "",
            deptImg = item.deptImg ?: ""
        )
    }
}