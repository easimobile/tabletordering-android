package com.easipos.tabletordering.mapper.ordering

import com.easipos.tabletordering.api.responses.ordering.SetOccupiedSessionResponseModel
import com.easipos.tabletordering.mapper.Mapper
import com.easipos.tabletordering.models.SetOccupiedSession

class SetOccupiedSessionMapper : Mapper<SetOccupiedSession, SetOccupiedSessionResponseModel>() {

    override fun transform(item: SetOccupiedSessionResponseModel): SetOccupiedSession {
        return SetOccupiedSession(
            pax = item.pax?.toIntOrNull() ?: 0
        )
    }
}