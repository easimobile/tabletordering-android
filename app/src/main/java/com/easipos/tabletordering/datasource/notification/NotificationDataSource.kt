package com.easipos.tabletordering.datasource.notification

import com.easipos.tabletordering.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.tabletordering.api.requests.notification.RemoveFcmTokenRequestModel
import com.easipos.tabletordering.api.services.Api
import com.easipos.tabletordering.executor.PostExecutionThread
import com.easipos.tabletordering.executor.ThreadExecutor
import io.reactivex.Completable

class NotificationDataSource(private val api: Api,
                             private val threadExecutor: ThreadExecutor,
                             private val postExecutionThread: PostExecutionThread) : NotificationDataStore {

    override fun registerFcmToken(model: RegisterFcmTokenRequestModel): Completable =
        api.registerFcmToken(model.toFormDataBuilder().build())

    override fun removeFcmToken(model: RemoveFcmTokenRequestModel): Completable =
        api.removeFcmToken(model.toFormDataBuilder().build())
}
