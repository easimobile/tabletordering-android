package com.easipos.tabletordering.datasource.ordering

import android.app.Application
import android.os.AsyncTask
import com.easipos.tabletordering.api.misc.EmptySingleObserverRetrofit
import com.easipos.tabletordering.api.misc.SingleObserverRetrofit
import com.easipos.tabletordering.api.requests.ordering.*
import com.easipos.tabletordering.api.responses.ordering.*
import com.easipos.tabletordering.api.services.Api
import com.easipos.tabletordering.executor.PostExecutionThread
import com.easipos.tabletordering.executor.ThreadExecutor
import com.easipos.tabletordering.mapper.ordering.*
import com.easipos.tabletordering.models.*
import com.easipos.tabletordering.room.RoomService
import com.easipos.tabletordering.use_cases.complete
import com.easipos.tabletordering.use_cases.error
import com.easipos.tabletordering.use_cases.success
import com.orhanobut.logger.Logger
import io.reactivex.Observable
import io.reactivex.Single

class OrderingDataSource(private val application: Application,
                         private val api: Api,
                         private val roomService: RoomService,
                         private val threadExecutor: ThreadExecutor,
                         private val postExecutionThread: PostExecutionThread) : OrderingDataStore {

    override fun getFloors(floorMapper: FloorMapper): Single<List<Floor>> =
        Single.create { emitter ->
            api.getFloors()
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<List<FloorResponseModel>>() {
                    override fun onResponseSuccess(responseData: List<FloorResponseModel>) {
                        Logger.i("Floors retrieved")

                        emitter.success(floorMapper.transform(responseData))
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }

    override fun getTables(model: GetTablesRequestModel, tableMapper: TableMapper): Single<List<Table>> =
        Single.create { emitter ->
            api.getTables(model.floor)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<List<TableResponseModel>>() {
                    override fun onResponseSuccess(responseData: List<TableResponseModel>) {
                        Logger.i("Tables retrieved")

                        emitter.success(tableMapper.transform(responseData))
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }

    override fun unlockTable(model: UnlockTableRequestModel): Observable<Void> =
        Observable.create { emitter ->
            api.unlockTable(model)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : EmptySingleObserverRetrofit() {
                    override fun onResponseSuccess() {
                        Logger.i("Table ${model.tblname} unlocked")

                        emitter.complete()
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }

    override fun saveSelectedTable(model: SaveSelectedTableRequestModel, saveSelectedTableMapper: SaveSelectedTableMapper): Single<SaveSelectedTable> =
        Single.create { emitter ->
            api.saveSelectedTable(model)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<SaveSelectedTableResponseModel>() {
                    override fun onResponseSuccess(responseData: SaveSelectedTableResponseModel) {
                        Logger.i("Table ${model.tblname} saved")

                        emitter.success(saveSelectedTableMapper.transform(responseData))
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }

    override fun setOccupiedSession(model: SetOccupiedSessionRequestModel, setOccupiedSessionMapper: SetOccupiedSessionMapper): Single<SetOccupiedSession> =
        Single.create { emitter ->
            api.setOccupiedSession(model)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<SetOccupiedSessionResponseModel>() {
                    override fun onResponseSuccess(responseData: SetOccupiedSessionResponseModel) {
                        Logger.i("Table ${model.tblname} occupied session")

                        emitter.success(setOccupiedSessionMapper.transform(responseData))
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }

    override fun getOrderReview(model: GetOrderReviewRequestModel, orderReviewMapper: OrderReviewMapper): Single<List<OrderReview>> =
        Single.create { emitter ->
            api.getOrderReview(model.userCode)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<OrderReviewResponseModel>() {
                    override fun onResponseSuccess(responseData: OrderReviewResponseModel) {
                        Logger.i("Order review retrieved")

                        emitter.success(orderReviewMapper.transform(responseData))
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }

    override fun getMenu(menuMapper: MenuMapper): Single<List<Menu>> =
        Single.create { emitter ->
            api.getMenu()
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<List<MenuResponseModel>>() {
                    override fun onResponseSuccess(responseData: List<MenuResponseModel>) {
                        Logger.i("Menu retrieved")

                        emitter.success(menuMapper.transform(responseData))
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }

    override fun getNewProducts(model: GetNewProductsRequestModel, productMapper: ProductMapper): Single<List<Product>> =
        Single.create { emitter ->
            api.getNewProducts(model.deptCd)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<List<ProductResponseModel>>() {
                    override fun onResponseSuccess(responseData: List<ProductResponseModel>) {
                        Logger.i("Products retrieved for ${model.deptCd}")

                        AsyncTask.execute {
                            roomService.runInTransaction {
                                roomService.orderingDao().apply {
                                    val products = productMapper.transform(responseData)
                                    products.forEach { product ->
                                        product.deptCode = model.deptCd
                                    }
                                    this.insertProducts(products)

                                    emitter.success(products)
                                }
                            }
                        }
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }

    override fun getProductSet(model: GetProductSetRequestModel, productSetMapper: ProductSetMapper): Single<List<ProductSet>> =
        Single.create { emitter ->
            api.getProductSet(model)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<List<ProductSetResponseModel>>() {
                    override fun onResponseSuccess(responseData: List<ProductSetResponseModel>) {
                        Logger.i("Product set retrieved for ${model.prodCode}")

                        emitter.success(productSetMapper.transform(responseData))
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }

    override fun getCurrentProducts(deptCode: String): Single<List<Product>> =
        Single.create { emitter ->
            AsyncTask.execute {
                roomService.runInTransaction {
                    roomService.orderingDao().apply {
                        emitter.success(this.findProducts(deptCode))
                    }
                }
            }
        }

    override fun getOrderItems(model: GetOrderItemsRequestModel, orderItemMapper: OrderItemMapper): Single<List<OrderItem>> =
        Single.create { emitter ->
            api.getOrderItems(model.tableName, model.tblSysId)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<List<OrderItemResponseModel>>() {
                    override fun onResponseSuccess(responseData: List<OrderItemResponseModel>) {
                        Logger.i("Order items retrieved")

                        emitter.success(orderItemMapper.transform(responseData))
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }

    override fun voidItem(model: VoidItemRequestModel): Observable<Void> =
        Observable.create { emitter ->
            api.voidItem(model)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : EmptySingleObserverRetrofit() {
                    override fun onResponseSuccess() {
                        Logger.i("Item voided ${model.linkCd}")

                        emitter.complete()
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }

    override fun getRemarks(model: GetRemarksRequestModel, remarkMapper: RemarkMapper): Single<List<Remark>> =
        Single.create { emitter ->
            api.getRemarks(model.prodCode)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<List<RemarkResponseModel>>() {
                    override fun onResponseSuccess(responseData: List<RemarkResponseModel>) {
                        Logger.i("Remarks retrieved for ${model.prodCode}")

                        emitter.success(remarkMapper.transform(responseData))
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.onError(throwable)
                    }
                })
        }

    override fun holdBill(model: HoldBillRequestModel): Observable<Void> =
        Observable.create { emitter ->
            api.holdBill(model)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : EmptySingleObserverRetrofit() {
                    override fun onResponseSuccess() {
                        Logger.i("Bill hold")

                        emitter.complete()
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }

    override fun getGuestCheck(model: GetGuestCheckRequestModel, receiptMapper: ReceiptMapper): Single<Receipt> =
        Single.create { emitter ->
            api.getGuestCheck(model)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<ReceiptResponseModel>() {
                    override fun onResponseSuccess(responseData: ReceiptResponseModel) {
                        Logger.i("Guest check retrieved")

                        emitter.success(receiptMapper.transform(responseData))
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }

    override fun deleteUsingTable(model: DeleteUsingTableRequestModel): Observable<Void> =
        Observable.create { emitter ->
            api.deleteUsingTable(model)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : EmptySingleObserverRetrofit() {
                    override fun onResponseSuccess() {
                        Logger.i("Table removed")

                        emitter.complete()
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }
}
