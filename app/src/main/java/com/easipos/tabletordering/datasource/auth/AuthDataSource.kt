package com.easipos.tabletordering.datasource.auth

import com.easipos.tabletordering.api.misc.SingleObserverRetrofit
import com.easipos.tabletordering.api.requests.auth.LoginRequestModel
import com.easipos.tabletordering.api.responses.auth.LoginInfoResponseModel
import com.easipos.tabletordering.api.services.Api
import com.easipos.tabletordering.executor.PostExecutionThread
import com.easipos.tabletordering.executor.ThreadExecutor
import com.easipos.tabletordering.mapper.auth.LoginInfoMapper
import com.easipos.tabletordering.models.LoginInfo
import com.easipos.tabletordering.use_cases.error
import com.easipos.tabletordering.use_cases.success
import com.orhanobut.logger.Logger
import io.reactivex.Single

class AuthDataSource(private val api: Api,
                     private val threadExecutor: ThreadExecutor,
                     private val postExecutionThread: PostExecutionThread) : AuthDataStore {

    override fun login(model: LoginRequestModel, loginInfoMapper: LoginInfoMapper): Single<LoginInfo> =
        Single.create { emitter ->
            api.login(model)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<LoginInfoResponseModel>() {
                    override fun onResponseSuccess(responseData: LoginInfoResponseModel) {
                        Logger.i("User logged in")

                        emitter.success(loginInfoMapper.transform(responseData))
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }
}
