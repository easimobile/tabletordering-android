package com.easipos.tabletordering.datasource

import android.app.Application
import com.easipos.tabletordering.api.services.Api
import com.easipos.tabletordering.datasource.auth.AuthDataSource
import com.easipos.tabletordering.datasource.auth.AuthDataStore
import com.easipos.tabletordering.datasource.notification.NotificationDataSource
import com.easipos.tabletordering.datasource.notification.NotificationDataStore
import com.easipos.tabletordering.datasource.ordering.OrderingDataSource
import com.easipos.tabletordering.datasource.ordering.OrderingDataStore
import com.easipos.tabletordering.datasource.precheck.PrecheckDataSource
import com.easipos.tabletordering.datasource.precheck.PrecheckDataStore
import com.easipos.tabletordering.executor.PostExecutionThread
import com.easipos.tabletordering.executor.ThreadExecutor
import com.easipos.tabletordering.room.RoomService

class DataFactory(private val application: Application,
                                      private val api: Api,
                                      private val roomService: RoomService,
                                      private val threadExecutor: ThreadExecutor,
                                      private val postExecutionThread: PostExecutionThread) {

    fun createPrecheckDataSource(): PrecheckDataStore =
        PrecheckDataSource(api, threadExecutor, postExecutionThread)

    fun createAuthDataSource(): AuthDataStore =
        AuthDataSource(api, threadExecutor, postExecutionThread)

    fun createOrderingDataSource(): OrderingDataStore =
        OrderingDataSource(application, api, roomService, threadExecutor, postExecutionThread)

    fun createNotificationDataSource(): NotificationDataStore =
        NotificationDataSource(api, threadExecutor, postExecutionThread)
}
