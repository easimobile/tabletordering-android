package com.easipos.tabletordering.datasource.precheck

import com.easipos.tabletordering.api.misc.SingleObserverRetrofit
import com.easipos.tabletordering.api.requests.precheck.CheckVersionRequestModel
import com.easipos.tabletordering.api.services.Api
import com.easipos.tabletordering.executor.PostExecutionThread
import com.easipos.tabletordering.executor.ThreadExecutor
import com.easipos.tabletordering.use_cases.error
import com.easipos.tabletordering.use_cases.success
import com.orhanobut.logger.Logger
import io.reactivex.Single

class PrecheckDataSource(private val api: Api,
                         private val threadExecutor: ThreadExecutor,
                         private val postExecutionThread: PostExecutionThread) : PrecheckDataStore {

    override fun checkVersion(model: CheckVersionRequestModel): Single<Boolean> =
        Single.create { emitter ->
            api.checkVersion(model.toFormDataBuilder().build())
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<Boolean>() {
                    override fun onResponseSuccess(responseData: Boolean) {
                        Logger.i("Version checked: $responseData")

                        emitter.success(responseData)
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }
}
