package com.easipos.tabletordering.datasource.notification

import com.easipos.tabletordering.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.tabletordering.api.requests.notification.RemoveFcmTokenRequestModel
import io.reactivex.Completable

interface NotificationDataStore {

    fun registerFcmToken(model: RegisterFcmTokenRequestModel): Completable

    fun removeFcmToken(model: RemoveFcmTokenRequestModel): Completable
}
