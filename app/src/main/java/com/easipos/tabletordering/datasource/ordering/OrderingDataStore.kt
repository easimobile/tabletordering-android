package com.easipos.tabletordering.datasource.ordering

import com.easipos.tabletordering.api.requests.ordering.*
import com.easipos.tabletordering.mapper.ordering.*
import com.easipos.tabletordering.models.*
import io.reactivex.Observable
import io.reactivex.Single

interface OrderingDataStore {

    fun getFloors(floorMapper: FloorMapper): Single<List<Floor>>

    fun getTables(model: GetTablesRequestModel, tableMapper: TableMapper): Single<List<Table>>

    fun unlockTable(model: UnlockTableRequestModel): Observable<Void>

    fun saveSelectedTable(model: SaveSelectedTableRequestModel, saveSelectedTableMapper: SaveSelectedTableMapper): Single<SaveSelectedTable>

    fun setOccupiedSession(model: SetOccupiedSessionRequestModel, setOccupiedSessionMapper: SetOccupiedSessionMapper): Single<SetOccupiedSession>

    fun getOrderReview(model: GetOrderReviewRequestModel, orderReviewMapper: OrderReviewMapper): Single<List<OrderReview>>

    fun getMenu(menuMapper: MenuMapper): Single<List<Menu>>

    fun getNewProducts(model: GetNewProductsRequestModel, productMapper: ProductMapper): Single<List<Product>>

    fun getProductSet(model: GetProductSetRequestModel, productSetMapper: ProductSetMapper): Single<List<ProductSet>>

    fun getCurrentProducts(deptCode: String): Single<List<Product>>

    fun getOrderItems(model: GetOrderItemsRequestModel, orderItemMapper: OrderItemMapper): Single<List<OrderItem>>

    fun voidItem(model: VoidItemRequestModel): Observable<Void>

    fun getRemarks(model: GetRemarksRequestModel, remarkMapper: RemarkMapper): Single<List<Remark>>

    fun holdBill(model: HoldBillRequestModel): Observable<Void>

    fun getGuestCheck(model: GetGuestCheckRequestModel, receiptMapper: ReceiptMapper): Single<Receipt>

    fun deleteUsingTable(model: DeleteUsingTableRequestModel): Observable<Void>
}
