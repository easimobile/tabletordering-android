package com.easipos.tabletordering.datasource.auth

import com.easipos.tabletordering.api.requests.auth.LoginRequestModel
import com.easipos.tabletordering.mapper.auth.LoginInfoMapper
import com.easipos.tabletordering.models.LoginInfo
import io.reactivex.Single

interface AuthDataStore {

    fun login(model: LoginRequestModel, loginInfoMapper: LoginInfoMapper): Single<LoginInfo>
}
