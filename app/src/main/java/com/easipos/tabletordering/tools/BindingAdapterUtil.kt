package com.easipos.tabletordering.tools

import android.graphics.Color
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.easipos.tabletordering.R
import com.easipos.tabletordering.util.defaultRequestOption
import com.easipos.tabletordering.util.loadImageBase64
import io.github.anderscheow.library.kotlinExt.findColor

object BindingAdapterUtil {

    @BindingAdapter("loadMenuImage")
    @JvmStatic
    fun loadMenuImage(appCompatImageView: AppCompatImageView, imageStr: String?) {
        if (imageStr != null && imageStr.isNotBlank()) {
            Glide.with(appCompatImageView.context)
                .load(imageStr)
                .apply(defaultRequestOption().override(72))
                .transition(DrawableTransitionOptions().crossFade(0))
                .into(appCompatImageView)
        } else {
            appCompatImageView.setImageDrawable(null)
        }
    }

    @BindingAdapter("setMenuSelected")
    @JvmStatic
    fun setMenuSelected(appCompatImageView: AppCompatImageView, isSelected: Boolean) {
        if (isSelected) {
            appCompatImageView.setColorFilter(Color.WHITE)
        } else {
            appCompatImageView.setColorFilter(appCompatImageView.context.findColor(R.color.colorTVPrimary))
        }
    }

    @BindingAdapter("loadProductImage")
    @JvmStatic
    fun loadProductImage(appCompatImageView: AppCompatImageView, imageStr: String?) {
        if (imageStr != null && imageStr.isNotBlank()) {
            appCompatImageView.loadImageBase64(imageStr)
        } else {
            appCompatImageView.setImageDrawable(null)
        }
    }
}