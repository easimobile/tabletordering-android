package com.easipos.tabletordering.tools

import com.easipos.tabletordering.BuildConfig
import com.pixplicity.easyprefs.library.Prefs

object Preference {

    private const val PREF_LANGUAGE_CODE = "PREF_LANGUAGE_CODE"
    private const val PREF_ACCESS_TOKEN = "PREF_ACCESS_TOKEN"
    private const val PREF_IS_LOGGED_IN = "PREF_IS_LOGGED_IN"
    private const val PREF_IS_FCM_REGISTERED = "PREF_IS_FCM_REGISTERED"
    private const val PREF_FCM_TOKEN = "PREF_FCM_TOKEN"
    private const val PREF_NOTIFICATION_COUNT = "PREF_NOTIFICATION_COUNT"
    private const val PREF_TABLE_NAME = "PREF_TABLE_NAME"
    private const val PREF_TABLE_SYS_ID = "PREF_TABLE_SYS_ID"
    private const val PREF_USER_CODE = "PREF_USER_CODE"
    private const val PREF_SETTING_NUM_PRODUCT_PER_ROW = "PREF_SETTING_NUM_PRODUCT_PER_ROW"
    private const val PREF_SETTING_SHOW_PRICE = "PREF_SETTING_SHOW_PRICE"
    private const val PREF_SETTING_SHOW_QUANTITY_ADD_BUTTON = "PREF_SETTING_SHOW_QUANTITY_ADD_BUTTON"
    private const val PREF_SETTING_CURRENCY = "PREF_SETTING_CURRENCY"
    private const val PREF_SETTING_REQUIRE_PAX = "PREF_SETTING_REQUIRE_PAX"
    private const val PREF_CUSTOM_BASE_URL = "PREF_CUSTOM_BASE_URL"
    private const val PREF_CURRENT_PAX = "PREF_CURRENT_PAX"
    private const val PREF_DEFAULT_FLOOR = "PREF_DEFAULT_FLOOR"

    var prefLanguageCode: String
        get() = Prefs.getString(PREF_LANGUAGE_CODE, "en")
        set(languageCode) = Prefs.putString(PREF_LANGUAGE_CODE, languageCode)

    var prefAccessToken: String
        get() = Prefs.getString(PREF_ACCESS_TOKEN, "")
        set(accessToken) = Prefs.putString(PREF_ACCESS_TOKEN, accessToken)

    var prefIsLoggedIn: Boolean
        get() = Prefs.getBoolean(PREF_IS_LOGGED_IN, false)
        set(isLoggedIn) = Prefs.putBoolean(PREF_IS_LOGGED_IN, isLoggedIn)

    var prefIsFcmTokenRegistered: Boolean
        get() = Prefs.getBoolean(PREF_IS_FCM_REGISTERED, false)
        set(isRegistered) = Prefs.putBoolean(PREF_IS_FCM_REGISTERED, isRegistered)

    var prefFcmToken: String
        get() = Prefs.getString(PREF_FCM_TOKEN, "")
        set(token) = Prefs.putString(PREF_FCM_TOKEN, token)

    var prefNotificationCount: Int
        get() = Prefs.getInt(PREF_NOTIFICATION_COUNT, 0)
        set(count) = Prefs.putInt(PREF_NOTIFICATION_COUNT, count)

    var prefTableName: String
        get() = Prefs.getString(PREF_TABLE_NAME, "")
        set(tableName) = Prefs.putString(PREF_TABLE_NAME, tableName)

    var prefTableSysId: String
        get() = Prefs.getString(PREF_TABLE_SYS_ID, "")
        set(tableSysId) = Prefs.putString(PREF_TABLE_SYS_ID, tableSysId)

    var prefUserCode: String
        get() = Prefs.getString(PREF_USER_CODE, "")
        set(userCode) = Prefs.putString(PREF_USER_CODE, userCode)

    var prefSettingNumProductPerRow: Int
        get() = Prefs.getInt(PREF_SETTING_NUM_PRODUCT_PER_ROW, 3)
        set(row) = Prefs.putInt(PREF_SETTING_NUM_PRODUCT_PER_ROW, row)

    var prefSettingShowPrice: Boolean
        get() = Prefs.getBoolean(PREF_SETTING_SHOW_PRICE, true)
        set(showPrice) = Prefs.putBoolean(PREF_SETTING_SHOW_PRICE, showPrice)

    var prefSettingShowQuantityAddButton: Boolean
        get() = Prefs.getBoolean(PREF_SETTING_SHOW_QUANTITY_ADD_BUTTON, true)
        set(showQuantity) = Prefs.putBoolean(PREF_SETTING_SHOW_QUANTITY_ADD_BUTTON, showQuantity)

    var prefSettingCurrency: String
        get() = Prefs.getString(PREF_SETTING_CURRENCY, "$")
        set(currency) = Prefs.putString(PREF_SETTING_CURRENCY, currency)

    var prefSettingRequirePax: Boolean
        get() = Prefs.getBoolean(PREF_SETTING_REQUIRE_PAX, false)
        set(requirePax) = Prefs.putBoolean(PREF_SETTING_REQUIRE_PAX, requirePax)

    var prefCustomBaseUrl: String
        get() = Prefs.getString(PREF_CUSTOM_BASE_URL, BuildConfig.API_DOMAIN)
        set(url) = Prefs.putString(PREF_CUSTOM_BASE_URL, url)

    var prefCurrentPax: Int
        get() = Prefs.getInt(PREF_CURRENT_PAX, 0)
        set(pax) = Prefs.putInt(PREF_CURRENT_PAX, pax)

    var prefDefaultFloor: String
        get() = Prefs.getString(PREF_DEFAULT_FLOOR, "")
        set(floor) = Prefs.putString(PREF_DEFAULT_FLOOR, floor)

    fun logout() {
        prefIsLoggedIn = false
        prefIsFcmTokenRegistered = false
        prefNotificationCount = 0
        prefTableName = ""
        prefTableSysId = ""
        prefUserCode = ""
        prefCurrentPax = 0

        Prefs.remove(PREF_ACCESS_TOKEN)
    }
}