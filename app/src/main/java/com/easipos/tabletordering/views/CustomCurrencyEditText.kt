package com.easipos.tabletordering.views

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.ActionMode
import android.view.Menu
import android.view.MenuItem
import android.view.View.OnFocusChangeListener
import androidx.appcompat.widget.AppCompatEditText
import com.easipos.tabletordering.tools.Preference
import com.orhanobut.logger.Logger
import io.github.anderscheow.library.kotlinExt.click

class CustomCurrencyEditText : AppCompatEditText {

    private val currency = Preference.prefSettingCurrency

    private var textChangedListener = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            val str = s?.toString() ?: ""
            if (str.startsWith(currency).not()) {   // Check if start with currency
                val newStr = "$currency $str"
                setText(newStr)
                setSelection(newStr.length)
            } else if (str.length == currency.length) {   // Check if contain space after currency
                val newStr = "$currency "
                setText(newStr)
                setSelection(newStr.length)
                return
            } else if (str.length == "$currency $str".length) {   // Check if after (currency + blank space) start with dot
                val strAfterCurrency = str.substring(2)
                if (strAfterCurrency == ".") {
                    val newStr = "$currency "
                    setText(newStr)
                    setSelection(newStr.length)
                    return
                }
            } else if (str.length > "$currency $str".length) {
                val strOld = str.substring(0, str.length - 1)
                val lastChar = str.last().toString()

                // Check if contain 2 dots
                if (strOld.contains(".") && lastChar == ".") {
                    setText(strOld)
                    setSelection(strOld.length)
                    return
                }

                val firstIndexOfDot = str.indexOf(".")
                if (firstIndexOfDot != -1) {
                    val textOnFirstDot = str.substring(str.indexOf("."))
                    val textBeforeFirstDot = str.substring(0, str.indexOf("."))

                    if (textOnFirstDot.length > 3) {
                        Logger.d(textOnFirstDot.substring(0, 3))

                        val newStr = textBeforeFirstDot + textOnFirstDot.substring(0, 3)
                        setText(newStr)
                        setSelection(newStr.length)
                        return
                    }
                }
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
    }

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    override fun isSuggestionsEnabled(): Boolean {
        return false
    }

    fun getDoubleOrNull(): Double? {
        return if (text == null) {
            null
        } else {
            text.toString()
                .replace(currency, "")
                .replace(" ", "")
                .toDoubleOrNull()
        }
    }

    private fun init() {
        isCursorVisible = true
        isLongClickable = false

        onFocusChangeListener = OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                selectAll()
            }
        }

        addTextChangedListener(textChangedListener)

        customSelectionActionModeCallback = object : ActionMode.Callback {
            override fun onActionItemClicked(p0: ActionMode?, p1: MenuItem?): Boolean {
                return false
            }

            override fun onCreateActionMode(p0: ActionMode?, p1: Menu?): Boolean {
                return false
            }

            override fun onPrepareActionMode(p0: ActionMode?, p1: Menu?): Boolean {
                return false
            }

            override fun onDestroyActionMode(p0: ActionMode?) {
            }
        }

        //setOnEditorActionListener { _, _, _ -> true }

        click {
            setCursorToEnd()
        }

        setCursorToEnd()
    }

    private fun setCursorToEnd() {
        post {
            setSelection(length())
        }
    }
}