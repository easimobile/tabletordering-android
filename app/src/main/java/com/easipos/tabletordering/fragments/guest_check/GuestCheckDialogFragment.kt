package com.easipos.tabletordering.fragments.guest_check

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.easipos.tabletordering.R
import com.easipos.tabletordering.adapters.ReceiptItemAdapter
import com.easipos.tabletordering.base.CustomBaseDialogFragment
import com.easipos.tabletordering.bundle.ParcelData
import com.easipos.tabletordering.models.Receipt
import com.easipos.tabletordering.tools.EqualSpacingItemDecoration
import com.easipos.tabletordering.tools.Preference
import io.github.anderscheow.library.appCompat.activity.FoundationAppCompatActivity
import io.github.anderscheow.library.kotlinExt.*
import kotlinx.android.synthetic.main.dialog_fragment_guest_check.*
import org.jetbrains.anko.displayMetrics

class GuestCheckDialogFragment : CustomBaseDialogFragment() {

    companion object {
        fun show(activity: FoundationAppCompatActivity, receipt: Receipt) {
            activity.removeDialogFragmentThen(GuestCheckDialogFragment.TAG) {
                GuestCheckDialogFragment().apply {
                    arguments = Bundle().apply {
                        this.putParcelable(ParcelData.RECEIPT, receipt)
                    }
                }
            }
        }
    }

    private val receipt by argument<Receipt>(ParcelData.RECEIPT)

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            this.setCanceledOnTouchOutside(true)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        withActivity { activity ->
            dialog?.window?.setLayout((activity.displayMetrics.widthPixels * 0.95).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        }
    }

    override fun getResLayout(): Int = R.layout.dialog_fragment_guest_check

    override fun init() {
        super.init()
        setupListeners()
        setupViews()
    }

    private fun setupViews() {
        setupRecyclerView()

        receipt?.let { receipt ->
            text_view_address.text = receipt.formatAddress()
            text_view_invoice_no.text = getString(R.string.label_invoice_no, receipt.receiptNo)
            text_view_table_no.text = getString(R.string.label_table_no, receipt.tableNo)
            text_view_price.text = getString(R.string.label_price_label, Preference.prefSettingCurrency)
            text_view_amount.text = getString(R.string.label_amount_label, Preference.prefSettingCurrency)
            text_view_total_quantity.text = getString(R.string.label_total_quantity, receipt.formatQuantity())
            text_view_date.text = getString(R.string.label_date, receipt.formatDate())
            text_view_time.text = getString(R.string.label_time, receipt.formatTime())
            text_view_subtotal.text = getString(R.string.label_subtotal, receipt.formatSubtotal())
            text_view_tax.text = getString(R.string.label_tax, receipt.formatTaxName(), receipt.formatTax())
            text_view_total.text = getString(R.string.label_total_2, receipt.formatTotal())
        }
    }

    private fun setupListeners() {
        layout_root.click {
            dismissAllowingStateLoss()
        }
    }

    private fun setupRecyclerView() {
        withContext { context ->
            val adapter = ReceiptItemAdapter(context)
            recycler_view_receipt_item.apply {
                this.adapter = adapter
                this.layoutManager = LinearLayoutManager(context)
                this.addItemDecoration(EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.VERTICAL))
            }
            adapter.items = receipt?.items?.toMutableList() ?: mutableListOf()
        }
    }
}