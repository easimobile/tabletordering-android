package com.easipos.tabletordering.fragments.alert_dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import com.easipos.tabletordering.R
import com.easipos.tabletordering.base.CustomAlertDialog
import com.easipos.tabletordering.models.Table
import io.github.anderscheow.library.appCompat.activity.FoundationAppCompatActivity
import io.github.anderscheow.library.kotlinExt.click
import kotlinx.android.synthetic.main.alert_dialog_occupied_table.*

class OccupiedTableAlertDialog(context: Context,
                               private val table: Table,
                               private val onSelectCurrentOrder: () -> Unit,
                               private val onSelectNewOrder: () -> Unit,
                               private val onSelectCancel: () -> Unit) : CustomAlertDialog(context) {

    companion object {
        fun show(activity: FoundationAppCompatActivity, table: Table,
                 onSelectCurrentOrder: () -> Unit, onSelectNewOrder: () -> Unit,
                 onSelectCancel: () -> Unit) {
            activity.displayAlertDialog {
                OccupiedTableAlertDialog(activity, table, onSelectCurrentOrder,
                    onSelectNewOrder, onSelectCancel)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.alert_dialog_occupied_table)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        text_view_message?.text = context.getString(R.string.label_occupied_table_alert, table.tableName)

        button_current_order?.click {
            onSelectCurrentOrder()
            dismiss()
        }

        button_new_order?.click {
            onSelectNewOrder()
            dismiss()
        }

        button_cancel?.click {
            onSelectCancel()
            dismiss()
        }
    }
}