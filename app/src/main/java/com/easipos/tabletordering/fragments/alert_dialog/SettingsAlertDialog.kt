package com.easipos.tabletordering.fragments.alert_dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import com.easipos.tabletordering.R
import com.easipos.tabletordering.activities.main.MainActivity
import com.easipos.tabletordering.base.CustomAlertDialog
import com.easipos.tabletordering.tools.Preference
import io.github.anderscheow.library.kotlinExt.click
import kotlinx.android.synthetic.main.alert_dialog_settings.*

class SettingsAlertDialog(private val activity: MainActivity) : CustomAlertDialog(activity) {

    companion object {
        fun show(activity: MainActivity) {
            activity.displayAlertDialog {
                SettingsAlertDialog(activity)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.alert_dialog_settings)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        switch_show_price?.isChecked = Preference.prefSettingShowPrice
        switch_show_quantity_add_button?.isChecked = Preference.prefSettingShowQuantityAddButton
        switch_require_pax?.isChecked = Preference.prefSettingRequirePax
        segmented_control_num_product_per_row?.invoke {
            this.initialCheckedIndex = when (Preference.prefSettingNumProductPerRow) {
                2 -> 0
                3 -> 1
                4 -> 2
                else -> 0
            }
        }
        segmented_control_currency?.invoke {
            this.initialCheckedIndex = if (Preference.prefSettingCurrency == "$") 0 else 1
        }

        switch_show_price?.setOnCheckedChangeListener { _, isChecked ->
            Preference.prefSettingShowPrice = isChecked
            activity.updateProductAdapter()
        }

        switch_show_quantity_add_button?.setOnCheckedChangeListener { _, isChecked ->
            Preference.prefSettingShowQuantityAddButton = isChecked
            activity.updateProductAdapter()
        }

        switch_require_pax?.setOnCheckedChangeListener { _, isChecked ->
            Preference.prefSettingRequirePax = isChecked
            if (isChecked.not()) {
                Preference.prefCurrentPax = 0
            }
        }

        segmented_control_num_product_per_row?.onSegmentChecked { segment ->
            Preference.prefSettingNumProductPerRow = segment.text.toString().toIntOrNull() ?: 3
            activity.updateProductRow()
        }

        segmented_control_currency?.onSegmentChecked { segment ->
            Preference.prefSettingCurrency = segment.text.toString()
            activity.updateProductAdapter()
            activity.updateCartItemAdapter()
        }

        button_done?.click {
            dismiss()
        }
    }
}