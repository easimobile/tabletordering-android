package com.easipos.tabletordering.fragments.alert_dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import com.easipos.tabletordering.R
import com.easipos.tabletordering.base.CustomAlertDialog
import io.github.anderscheow.library.appCompat.activity.FoundationAppCompatActivity
import io.github.anderscheow.library.kotlinExt.click
import kotlinx.android.synthetic.main.alert_dialog_yes_no.*

class YesNoAlertDialog(context: Context,
                       private val message: CharSequence,
                       private val onNo: () -> Unit,
                       private val onYes: () -> Unit) : CustomAlertDialog(context) {

    companion object {
        fun show(activity: FoundationAppCompatActivity, message: CharSequence,
                 onNo: () -> Unit, onYes: () -> Unit) {
            activity.displayAlertDialog {
                YesNoAlertDialog(activity, message, onNo, onYes)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.alert_dialog_yes_no)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        text_view_message.text = message

        button_no.click {
            onNo()
            dismiss()
        }

        button_yes.click {
            onYes()
            dismiss()
        }
    }
}