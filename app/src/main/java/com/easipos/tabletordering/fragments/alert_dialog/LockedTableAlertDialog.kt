package com.easipos.tabletordering.fragments.alert_dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import com.easipos.tabletordering.R
import com.easipos.tabletordering.base.CustomAlertDialog
import io.github.anderscheow.library.appCompat.activity.FoundationAppCompatActivity
import io.github.anderscheow.library.kotlinExt.click
import kotlinx.android.synthetic.main.alert_dialog_locked_table.*

class LockedTableAlertDialog(context: Context,
                             private val onSelectYes: () -> Unit,
                             private val onSelectNo: () -> Unit) : CustomAlertDialog(context) {

    companion object {
        fun show(activity: FoundationAppCompatActivity, onSelectYes: () -> Unit, onSelectNo: () -> Unit) {
            activity.displayAlertDialog {
                LockedTableAlertDialog(activity, onSelectYes, onSelectNo)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.alert_dialog_locked_table)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        button_yes?.click {
            onSelectYes()
            dismiss()
        }

        button_no?.click {
            onSelectNo()
            dismiss()
        }
    }
}