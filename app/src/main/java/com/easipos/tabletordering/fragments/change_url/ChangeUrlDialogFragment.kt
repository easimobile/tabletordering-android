package com.easipos.tabletordering.fragments.change_url

import android.app.Dialog
import android.os.Bundle
import android.view.ViewGroup
import com.easipos.tabletordering.R
import com.easipos.tabletordering.activities.login.LoginActivity
import com.easipos.tabletordering.base.CustomBaseDialogFragment
import com.easipos.tabletordering.tools.Preference
import io.github.anderscheow.library.appCompat.activity.FoundationAppCompatActivity
import io.github.anderscheow.library.kotlinExt.TAG
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.withContext
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.extensions.validate
import io.github.anderscheow.validator.rules.common.notBlank
import io.github.anderscheow.validator.rules.common.regex
import kotlinx.android.synthetic.main.dialog_fragment_change_url.*

class ChangeUrlDialogFragment : CustomBaseDialogFragment() {

    companion object {
        fun show(activity: FoundationAppCompatActivity) {
            activity.removeDialogFragmentThen(ChangeUrlDialogFragment.TAG) {
                ChangeUrlDialogFragment()
            }
        }
    }

    private val urlRegex = "https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)"

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            this.setCanceledOnTouchOutside(true)
        }
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun getResLayout(): Int = R.layout.dialog_fragment_change_url

    override fun init() {
        super.init()
        setupViews()
        setupListeners()
    }

    private fun setupViews() {
        val url = Preference.prefCustomBaseUrl
        if (url.isNotBlank()) {
            text_input_edit_text_url.setText(url)
        } else {
            text_input_edit_text_url.setText("http://")
        }
    }

    private fun setupListeners() {
        button_ok.click {
            attemptSaveConfiguration()
        }
    }

    private fun attemptSaveConfiguration() {
        withContext { context ->
            val urlValidation = text_input_layout_url.validate()
                .notBlank(R.string.error_field_required)
                .regex(urlRegex, R.string.error_url_format)

            Validator.with(context)
                .setListener(object : Validator.OnValidateListener {
                    override fun onValidateFailed(errors: List<String>) {
                    }

                    override fun onValidateSuccess(values: List<String>) {
                        val url = if (values[0].endsWith("/")) {
                            values[0]
                        } else {
                            values[0] + "/"
                        }

                        (activity as? LoginActivity)?.setCustomBaseUrl(url)
                    }
                }).validate(urlValidation)
        }
    }
}