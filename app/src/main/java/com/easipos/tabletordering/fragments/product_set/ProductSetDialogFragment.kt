package com.easipos.tabletordering.fragments.product_set

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.easipos.tabletordering.R
import com.easipos.tabletordering.adapters.ProductSetAdapter
import com.easipos.tabletordering.base.CustomBaseDialogFragment
import com.easipos.tabletordering.bundle.ParcelData
import com.easipos.tabletordering.constant.Layer
import com.easipos.tabletordering.fragments.alert_dialog.CommonAlertDialog
import com.easipos.tabletordering.models.*
import com.easipos.tabletordering.tools.EqualSpacingItemDecoration
import com.google.gson.Gson
import com.orhanobut.logger.Logger
import io.github.anderscheow.library.appCompat.activity.FoundationAppCompatActivity
import io.github.anderscheow.library.kotlinExt.*
import kotlinx.android.synthetic.main.dialog_fragment_product_set.*
import org.jetbrains.anko.displayMetrics

class ProductSetDialogFragment : CustomBaseDialogFragment() {

    companion object {
        fun show(activity: FoundationAppCompatActivity, product: Product, productSet: List<ProductSet>) {
            activity.removeDialogFragmentThen(ProductSetDialogFragment.TAG) {
                ProductSetDialogFragment().apply {
                    this.arguments = Bundle().apply {
                        this.putParcelable(ParcelData.PRODUCT, product)
                        this.putSerializable(ParcelData.PRODUCT_SET, ArrayList(productSet))
                    }
                }
            }
        }
    }

    private val product by argument<Product>(ParcelData.PRODUCT)
    private val productSet by argument(ParcelData.PRODUCT_SET, arrayListOf<ProductSet>())

    override fun onStart() {
        super.onStart()
        withActivity { activity ->
            dialog?.window?.setLayout((activity.displayMetrics.widthPixels * 1.0).toInt(),
                (activity.displayMetrics.heightPixels * 1.0).toInt())
        }
    }

    override fun getResLayout(): Int = R.layout.dialog_fragment_product_set

    override fun init() {
        super.init()

        withContext { context ->
            text_view_product_name.text = product?.prodShNm
            text_view_product_price.text = product?.formatPrice(context)

            recycler_view_product_set.apply {
                val adapter = ProductSetAdapter(context)

                this.adapter = adapter
                this.layoutManager = LinearLayoutManager(context)
                this.addItemDecoration(EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.VERTICAL))

                val firstLayerProductSet = arrayListOf<ProductSet>()
                val otherLayerProductSet = arrayListOf<ProductSet>()
                val productSetWithLayer = arrayListOf<ProductSetWithLayer>()

                productSet.forEach { item ->
                    if (item.insert) {
                        firstLayerProductSet.add(item)
                    } else {
                        otherLayerProductSet.add(item)
                    }
                }

                productSetWithLayer.add(
                    ProductSetWithLayer(
                        layer = Layer.ONE,
                        productSet = firstLayerProductSet
                    )
                )

                otherLayerProductSet.forEach { item ->
                    productSetWithLayer.add(
                        ProductSetWithLayer(
                            layer = Layer.MULTIPLE,
                            productSet = arrayListOf<ProductSet>().apply {
                                this.add(item)
                            }
                        )
                    )
                }

                adapter.items = productSetWithLayer.toMutableList()
            }
        }

        button_add.click {
            attemptAddProductSet()
        }

        button_cancel.click {
            dismiss()
        }
    }

    private fun attemptAddProductSet() {
        if (product != null) {
            (recycler_view_product_set.adapter as? ProductSetAdapter)?.let { adapter ->
                val selectedProductSet = adapter.getSelectedProductSet()

                val firstLayerProductSet = productSet.filter { it.insert }
                if (firstLayerProductSet.isNotEmpty()) {
                    if (selectedProductSet.isNotEmpty()) {
                        selectedProductSet.forEach { item ->
                            val searchable = firstLayerProductSet.find {
                                it.productCode == item.productCode
                            }

                            if (searchable == null) {
                                showErrorMessage()
                                return@let
                            }
                        }
                    } else {
                        showErrorMessage()
                        return@let
                    }
                }

                val result = checkIsProductSetIncluded(productSet, selectedProductSet)
                if (result.not()) {
                    showErrorMessage()
                    return@let
                }

                Logger.json(Gson().toJson(selectedProductSet))

                Cart.addCartItem(
                    CartItem(
                        product = product?.apply {
                            this.productSet = selectedProductSet
                        }
                    )
                )

                dismissAllowingStateLoss()
            }
        }
    }


    private fun checkIsProductSetIncluded(productSet: List<ProductSet>, productSetToCheck: List<ProductSet>): Boolean {
        var result = true
        val otherLayerProductSet = productSet.filter { it.insert.not() }
        otherLayerProductSet.forEach { item ->
            if (item.quantity > 0) {
                val searchable = productSetToCheck.find {
                    it.parentProductCode == item.productCode
                }

                if (searchable != null) {
                    result = checkIsProductSetIncluded(
                        searchable.selections,
                        productSetToCheck
                    )
                    if (result.not()) {
                        return false
                    }
                } else {
                    return false
                }
            }
        }
        return result
    }

    private fun showErrorMessage() {
        withContext { context ->
            CommonAlertDialog(context, "Incomplete selection").show()
        }
    }
}