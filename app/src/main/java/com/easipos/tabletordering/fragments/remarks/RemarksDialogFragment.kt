package com.easipos.tabletordering.fragments.remarks

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.easipos.tabletordering.R
import com.easipos.tabletordering.activities.main.MainActivity
import com.easipos.tabletordering.adapters.RemarkAdapter
import com.easipos.tabletordering.base.CustomBaseDialogFragment
import com.easipos.tabletordering.bundle.ParcelData
import com.easipos.tabletordering.models.Remark
import com.easipos.tabletordering.tools.GridSpacingItemDecoration
import com.orhanobut.logger.Logger
import io.github.anderscheow.library.appCompat.activity.FoundationAppCompatActivity
import io.github.anderscheow.library.kotlinExt.*
import kotlinx.android.synthetic.main.dialog_fragment_remarks.*
import org.jetbrains.anko.displayMetrics

class RemarksDialogFragment : CustomBaseDialogFragment() {

    companion object {
        fun show(activity: FoundationAppCompatActivity, cartItemIndex: Int, remarks: List<Remark>) {
            activity.removeDialogFragmentThen(RemarksDialogFragment.TAG) {
                RemarksDialogFragment().apply {
                    arguments = Bundle().apply {
                        this.putInt(ParcelData.CART_ITEM_INDEX, cartItemIndex)
                        this.putParcelableArrayList(ParcelData.REMARKS, ArrayList(remarks))
                    }
                }
            }
        }
    }

    private val cartItemIndex by argument<Int>(ParcelData.CART_ITEM_INDEX)
    private val remarks by argument(ParcelData.REMARKS, ArrayList<Remark>())

    private var adapter: RemarkAdapter? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            this.setCanceledOnTouchOutside(true)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        activity?.let { activity ->
            dialog?.window?.setLayout((activity.displayMetrics.widthPixels * 0.5).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        }
    }

    override fun getResLayout(): Int = R.layout.dialog_fragment_remarks

    override fun init() {
        super.init()

        setupViews()
        setupListeners()
    }

    private fun setupViews() {
        remarks.find { remark ->
            remark.kitchenRequestCode.isBlank()
        }?.let { remark ->
            edit_text_remark.setText(remark.kitchenRequestDescription)
        }

        setupRecyclerView()
    }

    private fun setupListeners() {
        layout_root.click {
            activity?.hideKeyboard(view)
        }

        scrollView.viewTreeObserver.addOnScrollChangedListener {
            activity?.hideKeyboard(view)
        }

        image_view_remove.click {
            dismissAllowingStateLoss()
        }

        button_ok.click {
            submitRemarks()
        }
    }

    private fun setupRecyclerView() {
        withContext { context ->
            adapter = RemarkAdapter(context)
            recycler_view_remark.apply {
                this.layoutManager = GridLayoutManager(context, 2)
                this.adapter = this@RemarksDialogFragment.adapter
                this.addItemDecoration(GridSpacingItemDecoration(2, 16, true))
            }
            adapter?.items = remarks.filter { remark ->
                // Remove custom remark to place in the listing
                remark.kitchenRequestCode != ""
            }.toMutableList()
        }
    }

    private fun submitRemarks() {
        val remarks = adapter?.items ?: arrayListOf()
        if (edit_text_remark.text.isNotBlank()) {
            remarks.add(Remark(
                kitchenRequestCode = "",
                kitchenRequestDescription = edit_text_remark.text.toString().trim(),
                isSelected = true
            ))
        }

        Logger.d(remarks)

        if (cartItemIndex != null) {
            (activity as? MainActivity)?.submitRemarks(cartItemIndex!!, remarks)
        }

        dismissAllowingStateLoss()
    }
}