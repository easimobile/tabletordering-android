package com.easipos.tabletordering.fragments.alert_dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import com.easipos.tabletordering.R
import com.easipos.tabletordering.base.CustomAlertDialog
import io.github.anderscheow.library.appCompat.activity.FoundationAppCompatActivity
import io.github.anderscheow.library.kotlinExt.click
import kotlinx.android.synthetic.main.alert_dialog_common.*

class CommonAlertDialog(context: Context,
                        private val message: CharSequence,
                        private val action: (() -> Unit)? = null) : CustomAlertDialog(context) {

    companion object {
        fun show(activity: FoundationAppCompatActivity, message: CharSequence, action: (() -> Unit)?) {
            activity.displayAlertDialog {
                CommonAlertDialog(activity, message, action)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.alert_dialog_common)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        text_view_message.text = message

        button_ok.click {
            action?.invoke()
            dismiss()
        }
    }
}