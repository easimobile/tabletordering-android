package com.easipos.tabletordering.fragments.open_price

import android.os.Bundle
import android.view.ViewGroup
import com.easipos.tabletordering.R
import com.easipos.tabletordering.activities.main.MainActivity
import com.easipos.tabletordering.base.CustomBaseDialogFragment
import com.easipos.tabletordering.bundle.ParcelData
import com.easipos.tabletordering.models.Product
import io.github.anderscheow.library.appCompat.activity.FoundationAppCompatActivity
import io.github.anderscheow.library.kotlinExt.*
import kotlinx.android.synthetic.main.dialog_fragment_open_price.*
import org.jetbrains.anko.displayMetrics

class OpenPriceDialogFragment : CustomBaseDialogFragment() {

    companion object {
        fun show(activity: FoundationAppCompatActivity, product: Product) {
            activity.removeDialogFragmentThen(OpenPriceDialogFragment.TAG) {
                OpenPriceDialogFragment().apply {
                    this.arguments = Bundle().apply {
                        this.putParcelable(ParcelData.PRODUCT, product)
                    }
                }
            }
        }
    }

    private val product by argument<Product>(ParcelData.PRODUCT)

    override fun onStart() {
        super.onStart()
        withActivity { activity ->
            dialog?.window?.setLayout((activity.displayMetrics.widthPixels * 0.9).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        }
    }

    override fun getResLayout(): Int = R.layout.dialog_fragment_open_price

    override fun init() {
        super.init()

        text_input_edit_text_open_price.post {
            text_input_edit_text_open_price.requestFocus()
            context?.showKeyboard(text_input_edit_text_open_price)
        }

        text_input_edit_text_open_price.setText("")

        button_cancel.click {
            dismissAllowingStateLoss()
        }

        button_add.click {
            attemptPerformCashDeclaration()
        }
    }

    private fun attemptPerformCashDeclaration() {
        when (val amount = text_input_edit_text_open_price.getDoubleOrNull()) {
            null -> {
                text_input_layout_open_price.isErrorEnabled = true
                text_input_layout_open_price.error =
                    getString(R.string.error_open_price_wrong_format)
                return
            }
            else -> {
                text_input_layout_open_price.isErrorEnabled = false

                product?.let { product ->
                    (activity as? MainActivity)?.insertProductIntoCart(product, amount)
                    dismissAllowingStateLoss()
                }
            }
        }
    }
}