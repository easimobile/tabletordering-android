package com.easipos.tabletordering.fragments.pax

import android.os.Bundle
import android.view.ViewGroup
import com.easipos.tabletordering.R
import com.easipos.tabletordering.activities.table_selection.TableSelectionActivity
import com.easipos.tabletordering.base.CustomBaseDialogFragment
import com.easipos.tabletordering.bundle.ParcelData
import com.easipos.tabletordering.models.Table
import com.easipos.tabletordering.tools.Preference
import io.github.anderscheow.library.appCompat.activity.FoundationAppCompatActivity
import io.github.anderscheow.library.kotlinExt.*
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.extensions.validate
import io.github.anderscheow.validator.rules.common.notBlank
import kotlinx.android.synthetic.main.dialog_fragment_pax.*
import org.jetbrains.anko.displayMetrics
import org.kodein.di.generic.instance

class PaxDialogFragment : CustomBaseDialogFragment() {

    companion object {
        fun show(activity: FoundationAppCompatActivity, table: Table) {
            activity.removeDialogFragmentThen(PaxDialogFragment.TAG) {
                PaxDialogFragment().apply {
                    this.arguments = Bundle().apply {
                        this.putParcelable(ParcelData.TABLE, table)
                    }
                }
            }
        }
    }

    private val validator by instance<Validator>()

    private val table by argument<Table>(ParcelData.TABLE)

    override fun onStart() {
        super.onStart()
        withActivity { activity ->
            dialog?.window?.setLayout((activity.displayMetrics.widthPixels * 0.9).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        }
    }

    override fun getResLayout(): Int = R.layout.dialog_fragment_pax

    override fun init() {
        super.init()

        text_input_edit_text_pax.post {
            text_input_edit_text_pax.requestFocus()
            context?.showKeyboard(text_input_edit_text_pax)
        }

        text_input_edit_text_pax.setText("")

        button_cancel.click {
            dismissAllowingStateLoss()
        }

        button_continue.click {
            attemptAddPax()
        }
    }

    private fun attemptAddPax() {
        val paxValidation = text_input_layout_pax.validate()
            .notBlank(R.string.error_field_required)

        validator.setListener(object : Validator.OnValidateListener {
            override fun onValidateFailed(errors: List<String>) {
                dismissAllowingStateLoss()
            }

            override fun onValidateSuccess(values: List<String>) {
                val pax = values[0].toIntOrNull() ?: 0

                Preference.prefCurrentPax = pax

                table?.let { table ->
                    (activity as? TableSelectionActivity)?.saveSelectedTable(table)
                }

                dismissAllowingStateLoss()
            }
        }).validate(paxValidation)
    }
}