package com.easipos.tabletordering.base

import android.app.Application
import androidx.annotation.UiThread
import com.easipos.tabletordering.Easi
import org.kodein.di.KodeinAware
import java.lang.ref.WeakReference

interface MvpPresenter<V : View> {
    @UiThread
    fun onAttachView(view: V)

    @UiThread
    fun onDetachView()
}

open class Presenter<V : View>(val application: Application) : MvpPresenter<V>, KodeinAware {

    override val kodein by (application as Easi).kodein

    private var viewRef: WeakReference<V>? = null

    var view: V? = null
        private set
        get() = viewRef?.get()

    @UiThread
    override fun onAttachView(view: V) {
        viewRef = WeakReference(view)
    }

    @UiThread
    override fun onDetachView() {
        if (viewRef != null) {
            viewRef!!.clear()
            viewRef = null
        }
    }

    @UiThread
    fun isViewAttached(): Boolean {
        return viewRef != null && viewRef!!.get() != null
    }
}