package com.easipos.tabletordering.base

import android.content.Context
import android.os.Bundle
import android.view.View
import com.easipos.tabletordering.Easi
import com.easipos.tabletordering.util.hideSystemUI
import io.github.anderscheow.library.appCompat.activity.LifecycleAppCompatActivity
import io.github.anderscheow.library.viewModel.BaseAndroidViewModel
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein

abstract class CustomLifecycleActivity<VM : BaseAndroidViewModel<*>>
    : LifecycleAppCompatActivity<VM>(), KodeinAware {

    private val _kodein: Kodein by closestKodein()

    override val kodein = Kodein.lazy {
        extend(_kodein)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(Easi.localeManager.setLocale(base))
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) hideSystemUI()
    }

    override fun onStart() {
        super.onStart()
        hideSystemUI()
    }

    override fun init(savedInstanceState: Bundle?) {
        window.decorView.setOnSystemUiVisibilityChangeListener { visibility ->
            if (visibility and View.SYSTEM_UI_FLAG_FULLSCREEN == 0) {
                window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
            }
        }
    }
}
