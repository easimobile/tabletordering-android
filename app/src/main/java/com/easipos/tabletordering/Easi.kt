package com.easipos.tabletordering

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import androidx.core.app.TaskStackBuilder
import androidx.lifecycle.LifecycleObserver
import androidx.multidex.MultiDexApplication
import com.easipos.tabletordering.activities.main.MainActivity
import com.easipos.tabletordering.activities.splash.SplashActivity
import com.easipos.tabletordering.di.modules.*
import com.easipos.tabletordering.managers.UserManager
import com.easipos.tabletordering.tools.LocaleManager
import com.easipos.tabletordering.tools.Preference
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.orhanobut.logger.PrettyFormatStrategy
import com.pixplicity.easyprefs.library.Prefs
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule

class Easi : MultiDexApplication(), LifecycleObserver, KodeinAware {

    companion object {
        val UA = "Android/" + BuildConfig.FLAVOR + BuildConfig.VERSION_CODE + "_" + BuildConfig.BUILD_TYPE +
                " (" +
                Build.MANUFACTURER + " " +
                Build.MODEL + " " +
                Build.VERSION.RELEASE + " " +
                Build.VERSION_CODES::class.java.fields[Build.VERSION.SDK_INT].name +
                ")"
        const val API_ENDPOINT = BuildConfig.API_DOMAIN
        const val API_AUTHORISATION = ""
        const val DATABASE_NAME = "weborder-room"

        var activities = ArrayList<Activity>()

        var isActivityVisible = false

        @SuppressLint("StaticFieldLeak")
        var instance: Easi? = null

        lateinit var localeManager: LocaleManager
    }

    // Use to generate in-app notification
    var currentActivity: Activity? = null

    // Use mainActivity to refresh contents
    var mainActivity: MainActivity? = null
        private set

    override val kodein = Kodein.lazy {
        import(androidXModule(this@Easi))
        import(provideCommonModule(this@Easi))
        import(provideApiModule(UA, Preference.prefCustomBaseUrl, API_AUTHORISATION))
        import(provideDatabaseModule(this@Easi, DATABASE_NAME))

        import(ActivityModule().provideActivityModule())
        import(FragmentModule().provideFragmentModule())
    }

    override fun attachBaseContext(base: Context?) {
        localeManager = LocaleManager(base!!)
        super.attachBaseContext(localeManager.setLocale(base))
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        setupConfiguration()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        localeManager.setLocale(this)
    }

    fun logout() {
        UserManager.token = null
    }

    fun restartAndGotoOnboarding() {
        Logger.d("restartAndGotoOnboarding")
        logout()
        TaskStackBuilder.create(this)
            .addNextIntentWithParentStack(SplashActivity.newIntent(this, true))
            .startActivities()
    }

    private fun setupLifecycleCallback() {
        registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacks {
            override fun onActivityPaused(activity: Activity?) {
            }

            override fun onActivityResumed(activity: Activity?) {
                isActivityVisible = true
                currentActivity = activity
            }

            override fun onActivityStarted(activity: Activity?) {
            }

            override fun onActivityDestroyed(activity: Activity?) {
                activity?.let {
                    activities.remove(activity)
                }
                if (activity is MainActivity) {
                    mainActivity = null
                }
            }

            override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
            }

            override fun onActivityStopped(activity: Activity?) {
            }

            override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
                activity?.let {
                    activities.add(activity)
                }
                if (activity is MainActivity) {
                    mainActivity = activity
                }
            }
        })
    }

    private fun setupConfiguration() {
        setupLifecycleCallback()
        setupFirebaseCrashlytics()
        setupLogger()
        setupPrefs()
    }

    private fun setupFirebaseCrashlytics() {
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(BuildConfig.DEBUG.not())
    }

    private fun setupLogger() {
        val formatStrategy = PrettyFormatStrategy.newBuilder()
            .showThreadInfo(false)
            .build()
        Logger.addLogAdapter(object : AndroidLogAdapter(formatStrategy) {
            override fun isLoggable(priority: Int, tag: String?): Boolean {
                return BuildConfig.DEBUG
            }
        })
    }

    private fun setupPrefs() {
        Prefs.Builder()
            .setContext(this)
            .setMode(ContextWrapper.MODE_PRIVATE)
            .setPrefsName(packageName)
            .setUseDefaultSharedPreference(true)
            .build()
    }
}
