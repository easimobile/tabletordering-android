package com.easipos.tabletordering.api.responses.ordering

import com.google.gson.annotations.SerializedName

data class SaveSelectedTableResponseModel(
    @SerializedName("tblname")
    val tableName: String?,

    @SerializedName("tblsysid")
    val tblSysId: String?
)