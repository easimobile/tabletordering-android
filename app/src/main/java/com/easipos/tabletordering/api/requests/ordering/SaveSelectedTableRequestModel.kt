package com.easipos.tabletordering.api.requests.ordering

import com.easipos.tabletordering.constant.TableStatus
import com.easipos.tabletordering.tools.Preference

data class SaveSelectedTableRequestModel(
    val tblname: String,
    val tblsysid: String,
    val userCode: String = Preference.prefUserCode,
    val status: String = TableStatus.OCCUPIED,
    val pax: Int = Preference.prefCurrentPax
)