package com.easipos.tabletordering.api.requests.ordering

data class GetRemarksRequestModel(
    val prodCode: String
)