package com.easipos.tabletordering.api.responses.ordering

import com.google.gson.annotations.SerializedName

data class ProductSetResponseModel(
    @SerializedName("prod_cd")
    val productCode: String?,

    @SerializedName("quantity")
    val quantity: Int?,

    @SerializedName("prod_nm")
    val productName: String?,

    @SerializedName("prod_nm_ch")
    val productNameCh: String?,

    @SerializedName("price")
    val productPrice: String?,

    @SerializedName("prod_type")
    val prodType: String?,

    @SerializedName("insert")
    val insert: Int?,

    @SerializedName("selections")
    var selections: List<ProductSetResponseModel>?
)