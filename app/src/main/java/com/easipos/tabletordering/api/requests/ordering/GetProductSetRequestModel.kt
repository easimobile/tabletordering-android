package com.easipos.tabletordering.api.requests.ordering

import com.google.gson.annotations.SerializedName

data class GetProductSetRequestModel(
    @SerializedName("prod_cd")
    val prodCode: String
)