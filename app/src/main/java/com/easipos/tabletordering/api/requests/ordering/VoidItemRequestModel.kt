package com.easipos.tabletordering.api.requests.ordering

import com.easipos.tabletordering.tools.Preference
import com.google.gson.annotations.SerializedName

data class VoidItemRequestModel(
    @SerializedName("prod_cd")
    val prodCd: String,

    @SerializedName("link_cd")
    val linkCd: String,

    @SerializedName("tableName")
    val tableName: String = Preference.prefTableName,

    @SerializedName("tblSysId")
    val tblSysId: String = Preference.prefTableSysId
)