package com.easipos.tabletordering.api.requests.ordering

import com.easipos.tabletordering.tools.Preference

data class GetGuestCheckRequestModel(
    val tableName: String = Preference.prefTableName,
    val tblSysId: String = Preference.prefTableSysId
)