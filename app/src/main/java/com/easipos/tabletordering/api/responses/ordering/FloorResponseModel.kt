package com.easipos.tabletordering.api.responses.ordering

import com.google.gson.annotations.SerializedName

data class FloorResponseModel(
    @SerializedName("FloorName")
    val floorName: String?
)