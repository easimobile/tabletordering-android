package com.easipos.tabletordering.api.requests.ordering

import com.easipos.tabletordering.constant.TableStatus

data class SetOccupiedSessionRequestModel(
    val tblname: String,
    val tblsysid: String,
    val status: String = TableStatus.OCCUPIED
)