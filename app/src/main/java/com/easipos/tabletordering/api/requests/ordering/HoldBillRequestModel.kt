package com.easipos.tabletordering.api.requests.ordering

import com.easipos.tabletordering.tools.Preference
import com.google.gson.annotations.SerializedName

data class HoldBillRequestModel(
    @SerializedName("tableName")
    val tableName: String = Preference.prefTableName,

    @SerializedName("tblSysId")
    val tblSysId: String = Preference.prefTableSysId,

    @SerializedName("userCode")
    val userCode: String = Preference.prefUserCode,

    @SerializedName("total")
    val total: String,

    @SerializedName("pax")
    val pax: Int = Preference.prefCurrentPax,

    @SerializedName("items")
    var items: List<HoldBillItemRequestModel> = emptyList()
)

data class HoldBillItemRequestModel(
    @SerializedName("product_code")
    val productCode: String,

    @SerializedName("product_name")
    val productName: String,

    @SerializedName("product_price")
    val productPrice: String,

    @SerializedName("quantity")
    val quantity: String,

    @SerializedName("remarks")
    val remarks: String,

    @SerializedName("multotal")
    val multotal: String,

    @SerializedName("selections")
    val selections: List<HoldBillItemSelectionRequestModel> = emptyList()
)

data class HoldBillItemSelectionRequestModel(
    @SerializedName("product_code")
    val productCode: String,

    @SerializedName("product_name")
    val productName: String,

    @SerializedName("product_price")
    val productPrice: String,

    @SerializedName("quantity")
    val quantity: String,

    @SerializedName("multotal")
    val multotal: String
)