package com.easipos.tabletordering.api.requests.ordering

import com.easipos.tabletordering.tools.Preference

data class GetOrderReviewRequestModel(
    val userCode: String = Preference.prefUserCode
)