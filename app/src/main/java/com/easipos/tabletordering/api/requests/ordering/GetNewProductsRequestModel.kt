package com.easipos.tabletordering.api.requests.ordering

import com.google.gson.annotations.SerializedName

data class GetNewProductsRequestModel(
    @SerializedName("dept_cd")
    val deptCd: String
)