package com.easipos.tabletordering.api.responses.auth

import com.google.gson.annotations.SerializedName

data class LoginInfoResponseModel(
    @SerializedName("tableName")
    val tableName: String?,


    @SerializedName("tblSysId")
    val tblSysId: String?,


    @SerializedName("currency")
    val currency: String?
)