package com.easipos.tabletordering.api.responses.ordering

import com.google.gson.annotations.SerializedName

data class ReceiptResponseModel(
    @SerializedName("tableNo")
    val tableNo: String?,

    @SerializedName("total")
    val total: String?,

    @SerializedName("subTotal")
    val subTotal: String?,

    @SerializedName("tax1Name")
    val tax1Name: String?,

    @SerializedName("tax2Name")
    val tax2Name: String?,

    @SerializedName("tax1")
    val tax1: String?,

    @SerializedName("tax2")
    val tax2: String?,

    @SerializedName("totalQty")
    val totalQty: String?,

    @SerializedName("discount")
    val discount: String?,

    @SerializedName("receiptNo")
    val receiptNo: String?,

    @SerializedName("datetime")
    val datetime: String?,

    @SerializedName("address")
    val address: List<String>?,

    @SerializedName("items")
    val items: List<ReceiptItemResponseModel>?
)

data class ReceiptItemResponseModel(
    @SerializedName("productName")
    val productName: String?,

    @SerializedName("quantity")
    val quantity: String?,

    @SerializedName("price")
    val price: String?,

    @SerializedName("amount")
    val amount: String?,

    @SerializedName("Remarks")
    val remarks: String?,

    @SerializedName("selections")
    val selections: List<ReceiptItemSelectionResponseModel>?
)

data class ReceiptItemSelectionResponseModel(
    @SerializedName("productName")
    val productName: String?,

    @SerializedName("quantity")
    val quantity: String?,

    @SerializedName("price")
    val price: String?,

    @SerializedName("amount")
    val amount: String?,

    @SerializedName("Remarks")
    val remarks: String?
)