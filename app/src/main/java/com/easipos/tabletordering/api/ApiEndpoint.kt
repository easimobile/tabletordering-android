package com.easipos.tabletordering.api

object ApiEndpoint {

    const val CHECK_VERSION = "ddcard/version/check"

    const val LOGIN = "api/v1/login"

    const val GET_FLOORS = "api/v1/getFloors"
    const val GET_TABLES = "api/v1/getTables"
    const val UNLOCK_TABLE = "api/v1/unlockTable"
    const val SAVE_SELECTED_TABLE = "api/v1/saveSelectedTable"
    const val SET_OCCUPIED_SESSION = "api/v1/setOccupiedSession"
    const val GET_ORDER_REVIEW = "api/v1/getOrderReview"
    const val GET_MENU = "api/v1/getMenu"
    const val GET_NEW_PRODUCTS = "api/v1/getNewProducts"
    const val GET_PRODUCT_SET = "api/v1/getProductSet"
    const val GET_ORDER_ITEMS = "api/v1/getOrderItem"
    const val VOID_ITEM = "api/v1/voidItem"
    const val GET_REMARKS = "api/v1/getSelection"
    const val HOLD_BILL = "api/v1/holdBill"
    const val GET_GUEST_CHECK = "api/v1/getReceipt"
    const val DELETE_USING_TABLE = "api/v1/checkAndDeleteUsingTable"

    const val REGISTER_REMOVE_JPUSH_REG_ID = "ddcard/apptoken"
}
