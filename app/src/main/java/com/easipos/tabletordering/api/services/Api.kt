package com.easipos.tabletordering.api.services

import com.easipos.tabletordering.api.ApiEndpoint
import com.easipos.tabletordering.api.misc.EmptyResponseModel
import com.easipos.tabletordering.api.misc.ResponseModel
import com.easipos.tabletordering.api.requests.auth.LoginRequestModel
import com.easipos.tabletordering.api.requests.ordering.*
import com.easipos.tabletordering.api.responses.auth.LoginInfoResponseModel
import com.easipos.tabletordering.api.responses.ordering.*
import io.reactivex.Completable
import io.reactivex.Single
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface Api {

    @POST(ApiEndpoint.CHECK_VERSION)
    fun checkVersion(@Body body: RequestBody): Single<ResponseModel<Boolean>>

    @POST(ApiEndpoint.LOGIN)
    fun login(@Body body: LoginRequestModel): Single<ResponseModel<LoginInfoResponseModel>>

    @GET(ApiEndpoint.GET_FLOORS)
    fun getFloors(): Single<ResponseModel<List<FloorResponseModel>>>

    @GET(ApiEndpoint.GET_TABLES)
    fun getTables(@Query("floor") floor: String): Single<ResponseModel<List<TableResponseModel>>>

    @POST(ApiEndpoint.UNLOCK_TABLE)
    fun unlockTable(@Body body: UnlockTableRequestModel): Single<EmptyResponseModel>

    @POST(ApiEndpoint.SAVE_SELECTED_TABLE)
    fun saveSelectedTable(@Body body: SaveSelectedTableRequestModel): Single<ResponseModel<SaveSelectedTableResponseModel>>

    @POST(ApiEndpoint.SET_OCCUPIED_SESSION)
    fun setOccupiedSession(@Body body: SetOccupiedSessionRequestModel): Single<ResponseModel<SetOccupiedSessionResponseModel>>

    @GET(ApiEndpoint.GET_ORDER_REVIEW)
    fun getOrderReview(@Query("userCode") userCode: String): Single<ResponseModel<OrderReviewResponseModel>>

    @GET(ApiEndpoint.GET_MENU)
    fun getMenu(): Single<ResponseModel<List<MenuResponseModel>>>

    @POST(ApiEndpoint.GET_NEW_PRODUCTS)
    fun getNewProducts(@Query("menu") menu: String): Single<ResponseModel<List<ProductResponseModel>>>

    @POST(ApiEndpoint.GET_PRODUCT_SET)
    fun getProductSet(@Body model: GetProductSetRequestModel): Single<ResponseModel<List<ProductSetResponseModel>>>

    @GET(ApiEndpoint.GET_ORDER_ITEMS)
    fun getOrderItems(@Query("tableName") tableName: String, @Query("tblSysId") tblSysId: String): Single<ResponseModel<List<OrderItemResponseModel>>>

    @POST(ApiEndpoint.VOID_ITEM)
    fun voidItem(@Body body: VoidItemRequestModel): Single<EmptyResponseModel>

    @POST(ApiEndpoint.GET_REMARKS)
    fun getRemarks(@Query("prod_code") prodCode: String): Single<ResponseModel<List<RemarkResponseModel>>>

    @POST(ApiEndpoint.HOLD_BILL)
    fun holdBill(@Body body: HoldBillRequestModel): Single<EmptyResponseModel>

    @POST(ApiEndpoint.GET_GUEST_CHECK)
    fun getGuestCheck(@Body body: GetGuestCheckRequestModel): Single<ResponseModel<ReceiptResponseModel>>

    @POST(ApiEndpoint.DELETE_USING_TABLE)
    fun deleteUsingTable(@Body body: DeleteUsingTableRequestModel): Single<EmptyResponseModel>

    @POST(ApiEndpoint.REGISTER_REMOVE_JPUSH_REG_ID)
    fun registerFcmToken(@Body body: RequestBody): Completable

    @POST(ApiEndpoint.REGISTER_REMOVE_JPUSH_REG_ID)
    fun removeFcmToken(@Body body: RequestBody): Completable
}
