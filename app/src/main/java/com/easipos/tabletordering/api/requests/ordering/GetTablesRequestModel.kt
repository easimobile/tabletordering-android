package com.easipos.tabletordering.api.requests.ordering

data class GetTablesRequestModel(
    val floor: String
)