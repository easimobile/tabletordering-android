package com.easipos.tabletordering.api.requests.auth

data class LoginRequestModel(
    val usercode: String,
    val password: String
)