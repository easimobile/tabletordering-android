package com.easipos.tabletordering.api.responses.ordering

import com.google.gson.annotations.SerializedName

data class SetOccupiedSessionResponseModel(
    @SerializedName("pax")
    val pax: String?
)