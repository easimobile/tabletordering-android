package com.easipos.tabletordering.api.responses.ordering

import com.google.gson.annotations.SerializedName

data class TableResponseModel(
    @SerializedName("tblName")
    val tableName: String?,

    @SerializedName("tblsysid")
    val tblSysId: String?,

    @SerializedName("status")
    val status: String?
)