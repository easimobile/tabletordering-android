package com.easipos.tabletordering.api.requests.ordering

import com.easipos.tabletordering.tools.Preference

data class DeleteUsingTableRequestModel(
    val tableName: String = Preference.prefTableName,
    val tblsysid: String = Preference.prefTableSysId
)