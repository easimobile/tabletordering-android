package com.easipos.tabletordering.api.responses.ordering

import com.google.gson.annotations.SerializedName

data class OrderReviewResponseModel(
    @SerializedName("prepare")
    val prepare: List<PrepareResponseModel>,

    @SerializedName("ready")
    val ready: List<ReadyResponseModel>
)

data class PrepareResponseModel(
    @SerializedName("ORDER_NUMBER")
    val orderNumber: String?,

    @SerializedName("PROD_NM")
    val prodNm: String?,

    @SerializedName("PROD_NM_CH")
    val prodNmCh: String?,

    @SerializedName("Remarks")
    val remarks: String?,

    @SerializedName("imageUrl")
    val imageUrl: String?
)

data class ReadyResponseModel(
    @SerializedName("ORDER_NUMBER")
    val orderNumber: String?,

    @SerializedName("PROD_NM")
    val prodNm: String?,

    @SerializedName("PROD_NM_CH")
    val prodNmCh: String?,

    @SerializedName("Remarks")
    val remarks: String?,

    @SerializedName("imageUrl")
    val imageUrl: String?
)