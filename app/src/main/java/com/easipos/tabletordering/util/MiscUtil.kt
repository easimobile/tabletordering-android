package com.easipos.tabletordering.util

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.PopupWindow
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.easipos.tabletordering.Easi
import com.easipos.tabletordering.R
import com.easipos.tabletordering.managers.PushNotificationManager
import com.easipos.tabletordering.managers.PushNotificationManager.Companion.FCM_BODY
import com.easipos.tabletordering.managers.PushNotificationManager.Companion.FCM_TITLE
import com.easipos.tabletordering.tools.Preference
import com.tapadoo.alerter.Alerter
import io.github.anderscheow.library.kotlinExt.findColor
import io.github.anderscheow.library.kotlinExt.formatAmount
import org.json.JSONObject

val languages = mapOf("zh" to "中文", "en" to "English")

fun defaultRequestOption(): RequestOptions {
    return RequestOptions()
}

fun ImageView.loadImage(imageUrl: String) {
    Glide.with(this.context)
            .load(imageUrl)
            .apply(defaultRequestOption())
            .transition(DrawableTransitionOptions().crossFade(500))
            .into(this)
}

fun ImageView.loadImageBase64(imgBase64: String) {
    try {
        val imageBytes = Base64.decode(imgBase64, Base64.DEFAULT)
        Glide.with(this.context)
            .load(imageBytes)
            .apply(defaultRequestOption())
//            .transition(DrawableTransitionOptions().crossFade(500))
            .into(this)
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
}

fun changeLanguage(activity: Activity, language: String) {
    if (Preference.prefLanguageCode != language) {
        Preference.prefLanguageCode = language
        Easi.localeManager.setNewLocale(activity, language)
        activity.recreate()
    }
}

fun showAlerter(easi: Easi, pushNotificationManager: PushNotificationManager, jsonObject: JSONObject) {
    jsonObject.takeIf {
        jsonObject.has(FCM_TITLE) && jsonObject.has(FCM_BODY)
    }.run {
        this?.let { data ->
            easi.currentActivity?.let { activity ->
                Alerter.create(activity)
                    .setTitle(data.getString(FCM_TITLE) ?: "")
                    .setText(data.getString(FCM_BODY) ?: "")
                    .setBackgroundColorInt(activity.findColor(R.color.colorPrimary))
                    .setDuration(5000)
                    .setIconColorFilter(Color.WHITE)
                    .enableSwipeToDismiss()
                    .enableVibration(true)
                    .setOnClickListener(View.OnClickListener {
                        Alerter.hide()

                        pushNotificationManager.openNotification(easi, jsonObject, true)
                    })
                    //.setOnShowListener { Tracker.trackScreen(this@CustomLifecycleActivity, Screen.IN_APP_NOTIFICATION) }
                    .show()
            }
        }
    }
}

fun Activity.hideSystemUI() {
    // Enables regular immersive mode.
    // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
    // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
    window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            // Set the content to appear under the system bars so that the
            // content doesn't resize when the system bars hide and show.
            or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            // Hide the nav bar and status bar
            or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            or View.SYSTEM_UI_FLAG_FULLSCREEN)
}

// Shows the system bars by removing all the flags
// except for the ones that make the content appear under the system bars.
fun Activity.showSystemUI() {
    window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
}

fun Context.formatCurrencyAmount(amount: Double): String {
    return this.getString(R.string.label_amount, Preference.prefSettingCurrency, amount.formatAmount())
}

fun generatePopupWindow(activity: Activity, layout: Int, width: Int, height: Int, block: (View) -> Unit): PopupWindow? {
    val layoutInflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as? LayoutInflater
    var popupView: View
    layoutInflater?.let {
        popupView = it.inflate(layout, null)

        block(popupView)

        return PopupWindow(
            popupView,
            width,
            height
        ).apply {
            elevation = 8f
            isOutsideTouchable = true
            isFocusable = true
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    return null
}