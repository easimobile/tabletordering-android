package com.easipos.tabletordering.constant

object OrderReviewStatus {

    const val PREPARE = "PREPARE"
    const val READY = "READY"
}