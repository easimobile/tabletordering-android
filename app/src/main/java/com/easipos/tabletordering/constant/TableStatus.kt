package com.easipos.tabletordering.constant

object TableStatus {

    const val OCCUPIED = "Occupied"
    const val AVAILABLE = "Available"
    const val LOCKED = "Locked"
}