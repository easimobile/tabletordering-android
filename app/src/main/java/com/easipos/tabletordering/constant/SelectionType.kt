package com.easipos.tabletordering.constant

enum class SelectionType {
    SINGLE,
    MULTIPLE
}