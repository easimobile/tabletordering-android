package com.easipos.tabletordering.constant

enum class Layer {
    ONE,
    MULTIPLE
}