package com.easipos.tabletordering.event_bus

data class NotificationCount(val count: Int)