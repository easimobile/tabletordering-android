package com.easipos.tabletordering.models

data class LoginInfo(
    val tableName: String,
    val tblSysId: String,
    val currency: String
)