package com.easipos.tabletordering.models

data class Floor(
    val floorName: String
)