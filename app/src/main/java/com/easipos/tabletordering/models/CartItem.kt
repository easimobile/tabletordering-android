package com.easipos.tabletordering.models

import android.content.Context
import com.easipos.tabletordering.util.formatCurrencyAmount

data class CartItem(
    val product: Product? = null,
    val orderItem: OrderItem? = null,
    val openPrice: Double? = null
) {

    fun formatName(): String {
        if (product != null) {
            return product.prodShNm
        } else if (orderItem != null) {
            return orderItem.prodNm
        }
        return ""
    }

    fun formatBothName(): String {
        if (product != null) {
            return "${product.prodNmCh}\n${product.prodShNm}"
        } else if (orderItem != null) {
            return "${orderItem.prodNmCh}\n${orderItem.prodNm}"
        }
        return ""
    }

    fun formatRemarks(): String {
        if (product != null) {
            return product.remarks.filter { it.isSelected }
                .joinToString(", ") { it.kitchenRequestDescription }
        } else if (orderItem != null) {
            return orderItem.remarks
        }
        return ""
    }

    fun formatPrice(context: Context): String {
        if (product != null) {
            return if (openPrice != null) {
                context.formatCurrencyAmount(openPrice)
            } else {
                context.formatCurrencyAmount(product.price)
            }
        } else if (orderItem != null) {
            return context.formatCurrencyAmount(orderItem.prodPrice)
        }
        return ""
    }

    fun getTotalPrice(): Double {
        if (product != null) {
            return if (openPrice != null) {
                openPrice * product.quantity
            } else {
                if (product.productSet.isNotEmpty()) {
                    (product.price + product.getTotalProductSetPrice()) * product.quantity
                } else {
                    product.price * product.quantity
                }
            }
        }
        return 0.0
    }

    fun getQuantity(): Int {
        if (product != null) {
            return product.quantity
        } else if (orderItem != null) {
            return orderItem.quantity
        }
        return 1
    }

    fun getProductSetCart(): List<ProductSetCart> {
        val productSetCart = arrayListOf<ProductSetCart>()

        if (product != null) {
            productSetCart.addAll(
                product.productSet.map {
                    ProductSetCart(
                        productCode = it.productCode,
                        quantity = it.mutableQuantity,
                        productName = it.productName,
                        productNameCh = it.productNameCh,
                        productPrice = it.productPrice
                    )
                }
            )
        } else if (orderItem != null) {
            productSetCart.addAll(
                orderItem.selections.map {
                    ProductSetCart(
                        productCode = it.prodCd,
                        quantity = it.quantity,
                        productName = it.prodNm,
                        productNameCh = it.prodNmCh,
                        productPrice = it.prodPrice
                    )
                }
            )
        }

        return productSetCart
    }
}