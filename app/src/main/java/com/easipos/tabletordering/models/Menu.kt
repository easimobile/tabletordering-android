package com.easipos.tabletordering.models

data class Menu(
    val deptCd: String,
    val deptNm: String,
    val deptCh: String,
    val dispOrder: String,
    val deptImg: String
) {

    fun formatBothName(): String {
        return "$deptCh\n$deptNm"
    }
}