package com.easipos.tabletordering.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Remark(
    val kitchenRequestCode: String,
    val kitchenRequestDescription: String,
    var isSelected: Boolean = false
) : Parcelable