package com.easipos.tabletordering.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Table(
    val tableName: String,
    val tblSysId: String,
    val status: String
) : Parcelable