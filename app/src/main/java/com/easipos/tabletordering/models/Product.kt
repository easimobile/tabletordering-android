package com.easipos.tabletordering.models

import android.content.Context
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.easipos.tabletordering.util.formatCurrencyAmount
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class Product(
    @PrimaryKey
    val prodCd: String,
    val prodShNm: String,
    val price: Double,
    val prodNmCh: String,
    val tax1: String,
    val tax2: String,
    val imgData: String,
    val openPrice: Boolean,
    val productType: String,
    var deptCode: String = "",
    var quantity: Int = 1
) : Parcelable {

    @IgnoredOnParcel
    @Ignore
    var remarks: List<Remark> = emptyList()

    @IgnoredOnParcel
    @Ignore
    var productSet: List<ProductSet> = emptyList()

    fun formatBothName(): String {
        return "$prodNmCh\n$prodShNm"
    }

    fun formatPrice(context: Context): String {
        return context.formatCurrencyAmount(price)
    }

    fun getQuantityInCart(): Int {
        return Cart.cartLiveData.value?.cartItems?.let { cartItems ->
            val products = cartItems.filter { it.product != null && it.product.prodCd == prodCd }
            if (products.isNotEmpty()) {
                products.map { it.product?.quantity ?: 0 }.reduce { acc, i -> acc + i }
            } else {
                0
            }
        } ?: run {
            0
        }
    }

    fun getTotalProductSetPrice(): Double {
        var total = 0.0
        productSet.forEach { set ->
            total += set.productPrice * set.mutableQuantity
        }

        return total
    }
}