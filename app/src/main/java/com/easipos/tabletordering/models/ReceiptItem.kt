package com.easipos.tabletordering.models

import android.os.Parcelable
import io.github.anderscheow.library.kotlinExt.formatAmount
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ReceiptItem(
    val productName: String,
    val quantity: String,
    val price: String,
    val amount: String,
    val remarks: String,
    val selections: List<ReceiptItemSelection>
) : Parcelable {

    fun formatQuantity(): String {
        return quantity.toDoubleOrNull()?.toInt()?.toString() ?: quantity
    }

    fun formatDescription(): String {
        return if (remarks.isNotBlank()) {
            "$productName\n$remarks"
        } else {
            productName
        }
    }

    fun formatPrice(): String {
        return price.toDoubleOrNull()?.formatAmount() ?: price
    }

    fun formatAmount(): String {
        return amount.toDoubleOrNull()?.formatAmount() ?: amount
    }
}

@Parcelize
data class ReceiptItemSelection(
    val productName: String,
    val quantity: String,
    val price: String,
    val amount: String,
    val remarks: String
) : Parcelable {

    fun formatPrice(): String {
        return price.toDoubleOrNull()?.formatAmount() ?: price
    }

    fun formatAmount(): String {
        return amount.toDoubleOrNull()?.formatAmount() ?: amount
    }
}