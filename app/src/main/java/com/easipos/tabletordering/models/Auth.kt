package com.easipos.tabletordering.models

data class Auth(val token: String)