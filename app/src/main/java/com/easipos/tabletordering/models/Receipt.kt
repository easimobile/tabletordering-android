package com.easipos.tabletordering.models

import android.os.Parcelable
import io.github.anderscheow.library.kotlinExt.formatAmount
import io.github.anderscheow.library.kotlinExt.formatDate
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
data class Receipt(
    val tableNo: String,
    val total: String,
    val subTotal: String,
    val tax1Name: String,
    val tax2Name: String,
    val tax1: String,
    val tax2: String,
    val totalQty: String,
    val discount: String,
    val receiptNo: String,
    val datetime: String,
    val address: List<String>,
    val items: List<ReceiptItem>
) : Parcelable {

    fun formatQuantity(): String {
        return totalQty.toDoubleOrNull()?.toInt()?.toString() ?: totalQty
    }

    fun formatAddress(): String {
        return address.joinToString("\n")
    }

    fun formatDate(): String {
        return try {
            val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(
                datetime
            )
            date?.time.formatDate("dd/MM/yyyy")
        } catch (ex: Exception) {
            datetime
        }
    }

    fun formatTime(): String {
        return try {
            val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(
                datetime
            )
            date?.time.formatDate("HH:mm:ss")
        } catch (ex: Exception) {
            datetime
        }
    }

    fun formatSubtotal(): String {
        return subTotal.toDoubleOrNull()?.formatAmount() ?: subTotal
    }

    fun formatTaxName(): String {
        val tax1 = tax1.toDoubleOrNull() ?: 0.0

        return if (tax1 == 0.0) {
            tax2Name
        } else {
            tax1Name
        }
    }

    fun formatTax(): String {
        val tax1 = tax1.toDoubleOrNull() ?: 0.0
        val tax2 = tax2.toDoubleOrNull() ?: 0.0

        return if (tax1 == 0.0) {
            tax2.formatAmount()
        } else {
            tax1.formatAmount()
        }
    }

    fun formatTotal(): String {
        return total.toDoubleOrNull()?.formatAmount() ?: total
    }
}