package com.easipos.tabletordering.models

import androidx.lifecycle.MutableLiveData
import io.github.anderscheow.library.kotlinExt.mutation

class Cart {
    val cartItems = arrayListOf<CartItem>()

    companion object {
        val cartLiveData = MutableLiveData<Cart>().apply {
            this.postValue(Cart())
        }

        fun addCartItem(cartItem: CartItem) {
            cartLiveData.mutation {
                it.value?.cartItems?.add(cartItem)
            }
        }

        fun addOrderItems(orderItems: List<OrderItem>) {
            cartLiveData.mutation {
                it.value?.cartItems?.apply {
                    val productCartItems = this.filter { cartItem ->
                        cartItem.product != null
                    }

                    this.clear()
                    this.addAll(orderItems.map { orderItem ->
                        CartItem(
                            orderItem = orderItem
                        )
                    })
                    this.addAll(productCartItems)
                }
            }
        }

        fun removeCartItem(index: Int) {
            cartLiveData.mutation {
                it.value?.cartItems?.removeAt(index)
            }
        }

        fun updateQuantity(index: Int, quantity: Int) {
            cartLiveData.mutation {
                it.value?.cartItems?.getOrNull(index)?.product?.quantity = quantity
            }
        }

        fun addRemarks(index: Int, remarks: List<Remark>) {
            cartLiveData.mutation {
                it.value?.cartItems?.getOrNull(index)?.product?.remarks = remarks
            }
        }

        fun clearAll() {
            cartLiveData.mutation {
                it.value?.cartItems?.clear()
            }
        }
    }
}