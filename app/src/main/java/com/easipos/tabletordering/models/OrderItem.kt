package com.easipos.tabletordering.models

data class OrderItem(
    val prodCd: String,
    val prodNm: String,
    val quantity: Int,
    val remarks: String,
    val prodPrice: Double,
    val multotal: String,
    val linkCd: String,
    val prodNmCh: String,
    val selections: List<OrderItemSelection>
)

data class OrderItemSelection(
    val prodCd: String,
    val prodNm: String,
    val quantity: Int,
    val remarks: String,
    val prodPrice: Double,
    val multotal: String,
    val linkCd: String,
    val prodNmCh: String
)