package com.easipos.tabletordering.models

import android.content.Context
import android.os.Parcelable
import com.easipos.tabletordering.constant.Layer
import com.easipos.tabletordering.util.formatCurrencyAmount
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProductSet(
    val parentProductCode: String,
    val productCode: String,
    val quantity: Int,
    val productName: String,
    val productNameCh: String,
    val productPrice: Double,
    val prodType: String,
    val insert: Boolean,
    var mutableQuantity: Int = 1,
    var selections: List<ProductSet> = emptyList()
) : Parcelable {

    fun formatPrice(context: Context): String? {
        return context.formatCurrencyAmount(productPrice)
    }
}

data class ProductSetWithLayer(
    val layer: Layer,
    val productSet: List<ProductSet>
)

data class ProductSetCart(
    val productCode: String,
    val quantity: Int,
    val productName: String,
    val productNameCh: String,
    val productPrice: Double
) {

    fun formatName(): String {
        return "@ $productName x $quantity"
    }

    fun formatPrice(context: Context): String? {
        return context.formatCurrencyAmount(productPrice * quantity)
    }
}