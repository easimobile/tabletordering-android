package com.easipos.tabletordering.models

data class Selectable<T>(
    var data: T,
    var isSelected: Boolean = false
)