package com.easipos.tabletordering.models

data class SetOccupiedSession(
    val pax: Int
)