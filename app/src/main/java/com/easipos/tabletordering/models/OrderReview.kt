package com.easipos.tabletordering.models

data class OrderReview(
    val orderNumber: String,
    val prodNm: String,
    val prodNmCh: String,
    val remarks: String,
    val status: String,
    val imageUrl: String
)