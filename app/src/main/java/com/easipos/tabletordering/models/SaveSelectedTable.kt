package com.easipos.tabletordering.models

data class SaveSelectedTable(
    val tableName: String,
    val tblSysId: String
)