package com.easipos.tabletordering.activities.table_selection

import android.content.Context
import android.content.Intent

class TableSelectionActivity : TableSelectionBaseActivity() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, TableSelectionActivity::class.java)
        }
    }
}
