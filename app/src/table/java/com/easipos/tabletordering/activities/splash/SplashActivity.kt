package com.easipos.tabletordering.activities.splash

import android.content.Context
import android.content.Intent
import com.easipos.tabletordering.bundle.ParcelData

class SplashActivity : SplashBaseActivity() {

    companion object {
        fun newIntent(context: Context, clearDb: Boolean = true): Intent {
            return Intent(context, SplashActivity::class.java).apply {
                this.putExtra(ParcelData.CLEAR_DB, clearDb)
            }
        }
    }
}
