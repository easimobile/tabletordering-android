package com.easipos.tabletordering.activities.main

import android.content.Context
import android.content.Intent

class MainActivity : MainBaseActivity() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    fun updateProductAdapter() {
    }

    fun updateCartItemAdapter() {
    }

    fun updateProductRow() {
    }
}
