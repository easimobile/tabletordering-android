package com.easipos.tabletordering.activities.login

import android.content.Context
import android.content.Intent

class LoginActivity : LoginBaseActivity() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)
        }
    }
}
