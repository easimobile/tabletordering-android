package com.easipos.tabletordering.di.modules

import org.kodein.di.Kodein

class FragmentModule: FragmentBaseModule() {

    override fun provideAdditionalFragmentModule(builder: Kodein.Builder) {
    }
}
